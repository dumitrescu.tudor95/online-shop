package shop.creditcardmicroservice.creditcard.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import shop.creditcardmicroservice.creditcard.dto.CreditCardDto;
import shop.creditcardmicroservice.creditcard.service.CreditCardService;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CreditCardController.class)
public class CreditCardControllerTest {

    @MockBean
    CreditCardService creditCardService;

    @Autowired
    private MockMvc mockMvc;

    String userId = "testUserId";
    String creditCardNumber = "testCreditCardNumber";
    String numberBack = "testNumberBack";
    String expirationMonth = "testExpirationMonth";
    String expirationYear = "testExpirationYear";

    @Test
    public void saveCreditCardToDatabase() throws Exception {

        when(creditCardService.saveCreditCard(anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
                .thenReturn("Credit card saved");

        mockMvc.perform(post("/addCreditCardTo/{userId}", userId)
                .header("creditCardNumber", creditCardNumber)
                .header("numberBack", numberBack)
                .header("ownerName", userId)
                .header("expirationMonth", expirationMonth)
                .header("expirationYear",  expirationYear))
                .andExpect(status().isOk())
        .andExpect(content().string("Credit card saved"));


    }

    @Test
    public void getCreditCard() throws Exception {
        CreditCardDto dto = new CreditCardDto();
        dto.setOwnerName(userId);
        when(creditCardService.getCreditCard(anyString())).thenReturn(dto);

        mockMvc.perform(get("/getCreditCard/{userId}", userId))
                .andExpect(status().isOk());
    }

}