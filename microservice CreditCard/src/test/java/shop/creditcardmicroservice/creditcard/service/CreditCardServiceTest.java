package shop.creditcardmicroservice.creditcard.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import shop.creditcardmicroservice.creditcard.configuration.UserManager;
import shop.creditcardmicroservice.creditcard.dto.CreditCardDto;
import shop.creditcardmicroservice.creditcard.dto.UserEmailAndPasswordDto;
import shop.creditcardmicroservice.creditcard.entity.CreditCard;
import shop.creditcardmicroservice.creditcard.exceptions.CreditCardNotFoundException;
import shop.creditcardmicroservice.creditcard.mapper.CreditCardMapper;
import shop.creditcardmicroservice.creditcard.repository.CreditCardRepository;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CreditCardServiceTest {

    @MockBean
    CreditCardRepository creditCardRepository;
    @MockBean
    UserManager userManager;

    @InjectMocks
    CreditCardService creditCardService;

    CreditCard creditCard;
    String cardId = "testId";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        creditCard = new CreditCard();
        creditCard.setCardNumber(cardId);
    }

    @Test
    public void saveCreditCard() throws IOException {
        UserEmailAndPasswordDto user = new UserEmailAndPasswordDto();

        when(userManager.getUserByUsername(anyString())).thenReturn(user);

        creditCardService.saveCreditCard("x", "x", "1", "x", "2", "3");

        verify(userManager, times(1)).getUserByUsername(anyString());
        verify(creditCardRepository, times(1)).save(any());
        verify(userManager, times(1)).assignCreditCard(any(), any());

    }

    @Test
    public void getCreditCard() {
        when(creditCardRepository.findByOwnerUsername(anyString())).thenReturn(creditCard);

        CreditCardDto returnedCard = CreditCardMapper.convertToDto(creditCard);

        assertEquals(creditCardService.getCreditCard(cardId), returnedCard);
        verify(creditCardRepository, times(1)).findByOwnerUsername(anyString());
    }

    @Test(expected = CreditCardNotFoundException.class)
    public void getCreditCardWhenItDoesNotExist() {
        when(creditCardRepository.findByOwnerUsername(anyString())).thenReturn(null);

        creditCardService.getCreditCard(cardId);

        verify(creditCardRepository, times(1)).findByOwnerUsername(anyString());
    }
}