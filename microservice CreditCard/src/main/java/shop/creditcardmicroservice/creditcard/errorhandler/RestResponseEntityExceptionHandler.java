package shop.creditcardmicroservice.creditcard.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import shop.creditcardmicroservice.creditcard.exceptions.CreditCardNotFoundException;
import shop.creditcardmicroservice.creditcard.exceptions.UserDoesNotExistException;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler(value = {UserDoesNotExistException.class, CreditCardNotFoundException.class})
    protected ResponseEntity<Object> handleMethodArgumentNotValid(HttpServletRequest request, Exception ex) {

        System.out.println("======================== CREDIT CARD / USER DOES NOT EXIST EXCEPTION HANDLER ============================");

        ErrorDTO error = new ErrorDTO();
        error.setMessage(ex.getMessage());
        error.setStatusCode("404");


        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

}