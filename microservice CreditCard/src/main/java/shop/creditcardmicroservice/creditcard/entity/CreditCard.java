package shop.creditcardmicroservice.creditcard.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import org.springframework.data.annotation.Id;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Document(collection = "CreditCardDatabase")
public class CreditCard {

    @Id
    private String cardNumber;
    private String ownerName;
    private Integer cardNumberBack;
    private LocalDate expirationDate;
    private String ownerUsername;
}
