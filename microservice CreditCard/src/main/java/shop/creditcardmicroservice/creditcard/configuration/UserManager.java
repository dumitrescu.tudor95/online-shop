package shop.creditcardmicroservice.creditcard.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import shop.creditcardmicroservice.creditcard.configuration.requestMaker.MicroserviceRequest;
import shop.creditcardmicroservice.creditcard.configuration.requestMaker.RequestType;
import shop.creditcardmicroservice.creditcard.dto.UserEmailAndPasswordDto;
import shop.creditcardmicroservice.creditcard.dto.UserWithCardDto;
import shop.creditcardmicroservice.creditcard.entity.CreditCard;
import shop.creditcardmicroservice.creditcard.errorhandler.RestTemplateResponseErrorHandler;

import java.io.IOException;
import java.util.HashMap;

@Component
public class UserManager extends MicroserviceRequest {

    @Value("${get.user.email.and.password}")
    private String GET_USERNAME_AND_PASSWORD_LINK;
    @Value("${get.user.with.credit.card}")
    private String GET_USER_WITH_CREDIT_CARD_LINK;
    @Value("${assign.credit.card}")
    private String ASSIGN_CREDIT_CARD_LINK;
    @Value("${add.balance.to}")
    private String ADD_BALANCE_LINK;

    public UserEmailAndPasswordDto getUserByUsername(String userId) throws IOException {

        Object returnedObject=requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_USERNAME_AND_PASSWORD_LINK + userId,
                NO_QUERY_PARAMS,
                NO_HEADERS);
        return convertToRequestedObject(returnedObject,UserEmailAndPasswordDto.class);
    }

    public UserWithCardDto getUserWithCard(String userId) throws IOException {
        Object returnedObject=requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_USER_WITH_CREDIT_CARD_LINK,
                NO_QUERY_PARAMS,
                NO_HEADERS);
        return convertToRequestedObject(returnedObject,UserWithCardDto.class);
        }

    public void assignCreditCard(String userId, CreditCard creditCard) {
        HashMap<String,String> headers=new HashMap<>();
        headers.put("cardNumber",creditCard.getCardNumber());

        requestSender.sendMessageReturningRequest(RequestType.PUT,
                ASSIGN_CREDIT_CARD_LINK +userId,
                NO_QUERY_PARAMS,
                headers);
    }

    public void addBalanceTo(String userId, Double amount) {
        HashMap<String,String> headers=new HashMap<>();
        headers.put("amount",String.valueOf(amount));

        requestSender.sendMessageReturningRequest(RequestType.PUT,
                ADD_BALANCE_LINK,
                NO_QUERY_PARAMS,
                headers);
    }


}
