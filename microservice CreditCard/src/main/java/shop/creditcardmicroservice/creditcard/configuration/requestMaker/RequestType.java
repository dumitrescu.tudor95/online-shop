package shop.creditcardmicroservice.creditcard.configuration.requestMaker;

public enum RequestType {
    GET, PUT, POST, DELETE;
}
