package shop.creditcardmicroservice.creditcard.configuration;


public interface EmailSender {

    String sendEmail(String specificUser, String subject, String message);

}
