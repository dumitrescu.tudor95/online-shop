package shop.creditcardmicroservice.creditcard.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CreditCardDto {

    private String cardNumber;
    private String ownerName;
    private Integer cardNumberBack;
    private LocalDate expirationDate;
    private String ownerUsername;



}
