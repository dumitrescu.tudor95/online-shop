package shop.creditcardmicroservice.creditcard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import shop.creditcardmicroservice.creditcard.dto.CreditCardDto;
import shop.creditcardmicroservice.creditcard.entity.CreditCard;
import shop.creditcardmicroservice.creditcard.exceptions.UserDoesNotExistException;
import shop.creditcardmicroservice.creditcard.service.CreditCardService;

import java.io.IOException;

@RestController
public class CreditCardController {

    @Autowired
    CreditCardService creditCardService;

    @PostMapping("/addCreditCardTo/{userId}")
    public String saveCreditCardToDatabase(@PathVariable("userId") String userId,
                                           @RequestHeader("creditCardNumber") String creditCardNumber,
                                           @RequestHeader("numberBack") String numberBack,
                                           @RequestHeader("ownerName") String ownerName,
                                           @RequestHeader("expirationMonth") String expirationMonth,
                                           @RequestHeader("expirationYear") String expirationYear) throws UserDoesNotExistException, IOException {
        return creditCardService.saveCreditCard(userId,creditCardNumber,numberBack,ownerName,expirationMonth,expirationYear);
    }

    @GetMapping("/getCreditCard/{userId}")
    public CreditCardDto getCreditCard(@PathVariable("userId") String userId) {
        return creditCardService.getCreditCard(userId);
    }


}
