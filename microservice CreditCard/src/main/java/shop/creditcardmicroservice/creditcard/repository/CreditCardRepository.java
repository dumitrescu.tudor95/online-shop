package shop.creditcardmicroservice.creditcard.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import shop.creditcardmicroservice.creditcard.entity.CreditCard;

@Repository
public interface CreditCardRepository extends MongoRepository<CreditCard,String> {

    CreditCard findByOwnerUsername(String ownerUsername);
}
