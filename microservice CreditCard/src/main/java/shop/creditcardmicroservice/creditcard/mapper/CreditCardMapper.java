package shop.creditcardmicroservice.creditcard.mapper;

import shop.creditcardmicroservice.creditcard.dto.CreditCardDto;
import shop.creditcardmicroservice.creditcard.entity.CreditCard;

import java.time.LocalDate;

public class CreditCardMapper {

    public static CreditCardDto convertToDto(CreditCard creditCard){
        CreditCardDto creditCardDto=new CreditCardDto();
        creditCardDto.setCardNumber(creditCard.getCardNumber());
        creditCardDto.setCardNumberBack(creditCard.getCardNumberBack());
        creditCardDto.setOwnerName(creditCard.getOwnerName());
        creditCardDto.setExpirationDate(creditCard.getExpirationDate());
        creditCardDto.setOwnerUsername(creditCard.getOwnerUsername());

        return creditCardDto;
    }
}
