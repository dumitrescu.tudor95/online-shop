package shop.creditcardmicroservice.creditcard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shop.creditcardmicroservice.creditcard.configuration.EmailSender;
import shop.creditcardmicroservice.creditcard.configuration.UserManager;
import shop.creditcardmicroservice.creditcard.dto.CreditCardDto;
import shop.creditcardmicroservice.creditcard.dto.UserEmailAndPasswordDto;
import shop.creditcardmicroservice.creditcard.entity.CreditCard;
import shop.creditcardmicroservice.creditcard.exceptions.CreditCardNotFoundException;
import shop.creditcardmicroservice.creditcard.exceptions.UserDoesNotExistException;
import shop.creditcardmicroservice.creditcard.mapper.CreditCardMapper;
import shop.creditcardmicroservice.creditcard.repository.CreditCardRepository;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    EmailSender emailSender;
    @Autowired
    CreditCardRepository creditCardRepository;
    @Autowired
    UserManager userManager;

    public String saveCreditCard(String userId,
                                 String creditCardNumber,
                                 String numberBack,
                                 String ownerName,
                                 String expirationMonth,
                                 String expirationYear) throws UserDoesNotExistException, IOException {

       UserEmailAndPasswordDto specificUser = userManager.getUserByUsername(userId);

        CreditCard newCard = new CreditCard();
        newCard.setCardNumber(creditCardNumber);
        newCard.setCardNumberBack(Integer.parseInt(numberBack));
        newCard.setOwnerName(ownerName);
        newCard.setExpirationDate(LocalDate.of(Integer.parseInt(expirationYear), Integer.parseInt(expirationMonth), 1));
        newCard.setOwnerUsername(userId);

        creditCardRepository.save(newCard);

        userManager.assignCreditCard(specificUser.getUsername(), newCard);

        //todo emailSender.sendEmail(specificUser.get().getEmail(), "Card registered", "Your card has been registered to your account.");

        return "Credit card saved";
    }



    public CreditCardDto getCreditCard(String ownerUsername) {
        Optional<CreditCard> specificCard = Optional.ofNullable(creditCardRepository.findByOwnerUsername(ownerUsername));
        if (!specificCard.isPresent()) {
            throw new CreditCardNotFoundException("Credit card not found");
        }
        return CreditCardMapper.convertToDto(specificCard.get());
    }


}
