package microservice.cart.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailFormDto {

    String from;
    String to;
    String subject;
    String message;
}
