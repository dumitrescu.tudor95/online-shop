package microservice.cart.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ProductDto {

    private String product_id;
    private String orderId;
    private String name;
    private String categoryId;
    private String details;
    private Double price;
    private Integer reviews;
    private Integer countInStock;
}
