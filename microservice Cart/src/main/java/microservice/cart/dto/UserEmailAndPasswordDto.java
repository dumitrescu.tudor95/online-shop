package microservice.cart.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserEmailAndPasswordDto {

    private String username;
    private String email;
    private String password;

}
