package microservice.cart.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@EqualsAndHashCode
public class UserDto {

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private List<String> ordersId;

}
