package microservice.cart.exceptions;

public class CartDoesNotExistException extends RuntimeException {
    public CartDoesNotExistException(String message){
        super(message);
    }
}