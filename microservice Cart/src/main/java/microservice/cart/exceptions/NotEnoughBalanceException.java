package microservice.cart.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class NotEnoughBalanceException extends RuntimeException {
    public NotEnoughBalanceException(String message){
        super(message);
    }
}
