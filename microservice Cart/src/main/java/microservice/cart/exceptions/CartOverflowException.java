package microservice.cart.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CartOverflowException extends RuntimeException {
    public CartOverflowException(String message){
        super(message);
    }
}