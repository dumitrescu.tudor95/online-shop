package microservice.cart.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import microservice.cart.dto.CartDto;
import microservice.cart.exceptions.CartDoesNotExistException;
import microservice.cart.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class CartController {

    @Autowired
    CartService cartService;

    @GetMapping("/carts")
    public List<CartDto> getAllCarts() {
        return cartService.getAllCarts();
    }

    @GetMapping("/carts/getCart")
    public CartDto getSpecificCart(@RequestParam("cartId") String cartId) {
        return cartService.getSpecificCart(cartId);
    }

    @PostMapping("/carts/addCart")
    public CartDto addCart(@RequestParam("userId") String user) throws IOException {
        return cartService.createCart(user);
    }

    @DeleteMapping("/carts/removeCart")
    public String deleteCart(@RequestParam("cartId") String cartId) throws IOException {
        return cartService.deleteCart(cartId);
    }

    @PutMapping("/carts/resetCart")
    public CartDto resetCart(@RequestParam("cartId") String cartId) {
        return cartService.resetCart(cartId);
    }

    @GetMapping("/getCartFrom/{userId}")
    public CartDto getCartFrom(@PathVariable("userId") String userId) throws IOException {
        return cartService.getCartFrom(userId);
    }

    @DeleteMapping("/deleteCartFrom/{userId}")
    public String deleteCartFrom(@PathVariable("userId") String userId) {
        return cartService.deleteCartFrom(userId);
    }

    @PutMapping("/addProductInCart")
    public String addProductInCart(@RequestParam("cartId") String cartId,
                                   @RequestParam("productId") String productId) throws CartDoesNotExistException, JsonProcessingException {
        return cartService.addProductInCart(cartId, productId);
    }

    @DeleteMapping("/removeProductFromCart")
    public String removeProductFromCart(@RequestParam("cartId") String cartId,
                                        @RequestParam("productId") String productId) throws JsonProcessingException {
        return cartService.removeProductFromCart(cartId, productId);
    }

    @PutMapping("/lowerCartPrice/{cartId}")
    public String lowerCartPrice(@PathVariable("cartId") String cartId,
                                 @RequestHeader("price") String price) {
        return cartService.lowerCartPrice(cartId, price);
    }


}