package microservice.cart.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import microservice.cart.configuration.requestMaker.RequestType;
import microservice.cart.dto.CartDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DiscountManager extends MicroserviceRequest{


    @Value("${get.discount}")
    private String GET_DISCOUNT_LINK;


    public Double getDiscount(CartDto cartDto) throws JsonProcessingException {

        String returnedString=requestSender.sendBodyPassedRequest_recievingString(RequestType.POST,
                GET_DISCOUNT_LINK,
                cartDto,
                NO_QUERY_PARAMS,
                NO_HEADERS);
        return Double.parseDouble(returnedString);
    }
}
