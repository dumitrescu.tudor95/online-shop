package microservice.cart.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import microservice.cart.configuration.requestMaker.NoHeaders;
import microservice.cart.configuration.requestMaker.NoQueryParams;
import microservice.cart.configuration.requestMaker.Request;
import microservice.cart.configuration.requestMaker.RequestType;
import microservice.cart.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class ProductManager extends MicroserviceRequest {

    @Value("${remove.product}")
    private String REMOVE_PRODUCT_LINK;
    @Value("${get.product}")
    private String GET_PRODUCT_LINK;


    public void removeProducts(String productId) {

        requestSender.sendMessageReturningRequest(RequestType.DELETE,
                REMOVE_PRODUCT_LINK + productId,
                NO_QUERY_PARAMS,
                NO_HEADERS);
    }

    public ProductDto getProductDto(String productId) throws IOException {
        Object returnedObject = requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_PRODUCT_LINK + productId,
                NO_QUERY_PARAMS,
                NO_HEADERS);
        return convertToRequestedObject(returnedObject, ProductDto.class);
    }


}
