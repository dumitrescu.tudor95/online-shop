package microservice.cart.configuration;

import com.netflix.discovery.converters.Auto;
import microservice.cart.dto.EmailFormDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class EmailManager implements EmailSender {

    @Value("${send.email}")
    private String SEND_EMAIL_LINK;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public String sendEmail(String specificUser, String subject, String message) {
        restTemplate = new RestTemplate();

        EmailFormDto emailDTO = new EmailFormDto();
        emailDTO.setFrom("tudor_project_shop@mailtrap.com");
        emailDTO.setTo(specificUser); //todo change email
        emailDTO.setSubject(subject);
        emailDTO.setMessage(message);

        new Thread(() -> {
            ResponseEntity<String> responseString = restTemplate.postForEntity(SEND_EMAIL_LINK, emailDTO, String.class);
        }).start();

        return "Mail will be sent shortly";
    }

}
