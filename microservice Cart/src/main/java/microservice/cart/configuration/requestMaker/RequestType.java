package microservice.cart.configuration.requestMaker;

public enum RequestType {
    GET, PUT, POST, DELETE;
}
