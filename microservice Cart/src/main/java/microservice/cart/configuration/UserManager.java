package microservice.cart.configuration;

import microservice.cart.configuration.requestMaker.RequestType;
import microservice.cart.dto.UserEmailAndPasswordDto;
import microservice.cart.dto.UserPersonalDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

@Component
@ConditionalOnProperty(value = "onlineshop.user.fetch.strategy", havingValue = "template", matchIfMissing = true)
public class UserManager extends MicroserviceRequest {

    @Value("${get.user.email.and.password}")
    private String GET_USER_EMAIL_AND_PASSWORD_LINK;
    @Value("${add.cart.to}")
    private String ADD_CART_TO_LINK;
    @Value("${get.personal.user}")
    private String GET_PERSONAL_USER_LINK;
    @Value("${lower.balance}")
    private String LOWER_BALANCE_LINK;
    @Value("${remove.cart.from}")
    private String REMOVE_CART_FROM;

    public UserEmailAndPasswordDto getUserEmailAndPassword(String username) throws IOException {
        Object returnedObject=requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_USER_EMAIL_AND_PASSWORD_LINK+username,
                NO_QUERY_PARAMS,
                NO_HEADERS);
        return convertToRequestedObject(returnedObject,UserEmailAndPasswordDto.class);
    }

    public void addCartTo(String userId, String cartId) {
        HashMap<String,String> headers=new HashMap<>();
        headers.put("cartId",cartId);

        requestSender.sendMessageReturningRequest(RequestType.PUT,
                ADD_CART_TO_LINK,
                NO_QUERY_PARAMS,
                headers);
    }

    public UserPersonalDto getPersonalUserDto(String orderOwnerUsername) throws IOException {
        Object returnedObject=requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_PERSONAL_USER_LINK + orderOwnerUsername,
                NO_QUERY_PARAMS,
                NO_HEADERS);
        return convertToRequestedObject(returnedObject,UserPersonalDto.class);
    }

    public String lowerBalance(String userId, String price) {
        HashMap<String,String> headers=new HashMap<>();
        headers.put("balance",price);
        String returnedString=requestSender.sendMessageReturningRequest(RequestType.PUT,
                LOWER_BALANCE_LINK + userId,
                NO_QUERY_PARAMS,
                headers);
        return returnedString;
    }

    public void removeCart(String orderId, String userId) {
        HashMap<String,String> headers=new HashMap<>();
        headers.put("orderId",orderId);
        requestSender.sendMessageReturningRequest(RequestType.DELETE,
                REMOVE_CART_FROM + userId,
                NO_QUERY_PARAMS,
                headers
                );
    }
}
