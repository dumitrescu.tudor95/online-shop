package microservice.cart.mapper;

import microservice.cart.dto.CartDto;
import microservice.cart.entity.Cart;

import java.util.ArrayList;

public class CartMapper {


    public static CartDto convertToDto(Cart cart) {

        return CartDto.builder()
                .cartId(cart.getId())
                .user(cart.getUserId())
                .productsId(new ArrayList<>(cart.getProductsId()))
                .price(cart.getPrice())
                .build();

    }

}
