package microservice.cart.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import microservice.cart.configuration.DiscountManager;
import microservice.cart.configuration.EmailSender;
import microservice.cart.configuration.ProductManager;
import microservice.cart.configuration.UserManager;
import microservice.cart.dto.CartDto;
import microservice.cart.dto.UserEmailAndPasswordDto;
import microservice.cart.entity.Cart;
import microservice.cart.exceptions.CartDoesNotExistException;
import microservice.cart.mapper.CartMapper;
import microservice.cart.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CartService {

    @Autowired
    CartRepository cartRepository;

    @Autowired
    EmailSender emailSender;

    @Autowired
    UserManager userManager;

    @Autowired
    ProductManager productManager;
    @Autowired
    DiscountManager discountManager;

    public List<CartDto> getAllCarts() {
        return cartRepository.findAll()
                .stream()
                .map(CartMapper::convertToDto)
                .collect(Collectors.toList());
    }

    public CartDto createCart(String user) throws IOException {

        UserEmailAndPasswordDto userToSendEmailTo = userManager.getUserEmailAndPassword(user);

        Cart newCart = new Cart();
        newCart.setUserId(user);
        newCart.setPrice(0.0);
        newCart.setProductsId(new ArrayList<>());
        Cart savedCart = cartRepository.save(newCart);

        //sending info email
        //todo  emailSender.sendEmail(userToSendEmailTo.getEmail(), "Cart created", "Your new cart has been successfully created.Feel free to checkout our products at http://localhost:9999/products");


        return CartMapper.convertToDto(savedCart);
    }


    public String deleteCart(String cartId) throws IOException {

        Optional<Cart> specificCart = cartRepository.findById(cartId);

        checkIfCartExists(specificCart);

        cartRepository.deleteById(cartId);

        UserEmailAndPasswordDto userToSendEmailTo = userManager.getUserEmailAndPassword(specificCart.get().getUserId());

        //todo  emailSender.sendEmail(userToSendEmailTo.get().getEmail(), "Cart deleted", "Your cart has been successfully deleted.\n For any complaints let us know at tudor_project_shop@mailtrap.com");

        return "Cart number " + cartId + " successfully deleted";
    }

    public CartDto resetCart(String orderId) {
        Optional<Cart> specificCart = cartRepository.findById(orderId);
        checkIfCartExists(specificCart);

        specificCart.get().setProductsId(new ArrayList<>());
        specificCart.get().setPrice(0.0);

        cartRepository.save(specificCart.get());

        return CartMapper.convertToDto(specificCart.get());
    }

    public CartDto getCartFrom(String userId) throws IOException {

        UserEmailAndPasswordDto specificUser = userManager.getUserEmailAndPassword(userId);

        Cart returnedCart = cartRepository.findByUserId(specificUser.getUsername());

        return CartMapper.convertToDto(returnedCart);

    }

    public String deleteCartFrom(String userId) {
        cartRepository.deleteByUserId(userId);
        return "Cart deleted";
    }

    public CartDto getSpecificCart(String cartId) {
        Optional<Cart> specificCart = cartRepository.findById(cartId);
        checkIfCartExists(specificCart);
        return CartMapper.convertToDto(specificCart.get());
    }

    public String addProductInCart(String cartId, String productId) throws JsonProcessingException {
        Optional<Cart> specificCart = cartRepository.findById(cartId);

        checkIfCartExists(specificCart);

        specificCart.get().getProductsId().add(productId);

        specificCart.get().setPrice(discountManager.getDiscount(CartMapper.convertToDto(specificCart.get())));

        cartRepository.save(specificCart.get());
        return "Product added in cart";
    }

    public String lowerCartPrice(String orderId, String price) {

        Optional<Cart> specificCart = cartRepository.findById(orderId);

        specificCart.get().setPrice(specificCart.get().getPrice() - Double.parseDouble(price));

        cartRepository.save(specificCart.get());

        return "Price lowered";
    }

    public String removeProductFromCart(String orderId, String productId) throws JsonProcessingException {
        Optional<Cart> specificCart = cartRepository.findById(orderId);

        specificCart.get().getProductsId().remove(productId);
        specificCart.get().setPrice(discountManager.getDiscount(CartMapper.convertToDto(specificCart.get())));
        cartRepository.save(specificCart.get());

        return "product removed from cart";
    }

    public void checkIfCartExists(Optional<Cart> optionalCart) {
        if (!optionalCart.isPresent()) {
            throw new CartDoesNotExistException("The cart you specified does not exist");
        }
    }


}

