package microservice.cart.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import microservice.cart.configuration.DiscountManager;
import microservice.cart.configuration.EmailSender;
import microservice.cart.configuration.UserManager;
import microservice.cart.dto.CartDto;
import microservice.cart.dto.UserEmailAndPasswordDto;
import microservice.cart.entity.Cart;
import microservice.cart.exceptions.CartDoesNotExistException;
import microservice.cart.mapper.CartMapper;
import microservice.cart.repository.CartRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CartServiceTest {

    @MockBean
    CartRepository cartRepository;
    @MockBean
    UserManager userManager;
    @MockBean
    EmailSender emailSender;
    @MockBean
    DiscountManager discountManager;

    @InjectMocks
    CartService cartService;

    Cart cart;
    String cartId = "testId";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        cart = new Cart();
        cart.setId(cartId);
        cart.setPrice(0.0);
        cart.setProductsId(new ArrayList<>());
        cart.setUserId("userId");
    }

    @Test
    public void getAllCarts() {
        List<Cart> list = new ArrayList<>();
        list.add(cart);
        List<CartDto> returnedList = new ArrayList<>();
        returnedList.add(CartMapper.convertToDto(cart));

        when(cartRepository.findAll()).thenReturn(list);

        assertEquals(cartService.getAllCarts(), returnedList);
        verify(cartRepository, times(1)).findAll();
    }

    @Test
    public void createCart() throws IOException {
        CartDto returnedCart = CartMapper.convertToDto(cart);

        when(cartRepository.save(any())).thenReturn(cart);

        assertEquals(cartService.createCart(cartId), returnedCart);
        verify(cartRepository, times(1)).save(any());
        //todo verify(emailSender,times(1)).sendEmail(x,y,z);
    }

    @Test(expected = CartDoesNotExistException.class)
    public void deleteCartWhenItDoesNotExist() throws IOException {
        when(cartRepository.findById(anyString())).thenReturn(Optional.ofNullable(null));

        cartService.deleteCart(cartId);
        verify(cartRepository, times(1)).findById(anyString());
    }

    @Test
    public void deleteCart() throws IOException {
        cart.setUserId("userId");

        when(cartRepository.findById(anyString())).thenReturn(Optional.of(cart));

        cartService.deleteCart(cartId);
        verify(cartRepository, times(1)).findById(anyString());
        verify(userManager, times(1)).getUserEmailAndPassword(anyString());
        verify(cartRepository, times(1)).deleteById(anyString());
    }

    @Test(expected = CartDoesNotExistException.class)
    public void resetCartWhenCartDoesNotExist() {
        when(cartRepository.findById(anyString())).thenReturn(Optional.ofNullable(null));

        cartService.resetCart(cartId);
        verify(cartRepository, times(1)).findById(anyString());
    }

    @Test
    public void resetCart() {
        CartDto returnedCart = CartDto.builder()
                .cartId(cartId)
                .user("userId")
                .productsId(new ArrayList<>())
                .price(0.0)
                .build();

        when(cartRepository.findById(anyString())).thenReturn(Optional.of(cart));

        assertEquals(cartService.resetCart(cartId), returnedCart);
        verify(cartRepository, times(1)).findById(anyString());
        verify(cartRepository, times(1)).save(any());
    }

    @Test
    public void getCartFrom() throws IOException {
        CartDto returnedCart = CartMapper.convertToDto(cart);

        UserEmailAndPasswordDto returnedUser = new UserEmailAndPasswordDto();
        returnedUser.setUsername("userId");

        when(cartRepository.findByUserId(anyString())).thenReturn(cart);
        when(userManager.getUserEmailAndPassword((anyString()))).thenReturn(returnedUser);

        assertEquals(cartService.getCartFrom("userId"), returnedCart);
        verify(userManager, times(1)).getUserEmailAndPassword(anyString());
        verify(cartRepository, times(1)).findByUserId(anyString());
    }

    @Test
    public void deleteCartFrom() {
        cartService.deleteCartFrom("userId");
        verify(cartRepository, times(1)).deleteByUserId(anyString());
    }

    @Test
    public void getSpecificCart() {
        CartDto returnedCart = CartMapper.convertToDto(cart);

        when(cartRepository.findById(anyString())).thenReturn(Optional.of(cart));

        assertEquals(cartService.getSpecificCart(cartId), returnedCart);
        verify(cartRepository, times(1)).findById(anyString());

    }

    @Test(expected = CartDoesNotExistException.class)
    public void getSpecificCartWhenItDoesNotExist() {

        when(cartRepository.findById(anyString())).thenReturn(Optional.ofNullable(null));

        cartService.getSpecificCart(cartId);

        verify(cartRepository, times(1)).findById(anyString());
    }

    @Test
    public void addProductInCart() throws JsonProcessingException {

        when(cartRepository.findById(anyString())).thenReturn(Optional.of(cart));
        when(discountManager.getDiscount(any())).thenReturn(100.0);

        cartService.addProductInCart(cartId, "validProductId");
        verify(cartRepository, times(1)).findById(anyString());
        verify(cartRepository, times(1)).save(any());
    }

    @Test(expected = CartDoesNotExistException.class)
    public void addProductInCartWhenCartDoesNotExist() throws JsonProcessingException {

        when(cartRepository.findById(anyString())).thenReturn(Optional.empty());

        cartService.addProductInCart(cartId, "validProductId");
        verify(cartRepository, times(1)).findById(anyString());
    }

    @Test
    public void lowerCartPrice() {
        when(cartRepository.findById(anyString())).thenReturn(Optional.of(cart));

        cartService.lowerCartPrice(cartId,"100.0");
        verify(cartRepository,times(1)).findById(anyString());
        verify(cartRepository,times(1)).save(any());
    }

    @Test
    public void removeProductFromCart() {
    }

    @Test
    public void checkIfCartExists() {
    }
}