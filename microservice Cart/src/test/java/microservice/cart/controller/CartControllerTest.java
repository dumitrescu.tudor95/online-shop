package microservice.cart.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import microservice.cart.dto.CartDto;
import microservice.cart.exceptions.CartDoesNotExistException;
import microservice.cart.service.CartService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CartController.class)
public class CartControllerTest {

    @MockBean
    CartService cartService;

    @Autowired
    MockMvc mockMvc;

    CartDto cartDto;
    String cartId = "cartIdTest";

    ObjectMapper mapper;

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
        cartDto = new CartDto();
        cartDto.setCartId(cartId);
    }


    @Test
    public void getAllCarts() throws Exception {

        List<CartDto> cartDtoList = new ArrayList<>();
        cartDtoList.add(cartDto);
        cartDtoList.add(new CartDto());

        String stringJson = mapper.writeValueAsString(cartDtoList);


        when(cartService.getAllCarts()).thenReturn(cartDtoList);

        mockMvc.perform(get("/carts"))
                .andExpect(status().isOk())
                .andExpect(content().string(stringJson));

    }


    @Test
    public void getSpecificCartHappyCase() throws Exception {
        String jsonString = mapper.writeValueAsString(cartDto);

        when(cartService.getSpecificCart(anyString())).thenReturn(cartDto);

        mockMvc.perform(get("/carts/getCart").param("cartId", cartId))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));

    }
    @Test
    public void getSpecificCartExceptionThrown() throws Exception {
        when(cartService.getSpecificCart(anyString())).thenThrow(CartDoesNotExistException.class);

        mockMvc.perform(get("carts/getCart").param("cartId", cartId))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void addCart() throws Exception {
        String jsonString = mapper.writeValueAsString(cartDto);

        when(cartService.createCart(anyString())).thenReturn(cartDto);

        mockMvc.perform(post("/carts/addCart").param("userId", "testUserId"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));

    }


    @Test
    public void deleteCartHappyCase() throws Exception {
        when(cartService.deleteCart(cartId)).thenReturn("Card number " + cartId + " successfully deleted");

        mockMvc.perform(delete("/carts/removeCart").param("cartId", cartId))
                .andExpect(status().isOk())
                .andExpect(content().string("Card number " + cartId + " successfully deleted"));
    }
    @Test
    public void deleteCartErrorThrown() throws Exception {
        when(cartService.deleteCart(cartId)).thenThrow(CartDoesNotExistException.class);

        mockMvc.perform(delete("/carts/removeCart").param("cartId", cartId))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void resetCartHappyCase() throws Exception {
        String jsonString = mapper.writeValueAsString(cartDto);

        when(cartService.resetCart(cartId)).thenReturn(cartDto);

        mockMvc.perform(put("/carts/resetCart").param("cartId", cartId))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }
    @Test
    public void resetCartErrorThrown() throws Exception {

        when(cartService.resetCart(cartId)).thenThrow(CartDoesNotExistException.class);

        mockMvc.perform(put("/carts/resetCart").param("cartId", cartId))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void getCartFromHappyCase() throws Exception {
        cartDto.setUser("testUser");

        String jsonString = mapper.writeValueAsString(cartDto);

        when(cartService.getCartFrom(anyString())).thenReturn(cartDto);

        mockMvc.perform(get("/getCartFrom/{userId}", "testUser"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }
    @Test
    public void getCartFromExceptionThrown() throws Exception {
        when(cartService.getCartFrom(anyString())).thenThrow(CartDoesNotExistException.class);

        mockMvc.perform(get("/getCartFrom/{userId}", "testUser"))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void deleteCartFromHappyCase() throws Exception {
        cartDto.setUser("testUser");

        when(cartService.deleteCartFrom(anyString())).thenReturn("Cart deleted");

        mockMvc.perform(delete("/deleteCartFrom/{userId}", "testUser"))
                .andExpect(status().isOk())
                .andExpect(content().string("Cart deleted"));
    }
    @Test
    public void deleteCartFromErrorThrown() throws Exception {

        when(cartService.deleteCartFrom(anyString())).thenThrow(CartDoesNotExistException.class);

        mockMvc.perform(delete("/deleteCartFrom/{userId}", "testUser"))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void addProductInCartHappyCase() throws Exception {

        when(cartService.addProductInCart(cartId, "someValidProductId")).thenReturn("Product added in cart");

        mockMvc.perform(put("/addProductInCart")
                .param("cartId", cartId)
                .param("productId", "someValidProductId"))
                .andExpect(status().isOk())
                .andExpect(content().string("Product added in cart"));
    }
    @Test
    public void addProductInCartErrorThrown() throws Exception {

        when(cartService.addProductInCart("someInvalidCartId", "whateverProductId"))
                .thenThrow(CartDoesNotExistException.class);

        mockMvc.perform(put("/addProductInCart")
                .param("cartId", "someInvalidCartId")
                .param("productId", "whateverProductId"))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void removeProductFromCartHappyCase() throws Exception {
        when(cartService.removeProductFromCart(cartId, "someValidProductId")).thenReturn("product removed from cart");

        mockMvc.perform(delete("/removeProductFromCart")
                .param("cartId", cartId)
                .param("productId", "someValidProductId"))
                .andExpect(status().isOk())
                .andExpect(content().string("product removed from cart"));
    }
    @Test
    public void removeProductFromCartErrorThrown() throws Exception {
        when(cartService.removeProductFromCart("someInvalidCartId", "someValidProductId"))
                .thenThrow(CartDoesNotExistException.class);

        mockMvc.perform(delete("/removeProductFromCart")
                .param("cartId", "someInvalidCartId")
                .param("productId", "someValidProductId"))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void lowerCartPrice() throws Exception {
        when(cartService.lowerCartPrice(anyString(), anyString())).thenReturn("Price lowered");

        mockMvc.perform(put("/lowerCartPrice/{cartId}", cartId)
                .header("price", "any price value"))
                .andExpect(status().isOk())
                .andExpect(content().string("Price lowered"));
    }
    @Test
    public void lowerCartPriceErrorThrown() throws Exception {
        when(cartService.lowerCartPrice(anyString(), anyString())).thenThrow(CartDoesNotExistException.class);

        mockMvc.perform(put("/lowerCartPrice/{cartId}", "someInvalidCartId")
                .header("price", "any price value"))
                .andExpect(status().is4xxClientError());
    }
}