package product.product.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import product.product.entity.Reservation;

@Repository
public interface ReservationRepository extends MongoRepository<Reservation, String> {

}
