package product.product.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import product.product.dto.ProductDto;
import product.product.service.ProductService;

import java.io.IOException;
import java.util.List;

@RestController
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public List<ProductDto> showAllProducts() {
        return productService.showAllProducts();
    }

    @GetMapping("/products/{productId}")
    public ProductDto getSpecificProduct(@PathVariable("productId") String productId) {
        return productService.getProduct(productId);
    }

    @GetMapping("/products/category/{category}")
    public List<ProductDto> listProductsByCategory(@PathVariable("category") String category) {
        return productService.listByCategory(category);
    }

    @PostMapping("/addProduct")
    public ProductDto addProduct(@RequestBody ProductDto productDto) {
        return productService.addProductInStock(productDto);
    }

    @DeleteMapping("/removeProduct/{productId}")
    public String removeProduct(@PathVariable("productId") String productId) {
        return productService.removeProduct(productId);
    }

    @PutMapping("/addProductInCart")
    public String addProductInCart(@RequestParam("cartId") String cartId,
                                   @RequestParam("productId") String productId) throws IOException {
        return productService.addProductInCart(cartId, productId);
    }

    @PutMapping("/removeProductFromCart")
    public String removeProductFromCart(@RequestParam("cartId") String cartId,
                                        @RequestParam("productId") String productId) {
        return productService.removeProductFromCart(cartId, productId);
    }


    @GetMapping("/getProductCountByCategory")
    public Long getProductCount(@RequestParam("categoryName") String categoryName) {
        return productService.getProductCount(categoryName);
    }

    @PutMapping("/raiseReviewCount")
    public String raiseReviewCount(@RequestParam("productName") String productName) {
        return productService.raiseReviewCount(productName);
    }

    @PutMapping("/putProductBackInShop")
    public String putProductBackInShop(@RequestParam("user") String user,
                                     @RequestParam("productId") String productId) {
       return productService.putProductBackInShop(user, productId);
    }
    @DeleteMapping("/removeReservation")
    public String removeReservation(@RequestParam("user") String user,
                                    @RequestParam("productId") String productId){
        return productService.cancelReservation(user,productId);
    }
}
