package product.product.mapper;

import product.product.dto.ProductDto;
import product.product.entity.Product;

public class ProductMapper {

    public static ProductDto convertToDto(Product product){
        return ProductDto.builder()
                .product_id(product.getId())
                .name(product.getName())
                .categoryId(product.getProductCategoryId())
                .details(product.getDescription())
                .price(product.getPrice())
                .reviews(product.getReviews())
                .countInStock(product.getCountInStock())
                .build();
    }

    public static Product convertToEntity(ProductDto productDto){
        Product product=new Product();
        product.setId(productDto.getProduct_id());
        product.setName(productDto.getName());
        product.setDescription(productDto.getDetails());
        product.setProductCategoryId(productDto.getCategoryId());
        product.setPrice(productDto.getPrice());
        product.setReviews(productDto.getReviews());
        product.setCountInStock(productDto.getCountInStock());
        return product;
    }
}
