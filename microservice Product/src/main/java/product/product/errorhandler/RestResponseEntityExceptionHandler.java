package product.product.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleMethodArgumentNotValid(HttpServletRequest request, Exception ex) {

        System.out.println("======================== ERROR THROWN  ============================");

        System.out.println(ex.getMessage());

        ErrorDTO error = new ErrorDTO();
        error.setStatusCode("404");
        error.setMessage(ex.getMessage());

        System.out.println(error.getMessage());

        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

}