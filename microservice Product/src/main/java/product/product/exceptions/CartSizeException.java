package product.product.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CartSizeException extends RuntimeException {
    public CartSizeException(String message){
        super(message);
    }
}
