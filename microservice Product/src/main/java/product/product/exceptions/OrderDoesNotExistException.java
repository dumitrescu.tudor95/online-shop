package product.product.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class OrderDoesNotExistException extends RuntimeException {
    public OrderDoesNotExistException(String message){
        super(message);
    }
}
