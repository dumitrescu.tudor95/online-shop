package product.product.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import product.product.configuration.requestMaker.RequestType;

import java.util.HashMap;

@Component
public class ProductCategoryManager extends MicroserviceRequest {

    @Value("${add.product.category}")
    private String ADD_PRODUCT_CATEGORY_LINK;

    public void addCategory(String categoryName) {
        HashMap<String,String> queryParams=new HashMap<>();
        queryParams.put("categoryName",categoryName);
        requestSender.sendMessageReturningRequest(RequestType.POST,
                ADD_PRODUCT_CATEGORY_LINK,
                queryParams,
                NO_HEADERS);
    }
}
