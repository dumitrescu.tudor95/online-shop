package product.product.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import product.product.configuration.requestMaker.RequestType;
import product.product.dto.CartDto;

import java.io.IOException;
import java.util.HashMap;

@Component
public class CartManager extends MicroserviceRequest {

    @Value("${get.cart}")
    private String GET_CART_LINK;
    @Value("${add.product.in.cart}")
    private String ADD_PRODUCT_IN_CART_LINK;
    @Value("${raise.cart.price}")
    private String RAISE_CART_PRICE_LINK;
    @Value("${lower.cart.price}")
    private String LOWER_CART_PRICE_LINK;
    @Value("${remove.product.from.cart}")
    private String REMOVE_PRODUCT_FROM_CART_LINK;

    public CartDto getCartById(String cartId) throws IOException {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("cartId", cartId);

        Object returnedObject = requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_CART_LINK,
                queryParams,
                NO_HEADERS);
        return convertToRequestedObject(returnedObject, CartDto.class);
    }

    public void addProductInCart(String productId, String cartId) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("cartId", cartId);
        queryParams.put("productId",productId);

        requestSender.sendMessageReturningRequest(RequestType.PUT,
                ADD_PRODUCT_IN_CART_LINK,
                queryParams,
                NO_HEADERS);
    }

    public void raiseCartPrice(String cartId, String price) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("price", price);

        requestSender.sendMessageReturningRequest(RequestType.PUT,
                RAISE_CART_PRICE_LINK+cartId,
                NO_QUERY_PARAMS,
                headers);
    }

    public void lowerCartPrice(String cartId, String price) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("price", price);
        requestSender.sendMessageReturningRequest(RequestType.PUT,
                LOWER_CART_PRICE_LINK + cartId,
                NO_QUERY_PARAMS,
                headers);
    }

    public String removeProductFromCart(String productId, String cartId) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("cartId", cartId);
        queryParams.put("productId",productId);

        String returnedString=requestSender.sendMessageReturningRequest(RequestType.DELETE,
                REMOVE_PRODUCT_FROM_CART_LINK,
                queryParams,
                NO_HEADERS);
        return returnedString;
    }


}
