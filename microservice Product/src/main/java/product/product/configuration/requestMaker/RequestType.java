package product.product.configuration.requestMaker;

public enum RequestType {
    GET, PUT, POST, DELETE;
}
