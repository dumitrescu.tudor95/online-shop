package product.product.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Document(collection = "ProductDatabase")
public class Product {

    @Id
    private String id;

    private String name;

    private String productCategoryId;

    private String description;

    private Double price;

    private Integer reviews;

    private Integer countInStock;
}
