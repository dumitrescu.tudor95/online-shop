package product.product.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import product.product.dto.ProductDto;

@AllArgsConstructor
@NoArgsConstructor
@Document
@Getter
@Setter
@ToString
public class Reservation {
    @Id
    private String id;

    private String futureOwnerId;
    private ProductDto reservedProduct;
}
