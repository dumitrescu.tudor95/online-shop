package product.product.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import product.product.configuration.CartManager;
import product.product.configuration.DiscountManager;
import product.product.configuration.ProductCategoryManager;
import product.product.dto.CartDto;
import product.product.dto.ProductDto;
import product.product.entity.Product;
import product.product.entity.Reservation;
import product.product.event.GoodsFetchedEvent;
import product.product.exceptions.CartSizeException;
import product.product.exceptions.ProductDoesNotExistException;
import product.product.mapper.ProductMapper;
import product.product.repository.ProductRepository;
import product.product.repository.ReservationRepository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;
    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    CartManager cartManager;
    @Autowired
    ProductCategoryManager productCategoryManager;
    @Autowired
    DiscountManager discountManager;

    ObjectMapper mapper;

    @RabbitListener(queues = "GOODS_FETCHED_EVENTS")
    void fetchGoods(String message) throws IOException {
        mapper = new ObjectMapper();
        GoodsFetchedEvent event = mapper.readValue(message, GoodsFetchedEvent.class);
        System.out.println(message);
        for (String productId : event.getProducts()) {
            Optional<Product> specificProduct = productRepository.findById(productId);
            System.out.println("This product should be taken care of " + productId);

            createReservation(event.getFutureOwner(), ProductMapper.convertToDto(specificProduct.get()));

            if (specificProduct.get().getCountInStock() > 1) {
                specificProduct.get().setCountInStock(specificProduct.get().getCountInStock() - 1);
                productRepository.save(specificProduct.get());
            } else {
                productRepository.deleteById(productId);
            }
        }

    }

    public void createReservation(String futureOwner, ProductDto reservedProduct) {
        Reservation reservation = new Reservation();
        reservation.setFutureOwnerId(futureOwner);
        reservation.setReservedProduct(reservedProduct);
        reservationRepository.save(reservation);
    }

    public List<ProductDto> showAllProducts() {
        return productRepository.findAll()
                .stream()
                .map(ProductMapper::convertToDto)
                .collect(Collectors.toList());
    }

    public String addProductInCart(String cartId, String productId) throws IOException {

        CartDto specificCart = cartManager.getCartById(cartId);

        Optional<Product> specificProduct = productRepository.findById(productId);

        checkIfProductExists(specificProduct);

        if (specificCart.getProductsId().size() >= 3) {
            throw new CartSizeException("Carts can have a number of maximum 3 products");
        }

        //add product in order and raise price
        cartManager.addProductInCart(productId, cartId);

        productRepository.save(specificProduct.get());

        return specificProduct.get().getName() + " successfully added to your order";
    }

    public String removeProductFromCart(String cartId, String productId) {

        Optional<Product> specificProduct = productRepository.findById(productId);

        checkIfProductExists(specificProduct);

        //remove product from order and lower price
        cartManager.removeProductFromCart(productId, cartId);

        productRepository.save(specificProduct.get());

        return specificProduct.get().getName() + " successfully removed from your order";
    }

    public ProductDto addProductInStock(ProductDto productDto) {
        Optional<Product> specificProduct = productRepository.findByName(productDto.getName());

        productCategoryManager.addCategory(productDto.getCategoryId());

        Product product = ProductMapper.convertToEntity(productDto);
        product.setReviews(0);
        product.setCountInStock(1);

        Product savedProduct;

        if (specificProduct.isPresent()) {
            specificProduct.get().setCountInStock(specificProduct.get().getCountInStock() + 1);
            savedProduct = productRepository.save(specificProduct.get());
        } else {
            savedProduct = productRepository.save(product);
        }
        return ProductMapper.convertToDto(savedProduct);

    }

    public List<ProductDto> listByCategory(String category) {
        return productRepository.findAll()
                .stream()
                .filter(product -> product.getProductCategoryId().equals(category))
                .map(ProductMapper::convertToDto)
                .collect(Collectors.toList());
    }

    public String removeProduct(String productId) {

        Optional<Product> specificProduct = productRepository.findById(productId);

        if (specificProduct.get().getCountInStock() > 0) {
            specificProduct.get().setCountInStock(specificProduct.get().getCountInStock() - 1);
            productRepository.save(specificProduct.get());
        } else {
            productRepository.deleteById(specificProduct.get().getId());
        }

        return specificProduct.get().getName() + " removed";
    }

    public ProductDto getProduct(String productId) {
        Optional<Product> specificProduct = productRepository.findById(productId);
        checkIfProductExists(specificProduct);

        return ProductMapper.convertToDto(specificProduct.get());
    }

    public Long getProductCount(String categoryName) {
        return productRepository.findByProductCategoryId(categoryName)
                .stream()
                .count();
    }

    public void checkIfProductExists(Optional<Product> optionalProduct) {
        if (!optionalProduct.isPresent()) {
            throw new ProductDoesNotExistException("The product you specified does not exist");
        }
    }


    public String raiseReviewCount(String productName) {
        Optional<Product> specificProduct = productRepository.findByName(productName);
        checkIfProductExists(specificProduct);
        specificProduct.get().setReviews(specificProduct.get().getReviews() + 1);
        productRepository.save(specificProduct.get());
        return "Count raised";
    }

    public String putProductBackInShop(String user, String productId) {

        Reservation specificReservation = reservationRepository.findAll()
                .stream()
                .filter(reservation -> reservation.getReservedProduct().getProduct_id().equals(productId) &&
                        reservation.getFutureOwnerId().equals(user))
                .findFirst()
                .get();
        ProductDto specificProduct = specificReservation.getReservedProduct();
        System.out.println(specificProduct);

        addProductInStock(specificProduct);

        reservationRepository.delete(specificReservation);
        return "Product put back";
    }

    public String cancelReservation(String user, String productId) {
        Reservation specificReservation = reservationRepository.findAll()
                .stream()
                .filter(reservation -> reservation.getReservedProduct().getProduct_id().equals(productId) &&
                        reservation.getFutureOwnerId().equals(user))
                .findFirst()
                .get();
        reservationRepository.delete(specificReservation);
        return "Reservation canceled";
    }
}
