package product.product.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import product.product.dto.ProductDto;
import product.product.exceptions.GeneralException;
import product.product.service.ProductService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    @MockBean
    ProductService productService;

    @Autowired
    MockMvc mockMvc;

    ProductDto productDto;
    String productId = "productIdTest";
    ObjectMapper mapper;

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
        productDto = new ProductDto();
        productDto.setProduct_id(productId);
    }

    @Test
    public void showAllProducts() throws Exception {

        List<ProductDto> productList = new ArrayList<>();
        productList.add(productDto);

        String jsonString = mapper.writeValueAsString(productList);

        when(productService.showAllProducts()).thenReturn(productList);

        mockMvc.perform(get("/products"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void getSpecificProduct() throws Exception {

        String jsonString = mapper.writeValueAsString(productDto);

        when(productService.getProduct(anyString())).thenReturn(productDto);

        mockMvc.perform(get("/products/{productId}", productId))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void getSpecificProductErrorThrown() throws Exception {
        when(productService.getProduct(anyString())).thenThrow(GeneralException.class);
        mockMvc.perform(get("/products/{productId}", productId))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void listProductsByCategory() throws Exception {
        List<ProductDto> productDtoList = new ArrayList<>();
        productDtoList.add(productDto);

        String jsonString = mapper.writeValueAsString(productDtoList);

        when(productService.listByCategory(anyString()))
                .thenReturn(productDtoList);

        mockMvc.perform(get("/products/category/{category}", "randomCategory"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void addProductHappyCase() throws Exception {

        String jsonString = mapper.writeValueAsString(productDto);

        when(productService.addProductInStock(productDto)).thenReturn(productDto);

        mockMvc.perform(post("/addProduct")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void addProductExceptionThrown() throws Exception {

        String jsonString = mapper.writeValueAsString(productDto);

        when(productService.addProductInStock(productDto)).thenThrow(GeneralException.class);

        mockMvc.perform(post("/addProduct")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void removeProductHappyCase() throws Exception {
        when(productService.removeProduct(anyString())).thenReturn("Product removed");

        mockMvc.perform(delete("/removeProduct/{productId}", "validId"))
                .andExpect(status().isOk())
                .andExpect(content().string("Product removed"));
    }

    @Test
    public void removeProductErrorThrown() throws Exception {
        when(productService.removeProduct(anyString())).thenThrow(GeneralException.class);

        mockMvc.perform(delete("/removeProduct/{productId}", "validId"))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void addProductInCartHappyCase() throws Exception {

        when(productService.addProductInCart(anyString(), anyString()))
                .thenReturn("Product added");

        mockMvc.perform(put("/addProductInCart")
                .param("cartId", "someValidCartId")
                .param("productId", "someValidProductId"))
                .andExpect(status().isOk())
                .andExpect(content().string("Product added"));
    }

    @Test
    public void addProductInCartErrorThrown() throws Exception {

        when(productService.addProductInCart(anyString(), anyString()))
                .thenThrow(GeneralException.class);

        mockMvc.perform(put("/addProductInCart")
                .param("cartId", "valid/invalid")
                .param("productId", "valid/invalid"))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void removeProductFromCartHappyCase() throws Exception {
        when(productService.removeProductFromCart(anyString(), anyString()))
                .thenReturn("Product removed");

        mockMvc.perform(put("/removeProductFromCart")
                .param("cartId", "someValidCartId")
                .param("productId", "someValidProductId"))
                .andExpect(status().isOk())
                .andExpect(content().string("Product removed"));
    }

    @Test
    public void removeProductFromCartExceptionThrown() throws Exception {
        when(productService.removeProductFromCart(anyString(), anyString()))
                .thenThrow(GeneralException.class);

        mockMvc.perform(put("/removeProductFromCart")
                .param("cartId", "valid/invalid")
                .param("productId", "valid/invalid"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void getProductCount() throws Exception {

        when(productService.getProductCount(anyString())).thenReturn(1L);

        mockMvc.perform(get("/getProductCountByCategory")
                .param("categoryName", "randomString"))
                .andExpect(status().isOk());

    }
}