package product.product.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import product.product.configuration.CartManager;
import product.product.configuration.ProductCategoryManager;
import product.product.dto.CartDto;
import product.product.dto.ProductDto;
import product.product.entity.Product;
import product.product.exceptions.CartSizeException;
import product.product.exceptions.ProductDoesNotExistException;
import product.product.mapper.ProductMapper;
import product.product.repository.ProductRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ProductServiceTest {

    @MockBean
    ProductRepository productRepository;
    @MockBean
    CartManager cartManager;
    @MockBean
    ProductCategoryManager productCategoryManager;

    @InjectMocks
    ProductService productService;


    Product product;
    String productId = "testId";
    CartDto cartDto;
    String cartId = "cartId";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        product = new Product();
        product.setId(productId);
        product.setPrice(10.0);
        cartDto = new CartDto();
        cartDto.setCartId(cartId);
        cartDto.setProductsId(new ArrayList<>());

    }

    @Test
    public void showAllProducts() {
        List<Product> list = new ArrayList<>();
        list.add(product);

        List<ProductDto> returnedList = list.stream()
                .map(ProductMapper::convertToDto)
                .collect(Collectors.toList());

        when(productRepository.findAll()).thenReturn(list);

        assertEquals(productService.showAllProducts(), returnedList);

        verify(productRepository, times(1)).findAll();
    }

    @Test(expected = ProductDoesNotExistException.class)
    public void addProductInCartWhenProductDoesNotExist() throws IOException {

        when(cartManager.getCartById(anyString())).thenReturn(cartDto);
        when(productRepository.findById(anyString())).thenReturn(Optional.ofNullable(null));

        productService.addProductInCart("someInvalidId", "productId");

        verify(cartManager, times(1)).getCartById((anyString()));
        verify(productRepository, times(1)).findById(anyString());
    }

    @Test(expected = CartSizeException.class)
    public void addProductInCartWhenThereAreMoreThan3ProductsInCart() throws IOException {
        cartDto.getProductsId().add("product1");
        cartDto.getProductsId().add("product2");
        cartDto.getProductsId().add("product3");

        when(cartManager.getCartById(anyString())).thenReturn(cartDto);
        when(productRepository.findById(anyString())).thenReturn(Optional.of(product));

        productService.addProductInCart(cartId, productId);

        verify(cartManager, times(1)).getCartById((anyString()));
        verify(productRepository, times(1)).findById(anyString());
    }

    @Test
    public void addProductInCart() throws IOException {

        when(cartManager.getCartById(anyString())).thenReturn(cartDto);
        when(productRepository.findById(anyString())).thenReturn(Optional.of(product));

        productService.addProductInCart(cartId, productId);

        verify(cartManager, times(1)).getCartById(anyString());
        verify(cartManager, times(1)).addProductInCart(anyString(), anyString());
        verify(cartManager, times(1)).raiseCartPrice(anyString(), anyString());
        verify(productRepository, times(1)).save(any());
        verify(productRepository, times(1)).findById(anyString());
    }

    @Test(expected = ProductDoesNotExistException.class)
    public void removeProductFromCartWhenProductDoesNotExist() {

        when(productRepository.findById(anyString())).thenReturn(Optional.ofNullable(null));

        productService.removeProductFromCart(cartId, "invalidProductId");

        verify(productRepository, times(1)).findById(anyString());

    }

    @Test
    public void removeProductFromCart() {

        when(productRepository.findById(anyString())).thenReturn(Optional.of(product));

        productService.removeProductFromCart(cartId, productId);

        verify(productRepository, times(1)).findById(anyString());
        verify(cartManager, times(1)).removeProductFromCart(anyString(), anyString());
        verify(cartManager, times(1)).lowerCartPrice(anyString(), anyString());
        verify(productRepository, times(1)).save(any());

    }

    @Test
    public void addProductInStock() {
        product.setProductCategoryId("testCategoryId");
        ProductDto productDto = ProductMapper.convertToDto(product);

        when(productRepository.save(any())).thenReturn(product);

        assertEquals(productService.addProductInStock(productDto), productDto);

        verify(productCategoryManager, times(1)).addCategory(anyString());
        verify(productRepository, times(1)).save(any());
    }

    @Test
    public void listByCategory() {
        product.setProductCategoryId("TEST");

        Product newProduct = new Product();
        newProduct.setProductCategoryId("SOME OTHER ID");

        List<Product> list = new ArrayList<>();
        list.add(product);
        list.add(newProduct);

        List<ProductDto> returnedList = new ArrayList<>();
        returnedList.add(ProductMapper.convertToDto(product));

        when(productRepository.findAll()).thenReturn(list);

        assertEquals(productService.listByCategory("TEST"), returnedList);

        verify(productRepository, times(1)).findAll();
    }


    @Test
    public void removeProduct() {

        when(productRepository.findById(anyString())).thenReturn(Optional.of(product));

        productService.removeProduct(productId);

        verify(productRepository, times(1)).deleteById(anyString());

    }

    @Test(expected = ProductDoesNotExistException.class)
    public void getProductWhenItDoesNotExist() {
        when(productRepository.findById(anyString())).thenReturn(Optional.ofNullable(null));

        productService.getProduct(productId);

        verify(productRepository,times(1)).findById(anyString());
    }
    @Test
    public void getProduct(){
        ProductDto returnedProduct = ProductMapper.convertToDto(product);

        when(productRepository.findById(anyString())).thenReturn(Optional.of(product));

        assertEquals(productService.getProduct(productId),returnedProduct);
        verify(productRepository,times(1)).findById(anyString());
    }

    @Test
    public void getProductCount() {
        List<Product> returnedList=new ArrayList<>();
        returnedList.add(product);
        returnedList.add(new Product());

        when(productRepository.findByProductCategoryId(anyString()))
                .thenReturn(returnedList);

        Long returnedCount= 2L;
        assertEquals(productService.getProductCount("givenName"),returnedCount);
        verify(productRepository,times(1)).findByProductCategoryId(anyString());
    }

    @Test(expected = ProductDoesNotExistException.class)
    public void checkIfProductExists() {
        productService.checkIfProductExists(Optional.ofNullable(null));
    }
}