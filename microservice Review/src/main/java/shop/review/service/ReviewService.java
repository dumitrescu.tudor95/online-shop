package shop.review.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shop.review.configuration.ProductManager;
import shop.review.configuration.UserManager;
import shop.review.dto.ReviewDto;
import shop.review.entity.Review;
import shop.review.exceptions.PermissionException;
import shop.review.mapper.ReviewMapper;
import shop.review.repository.ReviewRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReviewService {

    @Autowired
    ReviewRepository reviewRepository;

    @Autowired
    ProductManager productManager;
    @Autowired
    UserManager userManager;

    public ReviewDto saveReview(ReviewDto reviewDto) {

        userManager.checkIfUserExists(reviewDto.getUser());

        boolean userAlreadyReviewedThisProduct = reviewRepository.findAll()
                .stream()
                .anyMatch(review -> review.getUser().equals(reviewDto.getUser())
                        &&
                        review.getProductName().equals(reviewDto.getProductName()));
        if (userAlreadyReviewedThisProduct) {
            throw new PermissionException("You cannot review the same product twice");
        }

        Review review = new Review();
        review.setProductName(reviewDto.getProductName());
        review.setUser(reviewDto.getUser());
        review.setComment(reviewDto.getComment());
        review.setStars(reviewDto.getStars());

        Review savedReview = reviewRepository.save(review);

        productManager.raiseReviewCount(reviewDto.getProductName());

        return ReviewMapper.convertToDto(savedReview);
    }

    public List<ReviewDto> showReviewsFor(String productName) {
        return reviewRepository.findByProductName(productName)
                .stream()
                .map(ReviewMapper::convertToDto)
                .collect(Collectors.toList());
    }
}
