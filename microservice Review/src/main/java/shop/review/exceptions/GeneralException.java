package shop.review.exceptions;

public class GeneralException extends RuntimeException {
    public GeneralException(String message){
        super(message);
    }
}
