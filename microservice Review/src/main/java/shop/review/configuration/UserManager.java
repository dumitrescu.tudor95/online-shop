package shop.review.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import shop.review.configuration.requestMaker.RequestType;

@Component
public class UserManager extends MicroserviceRequest {

    @Value("${check.user}")
    private String CHECK_USER_LINK;

    public void checkIfUserExists(String user) {
        requestSender.sendMessageReturningRequest(RequestType.GET,
                CHECK_USER_LINK+user,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
    }
}
