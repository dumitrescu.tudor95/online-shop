package shop.review.configuration.requestMaker;

public enum RequestType {
    GET, PUT, POST, DELETE;
}
