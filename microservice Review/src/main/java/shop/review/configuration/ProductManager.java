package shop.review.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import shop.review.configuration.requestMaker.RequestType;

import java.util.HashMap;

@Component

public class ProductManager extends MicroserviceRequest {

    @Value("${raise.review.count}")
    private String RAISE_REVIEW_COUNT_LINK;

    public void raiseReviewCount(String productName) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("productName", productName);
        requestSender.sendMessageReturningRequest(RequestType.PUT,
                RAISE_REVIEW_COUNT_LINK,
                queryParams,
                NO_HEADERS);

    }
}
