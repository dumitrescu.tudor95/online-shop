package shop.review.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import shop.review.entity.Review;

import java.util.List;

@Repository
public interface ReviewRepository extends MongoRepository<Review,String> {

    List<Review> findByProductName(String productName);

}
