package shop.review.mapper;

import shop.review.dto.ReviewDto;
import shop.review.entity.Review;

public class ReviewMapper {

    public static ReviewDto convertToDto(Review review){
        ReviewDto dto=new ReviewDto();
        dto.setUser(review.getUser());
        dto.setProductName(review.getProductName());
        dto.setStars(review.getStars());
        dto.setComment(review.getComment());

        return dto;
    }
}
