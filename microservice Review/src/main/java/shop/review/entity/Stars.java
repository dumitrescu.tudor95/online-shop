package shop.review.entity;

import lombok.Getter;

@Getter
public enum Stars {
    ZERO_STARS(0.0),
    HALF_A_STAR(0.5),
    A_STAR(1.0),
    A_STAR_AND_A_HALF(1.5),
    TWO_STARS(2.0),
    TWO_STARS_AND_A_HALF(2.5),
    THREE_STARS(3.0),
    THREE_STARS_AND_A_HALF(3.5),
    FOUR_STARS(4.0),
    FOUR_STARS_AND_A_HALF(4.5),
    FIVE_STARS(5.0);

    private Double value;

    Stars(Double value) {
        this.value = value;
    }
}
