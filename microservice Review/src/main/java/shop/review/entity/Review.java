package shop.review.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor
@AllArgsConstructor
@Document
@Getter
@Setter
public class Review {

    @Id
    private String id;

    private String user;
    private String productName;
    private Stars stars;
    private String comment;

}
