package shop.review.errorhandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.micrometer.core.instrument.util.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;
import shop.review.exceptions.GeneralException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

@Component
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse httpResponse)
            throws IOException {

        return (httpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR
                || httpResponse.getStatusCode().series() == SERVER_ERROR);
    }


    @Override
    public void handleError(ClientHttpResponse httpResponse) throws IOException {

        if (httpResponse.getStatusCode()
                .series() == SERVER_ERROR) {
            // handle SERVER_ERROR

            System.out.println("======================== SERVER ERROR ============================");

            String jsonString = IOUtils.toString(httpResponse.getBody(), StandardCharsets.UTF_8);
            ObjectMapper mapper = new ObjectMapper();
            ErrorDTO errorDTO = mapper.readValue(jsonString, ErrorDTO.class);

            throw new GeneralException(errorDTO.getMessage());

        } else if (httpResponse.getStatusCode()
                .series() == HttpStatus.Series.CLIENT_ERROR) {
            // handle CLIENT_ERROR

            System.out.println("======================== CLIENT ERROR ============================ " + httpResponse.getStatusCode().value());

            String jsonString = IOUtils.toString(httpResponse.getBody(), StandardCharsets.UTF_8);
            ObjectMapper mapper = new ObjectMapper();
            ErrorDTO errorDTO = mapper.readValue(jsonString, ErrorDTO.class);
            System.out.println(errorDTO.getMessage());
            throw new GeneralException(errorDTO.getMessage());
        }
    }
}
