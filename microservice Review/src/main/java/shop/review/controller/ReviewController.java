package shop.review.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import shop.review.dto.ReviewDto;
import shop.review.service.ReviewService;

import java.util.List;

@RestController
public class ReviewController {

    @Autowired
    ReviewService reviewService;

    @PostMapping("/addReview")
    public ReviewDto addReview(@RequestBody ReviewDto reviewDto) {
        return reviewService.saveReview(reviewDto);
    }

    @GetMapping("/showReviewsFor")
    public List<ReviewDto> showReviewsFor(@RequestParam("productName") String productName) {
        return reviewService.showReviewsFor(productName);
    }

}
