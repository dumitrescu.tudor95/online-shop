package microservice.discount.service;

import microservice.discount.configuration.ProductManager;
import microservice.discount.dto.CartDto;
import microservice.discount.dto.ProductDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class DiscountServiceTest {


    @MockBean
    ProductManager productManager;

    @InjectMocks
    DiscountService discountService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getDiscount() throws IOException {
        CartDto cartDto = new CartDto();
        List<String> productIds = new ArrayList<>();
        productIds.add("testId");
        cartDto.setProductsId(productIds);

        ProductDto productDto = new ProductDto();
        productDto.setPrice(1.0);

        when(productManager.getProductDto(anyString())).thenReturn(productDto);

        Double returnedDouble=1.0;
        assertEquals(discountService.getDiscount(cartDto),returnedDouble);

    }

    @Test
    public void createDiscount() {
    }

    @Test
    public void processDiscount() {
    }
}