package microservice.discount.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import microservice.discount.dto.CartDto;
import microservice.discount.service.DiscountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DiscountController.class)
public class DiscountControllerTest {

    @MockBean
    DiscountService discountService;

    @Autowired
    MockMvc mockMvc;

    ObjectMapper mapper;

    CartDto cartDto;


    @Before
    public void setUp() throws Exception {
    mapper=new ObjectMapper();
    cartDto=new CartDto();
    cartDto.setCartId("testId");
    }

    @Test
    public void returnFinalPriceAfterApplyingDiscount() throws Exception {

        Double returnedValue = 2.0;
        String jsonString= mapper.writeValueAsString(cartDto);
        when(discountService.getDiscount(any())).thenReturn(String.valueOf(returnedValue));

        mockMvc.perform(post("/returnFinalPriceAfterApplyingDiscount")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonString))
                .andExpect(status().isOk());
    }
}