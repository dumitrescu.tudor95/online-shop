package microservice.discount.controller;

import microservice.discount.dto.CartDto;
import microservice.discount.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class DiscountController {

    @Autowired
    DiscountService discountService;

    @PostMapping("/returnFinalPriceAfterApplyingDiscount")
    public String returnFinalPriceAfterApplyingDiscount(@RequestBody CartDto cartDto) throws IOException {
        return discountService.getDiscount(cartDto);
    }


}
