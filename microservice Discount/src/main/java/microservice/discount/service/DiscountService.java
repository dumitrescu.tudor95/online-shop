package microservice.discount.service;

import microservice.discount.configuration.ProductManager;
import microservice.discount.dto.CartDto;
import microservice.discount.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class DiscountService {

    @Autowired
    ProductManager productManager;

    public String getDiscount(CartDto cartDto) throws IOException {

        Double finalPrice = 0.0;

        System.out.println(cartDto.getProductsId());

        for (String productId : cartDto.getProductsId()) {
            ProductDto product=productManager.getProductDto(productId);
            finalPrice += product.getPrice() - createDiscount(product.getPrice());
        }
        return String.valueOf(finalPrice);
    }

    public Double createDiscount(Double productPrice) {
        Double discount = 0.0;
        if (productPrice >= 100 && productPrice <= 500) {
            discount = processDiscount(productPrice, 10.0);
        } else if (productPrice > 1000 && productPrice <= 5000) {
            discount = processDiscount(productPrice, 20.0);
        } else if (productPrice > 5000 && productPrice <= 10000) {
            discount = processDiscount(productPrice, 25.0);
        } else if (productPrice > 10000) {
            discount = processDiscount(productPrice, 30.0);
        }
        return discount;
    }

    public Double processDiscount(Double productPrice, Double percentage) {
        return (percentage / 100) * productPrice;
    }
}
