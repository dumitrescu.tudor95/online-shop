package microservice.discount.configuration;

import microservice.discount.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ProductManager {

    @Autowired
    RestTemplate restTemplate;

    public ProductDto getProductDto(String productId) {
        restTemplate = new RestTemplate();

        //RequestHeaders
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        HttpEntity httpEntity = new HttpEntity(headers);

        ResponseEntity<ProductDto> exchangeResponse = restTemplate.exchange(
                "http://localhost:8084/products/" + productId,
                HttpMethod.GET,
                httpEntity,
                ProductDto.class
        );
        return exchangeResponse.getBody();
    }
}
