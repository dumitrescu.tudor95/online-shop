package tudor.onlineshop.onlineshop.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tudor.onlineshop.onlineshop.configuration.MicroserviceProductRelations;
import tudor.onlineshop.onlineshop.configuration.ShopStock;
import tudor.onlineshop.onlineshop.domain.dto.ProductDto;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    @MockBean
    MicroserviceProductRelations productRelations;
    @MockBean
    ShopStock stock;

    @Autowired
    MockMvc mockMvc;

    ProductDto productDto;
    String productId="productId";

    ObjectMapper mapper;

    @Before
    public void setUp() throws Exception {
    mapper=new ObjectMapper();
    productDto=new ProductDto();
    productDto.setProduct_id(productId);
    }

    @Test
    public void getAllProducts() throws Exception {
        List<ProductDto> returnedList=new ArrayList<>();
        returnedList.add(productDto);

        String jsonString=mapper.writeValueAsString(returnedList);

        when(stock.getAllProducts()).thenReturn(returnedList);

        mockMvc.perform(get("/products"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));

    }

    @Test
    public void getProduct() throws Exception {

        String jsonString=mapper.writeValueAsString(productDto);

        when(stock.getProduct(anyString())).thenReturn(productDto);

        mockMvc.perform(get("/getProduct")
        .param("productId","someValidProductId"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void getProductsByCategory() {
    }

    @Test
    public void addProduct() {
    }

    @Test
    public void showMyStock() {
    }

    @Test
    public void addProductInCart() {
    }

    @Test
    public void removeProductFromCart() {
    }
}