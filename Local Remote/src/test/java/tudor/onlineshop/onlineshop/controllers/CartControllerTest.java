package tudor.onlineshop.onlineshop.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tudor.onlineshop.onlineshop.configuration.MicroserviceCartRelations;
import tudor.onlineshop.onlineshop.domain.dto.CartDto;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CartController.class)
public class CartControllerTest {

    @MockBean
    MicroserviceCartRelations cartRelations;

    @Autowired
    MockMvc mockMvc;

    CartDto cartDto;
    String cartId = "cartId";
ObjectMapper mapper;
    @Before
    public void setUp() {
        mapper=new ObjectMapper();
        cartDto = new CartDto();
        cartDto.setCartId(cartId);
    }

    @Test
    public void getAllCarts() throws Exception {

        List<CartDto> returnedList = new ArrayList<>();
        returnedList.add(cartDto);

        String jsonString=mapper.writeValueAsString(returnedList);


        when(cartRelations.getAllCarts()).thenReturn(returnedList);

        mockMvc.perform(get("/carts"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void getCartFrom() throws Exception {

        String jsonString=mapper.writeValueAsString(cartDto);

        when(cartRelations.getCartFrom(anyString())).thenReturn(cartDto);

        mockMvc.perform(get("/getCartFrom/{userId}","someValidUserId"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void resetCart() throws Exception {
        String jsonString=mapper.writeValueAsString(cartDto);

        when(cartRelations.resetCart(anyString())).thenReturn(cartDto);

        mockMvc.perform(put("/carts/resetCart/")
        .param("cartId","someValidUserId"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void deleteCart() throws Exception {

        when(cartRelations.deleteCart(anyString())).thenReturn("Cart deleted");

        mockMvc.perform(put("/carts/removeCart")
                .param("cartId","someValidUserId"))
                .andExpect(status().isOk())
                .andExpect(content().string("Cart deleted"));
    }
}