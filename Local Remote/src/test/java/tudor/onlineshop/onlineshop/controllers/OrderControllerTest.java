package tudor.onlineshop.onlineshop.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tudor.onlineshop.onlineshop.configuration.MicroserviceOrderRelations;
import tudor.onlineshop.onlineshop.domain.dto.OrderDto;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    @MockBean
    MicroserviceOrderRelations orderRelations;

    @Autowired
    MockMvc mockMvc;

    OrderDto orderDto;
    String orderId = "orderId";

    ObjectMapper mapper;

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
        orderDto = new OrderDto();
        orderDto.setOrderId(orderId);
    }

    @Test
    public void createOrder() throws Exception {
//
//        String jsonString = mapper.writeValueAsString(orderDto);
//
//        when(orderRelations.createOrder(anyString())).thenReturn(orderDto);
//
//        mockMvc.perform(post("/createOrder")
//                .param("cartId", "validCartId"))
//                .andExpect(status().isOk())
//                .andExpect(content().string(jsonString));
    }

    @Test
    public void getAllOrders() throws Exception {
        List<OrderDto> returnedList = new ArrayList<>();
        returnedList.add(orderDto);

        String jsonString=mapper.writeValueAsString(returnedList);
        when(orderRelations.getAllOrders()).thenReturn(returnedList);

        mockMvc.perform(get("/getAllOrders"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }
}