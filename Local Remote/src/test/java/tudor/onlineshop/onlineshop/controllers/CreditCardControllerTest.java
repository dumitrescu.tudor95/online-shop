package tudor.onlineshop.onlineshop.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tudor.onlineshop.onlineshop.configuration.MicroserviceCardRelations;
import tudor.onlineshop.onlineshop.domain.dto.CreditCardDto;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CreditCardController.class)
public class CreditCardControllerTest {

    @MockBean
    MicroserviceCardRelations cardRelations;

    @Autowired
    MockMvc mockMvc;

    CreditCardDto cardDto;
    String cardId="cardId";
    ObjectMapper mapper;

    @Before
    public void setUp() throws Exception {
        mapper=new ObjectMapper();
    cardDto=new CreditCardDto();
    cardDto.setCardNumber(cardId);
    }

    @Test
    public void addCreditCardTo() throws Exception {

        when(cardRelations.addCreditCard(anyString(),anyString(),anyString(),anyString(),anyString(),anyString()))
                .thenReturn("Card added");

        mockMvc.perform(post("/addCreditCardTo/{userId}","userId")
        .header("creditCardNumber",cardId)
        .header("numberBack","123")
        .header("ownerName","ownerName")
        .header("expirationMonth","12")
        .header("expirationYear","2000"))
                .andExpect(status().isOk())
                .andExpect(content().string("Card added"));
    }

    @Test
    public void getCreditCard() throws Exception {
        String jsonString=mapper.writeValueAsString(cardDto);

        when(cardRelations.getCreditCardFrom(anyString())).thenReturn(cardDto);

        mockMvc.perform(get("/getCreditCard/{userId}","userId")
                .header("creditCardNumber",cardId))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void addBalance() throws Exception {
//        when(cardRelations.addBalance(anyString(),anyString()))
//                .thenReturn("Balance added");
//
//
//        mockMvc.perform(post("/addBalance/{userId}","userId")
//        .header("amount","100.0"))
//                .andExpect(status().isOk())
//                .andExpect(content().string("Balance added"));
    }
}