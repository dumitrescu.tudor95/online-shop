package tudor.onlineshop.onlineshop.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tudor.onlineshop.onlineshop.configuration.MicroservicePayment;
import tudor.onlineshop.onlineshop.domain.dto.PaymentDto;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PaymentController.class)
public class PaymentControllerTest {


    @MockBean
    MicroservicePayment paymentRelations;

    @Autowired
    MockMvc mockMvc;

    ObjectMapper mapper;
    PaymentDto paymentDto;
    String paymentId = "paymentId";


    @Before
    public void setUp() throws Exception {
        mapper = new ObjectMapper();
        paymentDto = new PaymentDto();
        paymentDto.setPaymentId(paymentId);
    }

    @Test
    public void executePayment() throws Exception {

        String jsonString = mapper.writeValueAsString(paymentDto);

        when(paymentRelations.createPayment(any())).thenReturn(paymentDto);

        mockMvc.perform(post("/executePayment")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonString))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void showAllPayments() throws Exception {
        List<PaymentDto> returnedList=new ArrayList<>();
        returnedList.add(paymentDto);

        String jsonString=mapper.writeValueAsString(returnedList);

        when(paymentRelations.showAllPayments()).thenReturn(returnedList);

        mockMvc.perform(get("/payments"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }
}