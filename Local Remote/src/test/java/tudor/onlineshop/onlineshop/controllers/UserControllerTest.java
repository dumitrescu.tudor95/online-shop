package tudor.onlineshop.onlineshop.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tudor.onlineshop.onlineshop.configuration.MicroserviceUserRelations;
import tudor.onlineshop.onlineshop.domain.dto.UserDto;
import tudor.onlineshop.onlineshop.domain.dto.UserPersonalDto;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @MockBean
    MicroserviceUserRelations userRelations;
    @Autowired
    MockMvc mockMvc;

    ObjectMapper mapper;

    UserDto userDto;
    String username = "usernameTest";

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
        userDto = new UserDto();
        userDto.setUsername(username);
    }

    @Test
    public void getAllUsers() throws Exception {
        List<UserDto> returnedList = new ArrayList<>();
        returnedList.add(userDto);

        String jsonString = mapper.writeValueAsString(returnedList);

        when(userRelations.getAllUsers()).thenReturn(returnedList);

        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void addUser() throws Exception {
        String jsonString = mapper.writeValueAsString(userDto);

        when(userRelations.createUser(any(), any(), any())).thenReturn(userDto);

        mockMvc.perform(post("/addUser")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonString)
                .header("password", "password")
                .header("confirmPassword", "password"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void updateUser() throws Exception {
        userDto.setEmail("test");
        String jsonString = mapper.writeValueAsString(userDto);

        when(userRelations.updateUser(any())).thenReturn(userDto);

        mockMvc.perform(put("/updateUser")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonString))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void deleteUser() throws Exception {
        when(userRelations.deleteUser(any())).thenReturn("User deleted");

        mockMvc.perform(delete("/delete/{userId}", username))
                .andExpect(status().isOk())
                .andExpect(content().string("User deleted"));
    }

    @Test
    public void showMyProfile() throws Exception {
        UserPersonalDto myPage = new UserPersonalDto();
        myPage.setUsername(username);

        String jsonString = mapper.writeValueAsString(myPage);

        when(userRelations.showMyProfile(anyString())).thenReturn(myPage);

        mockMvc.perform(get("/users/myPage/{userId}", username))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void getSpecificUser() throws Exception {
        String jsonString = mapper.writeValueAsString(userDto);

        when(userRelations.getSpecificUser(anyString())).thenReturn(userDto);

        mockMvc.perform(get("/users/{userId}", username))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void changePassword() throws Exception {
        when(userRelations.changePassword(anyString(), anyString())).thenReturn("Done");

        mockMvc.perform(put("/changePassword/{userId}", username)
                .header("newPassword", "newPassword"))
                .andExpect(status().isOk());
    }

    @Test
    public void sendPasswordViaEmail() throws Exception {
        when(userRelations.forgotMyPassword(anyString())).thenReturn("done");

        mockMvc.perform(get("/forgotMyPassword")
                .param("email", "testEmail@yahoo.com"))
                .andExpect(status().isOk());
    }
}