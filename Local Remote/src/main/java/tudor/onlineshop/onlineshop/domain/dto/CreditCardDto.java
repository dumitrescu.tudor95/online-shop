package tudor.onlineshop.onlineshop.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreditCardDto {

    private String cardNumber;
    private String ownerName;
    private Integer cardNumberBack;
    private String expirationDate;
    private String ownerUsername;



}
