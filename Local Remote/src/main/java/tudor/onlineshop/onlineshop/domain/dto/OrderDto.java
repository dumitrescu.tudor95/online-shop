package tudor.onlineshop.onlineshop.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class OrderDto {

    private String orderId;
    private Double orderPrice;
    private String user;
    private List<String> productIds;
    private Status availability;
    private LocalDateTime orderExpirationDate;

}

 enum Status {
    APROOVED,PENDING,EXPIRED;
}
