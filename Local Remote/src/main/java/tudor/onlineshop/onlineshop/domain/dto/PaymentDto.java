package tudor.onlineshop.onlineshop.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class PaymentDto {

    private String paymentId;
    private String user;
    private Double amount;
    private PaymentMethod paymentType;
    private AddressDto address;
    private String phoneNumber;
}

 enum PaymentMethod {
    CASH_ON_DELIVERY,CREDIT_CARD;
}
