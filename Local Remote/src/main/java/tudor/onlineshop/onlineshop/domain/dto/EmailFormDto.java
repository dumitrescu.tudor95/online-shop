package tudor.onlineshop.onlineshop.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmailFormDto {

    String from;
    String to;
    String subject;
    String message;
}
