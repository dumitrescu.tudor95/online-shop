package tudor.onlineshop.onlineshop.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CompletePaymentDto {

    private PaymentMethod paymentType;

    private String orderId;
    private String phoneNumber;

    private String country;
    private String city;
    private String street;
    private String number;
}
