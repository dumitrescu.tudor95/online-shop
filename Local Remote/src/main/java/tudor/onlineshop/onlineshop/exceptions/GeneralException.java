package tudor.onlineshop.onlineshop.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeneralException extends RuntimeException {

    public String statusCode;

    public GeneralException(String message,String statusCode){
        super(message);
        this.statusCode=statusCode;
    }
}