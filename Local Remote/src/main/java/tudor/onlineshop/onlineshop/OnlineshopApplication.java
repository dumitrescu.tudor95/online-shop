package tudor.onlineshop.onlineshop;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
public class OnlineshopApplication {

//    private static final String EXCHANGE_NAME = "spring-boot-exchange3";

//    ObjectMapper mapper = new ObjectMapper();

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ObjectMapper getObjectMapper(){
        return new ObjectMapper();
    }

//    @Bean
//    public void recieveMessagesSubscribe() throws Exception {
//
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("localhost");
//
//        Connection connection = factory.newConnection();
//        Channel channel = connection.createChannel();
//
//        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
//        String queueName = channel.queueDeclare().getQueue();
//        channel.queueBind(queueName, EXCHANGE_NAME, "");
//
//        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
//
//            String message = new String(delivery.getBody(), "UTF-8");
//
//            System.out.println(" [x] Received '" + message + "'");
//
//        };
//        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
//        });
//    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(OnlineshopApplication.class, args);


    }


}
