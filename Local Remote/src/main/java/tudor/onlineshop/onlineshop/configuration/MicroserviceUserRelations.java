package tudor.onlineshop.onlineshop.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tudor.onlineshop.onlineshop.configuration.requestMaker.NoHeaders;
import tudor.onlineshop.onlineshop.configuration.requestMaker.NoQueryParams;
import tudor.onlineshop.onlineshop.configuration.requestMaker.Request;
import tudor.onlineshop.onlineshop.configuration.requestMaker.RequestType;
import tudor.onlineshop.onlineshop.domain.dto.UserDto;
import tudor.onlineshop.onlineshop.domain.dto.UserPersonalDto;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Component
public class MicroserviceUserRelations  extends  MicroserviceRequest{

    @Autowired
    Request requestSender;

    @Value("${link.get.all.users}")
    private String GET_ALL_USERS_LINK;
    @Value("${create.user.link}")
    private String CREATE_USER_LINK;
    @Value("${update.user.link}")
    private String UPDATE_USER_LINK;
    @Value("${delete.user.link}")
    private String DELETE_USER_LINK;
    @Value("${my.profile.link}")
    private String MY_PROFILE_LINK;
    @Value("${specific.user.link}")
    private String SPECIFIC_USER_LINK;
    @Value("${password.change.link}")
    private  String PASSWORD_CHANGE_LINK;
    @Value("${password.forgotten.link}")
    private String PASSWORD_FORGOTTEN_LINK;

    public List<UserDto> getAllUsers() {
        List<UserDto> list = requestSender.sendListReturningRequest(RequestType.GET,
                GET_ALL_USERS_LINK,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return list;
    }

    public UserDto createUser(UserDto userDto, String password, String confirmPassword) throws IOException {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("password", password);
        headers.put("confirmPassword", confirmPassword);

        Object returnedObject = requestSender.sendBodyPassedRequest_recievingObject(RequestType.POST,
                CREATE_USER_LINK,
                userDto,
                NO_QUERY_PARAMS,
                headers
        );
        return convertToRequestedObject(returnedObject,UserDto.class);
    }

    public UserDto updateUser(UserDto userDto) throws IOException {
        Object returnedObject = requestSender.sendBodyPassedRequest_recievingObject(RequestType.PUT,
                UPDATE_USER_LINK,
                userDto,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return convertToRequestedObject(returnedObject,UserDto.class);
    }

    public String deleteUser(String userId) {

        Object returnedObject = requestSender.sendMessageReturningRequest(RequestType.DELETE,
                DELETE_USER_LINK + userId,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return returnedObject.toString();
    }

    public UserPersonalDto showMyProfile(String userId) throws IOException {

        Object returnedObject = requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                MY_PROFILE_LINK + userId,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return convertToRequestedObject(returnedObject,UserPersonalDto.class);
    }

    public UserDto getSpecificUser(String userId) throws IOException {

        Object returnedObject = requestSender.sendBodyPassedRequest_recievingObject(RequestType.GET,
                SPECIFIC_USER_LINK + userId,
                null,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return convertToRequestedObject(returnedObject,UserDto.class);
    }

    public String changePassword(String userId, String newPassword) {

        HashMap<String, String> headers = new HashMap<>();
        headers.put("password", newPassword);

        Object returnedObject = requestSender.sendMessageReturningRequest(RequestType.PUT,
                PASSWORD_CHANGE_LINK + userId,
                NO_QUERY_PARAMS,
                headers
        );
        return returnedObject.toString();
    }

    public String forgotMyPassword(String email) {

        HashMap<String, String> params = new HashMap<>();
        params.put("email", email);

        Object returnedObject = requestSender.sendMessageReturningRequest(RequestType.GET,
                PASSWORD_FORGOTTEN_LINK,
                params,
                NO_HEADERS
        );
        return returnedObject.toString();
    }


}
