package tudor.onlineshop.onlineshop.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tudor.onlineshop.onlineshop.configuration.requestMaker.NoHeaders;
import tudor.onlineshop.onlineshop.configuration.requestMaker.NoQueryParams;
import tudor.onlineshop.onlineshop.configuration.requestMaker.Request;
import tudor.onlineshop.onlineshop.configuration.requestMaker.RequestType;
import tudor.onlineshop.onlineshop.domain.dto.ProductDto;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Component
public class ShopStock extends MicroserviceRequest{

    @Autowired
    Request requestSender;

    @Value("${get.all.products}")
    private String GET_ALL_PRODUCTS_LINK;
    @Value("${get.product}")
    private String GET_PRODUCT_LINK;
    @Value("${get.products.by.categoty}")
    private String GET_PRODUCTS_BY_CATEGORY_LINK;
    @Value("${add.product}")
    private String ADD_PRODUCT_LINK;
    @Value("${product.count.in.stock}")
    private String PRODUCT_COUNT_IN_STOCK_LINK;


    public List<ProductDto> getAllProducts() {
        List<ProductDto> returnedList=requestSender.sendListReturningRequest(RequestType.GET,
                GET_ALL_PRODUCTS_LINK,
                NO_QUERY_PARAMS,
                NO_HEADERS
                );
                return returnedList;
    }

    public ProductDto getProduct(String productId) throws IOException {
        Object returnedObject=requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_PRODUCT_LINK + productId,
                NO_QUERY_PARAMS,
                NO_HEADERS
                );
        return mapper.readValue(mapper.writeValueAsString(returnedObject),ProductDto.class);
    }



    public List<ProductDto> getProductsByCategory(String category) {

        List<ProductDto> returnedList=requestSender.sendListReturningRequest(RequestType.GET,
                GET_PRODUCTS_BY_CATEGORY_LINK + category,
                NO_QUERY_PARAMS,
                NO_HEADERS
                );
        return returnedList;
    }

    public ProductDto addProductInShop(ProductDto productDto) throws IOException {

        Object returnedObject=requestSender.sendBodyPassedRequest_recievingObject(RequestType.POST,
                ADD_PRODUCT_LINK,
                productDto,
                NO_QUERY_PARAMS,
                NO_HEADERS
                );

        return mapper.readValue(mapper.writeValueAsString(returnedObject),ProductDto.class);
    }

    public String getProductCountInStock(String productName) {
        HashMap<String,String> queryParams = new HashMap<>();
        queryParams.put("productName", productName);
        String returnedString=requestSender.sendMessageReturningRequest(RequestType.GET,
                PRODUCT_COUNT_IN_STOCK_LINK,
                queryParams,
                NO_HEADERS
                );
        return returnedString;
    }
}
