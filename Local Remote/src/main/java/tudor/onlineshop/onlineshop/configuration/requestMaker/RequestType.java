package tudor.onlineshop.onlineshop.configuration.requestMaker;

public enum RequestType {
    GET, PUT, POST, DELETE;
}
