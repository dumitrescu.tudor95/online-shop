package tudor.onlineshop.onlineshop.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tudor.onlineshop.onlineshop.configuration.requestMaker.RequestType;
import tudor.onlineshop.onlineshop.domain.dto.CreditCardDto;

import java.io.IOException;
import java.util.HashMap;

@Component
public class MicroserviceCardRelations extends MicroserviceRequest {

    @Value("${add.credit.card}")
    private String ADD_CREDIT_CARD_LINK;
    @Value("${get.card.from}")
    private String GET_CARD_FROM_LINK;

    public String addCreditCard(String userId, String creditCardNumber, String numberBack, String ownerName, String expirationMonth, String expirationYear) {

        HashMap<String, String> headers = new HashMap<>();
        headers.put("creditCardNumber", creditCardNumber);
        headers.put("numberBack", numberBack);
        headers.put("ownerName", ownerName);
        headers.put("expirationMonth", expirationMonth);
        headers.put("expirationYear", expirationYear);

        Object returnedObject = requestSender.sendMessageReturningRequest(RequestType.POST,
                ADD_CREDIT_CARD_LINK + userId,
                NO_QUERY_PARAMS,
                headers
        );
        return returnedObject.toString();
    }

    public CreditCardDto getCreditCardFrom(String userId) throws InterruptedException, IOException {
        Object returnedObject= requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_CARD_FROM_LINK + userId,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return convertToRequestedObject(returnedObject,CreditCardDto.class);
    }




}
