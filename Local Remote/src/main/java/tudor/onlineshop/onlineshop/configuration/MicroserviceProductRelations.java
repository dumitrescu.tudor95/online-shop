package tudor.onlineshop.onlineshop.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tudor.onlineshop.onlineshop.configuration.requestMaker.Request;
import tudor.onlineshop.onlineshop.configuration.requestMaker.RequestType;

import java.util.HashMap;

@Component
public class MicroserviceProductRelations extends MicroserviceRequest{


    @Autowired
    Request requestSender;

    @Value("${add.product.in.cart}")
    private String ADD_PRODUCT_IN_CART_LINK;
    @Value("${remove.product.from.cart}")
    private String REMOVE_PRODUCT_FROM_CART;


    public String addProductInCart(String cartId, String productId) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("cartId", cartId);
        queryParams.put("productId", productId);

        String returnedString = requestSender.sendMessageReturningRequest(RequestType.PUT,
                ADD_PRODUCT_IN_CART_LINK,
                queryParams,
                NO_HEADERS
        );
        return returnedString;
    }

    public String removeProductFromCart(String orderId, String productId) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("cartId", orderId);
        queryParams.put("productId", productId);

        String returnedString = requestSender.sendMessageReturningRequest(RequestType.PUT,
                REMOVE_PRODUCT_FROM_CART,
                queryParams,
                NO_HEADERS
        );
        return returnedString;
    }
}
