package tudor.onlineshop.onlineshop.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tudor.onlineshop.onlineshop.configuration.requestMaker.NoHeaders;
import tudor.onlineshop.onlineshop.configuration.requestMaker.NoQueryParams;
import tudor.onlineshop.onlineshop.configuration.requestMaker.Request;
import tudor.onlineshop.onlineshop.configuration.requestMaker.RequestType;
import tudor.onlineshop.onlineshop.domain.dto.CartDto;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Component
public class MicroserviceCartRelations extends MicroserviceRequest {

    @Autowired
    Request requestSender;

    @Value("${get.all.carts}")
    private String GET_ALL_CARTS_LINK;
    @Value("${get.cart.from}")
    private String GET_CART_FROM_LINK;
    @Value("${reset.cart}")
    private String RESET_CART_LINK;
    @Value("${remove.cart}")
    private String REMOVE_CART_LINK;

    public List<CartDto> getAllCarts() {
        List<CartDto> returnedList = requestSender.sendListReturningRequest(RequestType.GET,
                GET_ALL_CARTS_LINK,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return returnedList;
    }

    public CartDto getCartFrom(String userId) throws IOException {
        Object returnedObject = requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_CART_FROM_LINK + userId,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return convertToRequestedObject(returnedObject , CartDto.class);
    }

    public CartDto resetCart(String cartId) throws IOException {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("cartId", cartId);
        Object returnedObject = requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.PUT,
                RESET_CART_LINK,
                queryParams,
                NO_HEADERS
        );
        return convertToRequestedObject(returnedObject, CartDto.class);
    }

    public String deleteCart(String cartId) throws IOException {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("cartId", cartId);
        Object returnedObject = requestSender.sendMessageReturningRequest(RequestType.GET,
                REMOVE_CART_LINK,
                queryParams,
                NO_HEADERS
        );
        return returnedObject.toString();
    }


}
