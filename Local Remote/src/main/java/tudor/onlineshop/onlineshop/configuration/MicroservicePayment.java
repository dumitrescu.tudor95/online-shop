package tudor.onlineshop.onlineshop.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tudor.onlineshop.onlineshop.configuration.requestMaker.NoHeaders;
import tudor.onlineshop.onlineshop.configuration.requestMaker.NoQueryParams;
import tudor.onlineshop.onlineshop.configuration.requestMaker.Request;
import tudor.onlineshop.onlineshop.configuration.requestMaker.RequestType;
import tudor.onlineshop.onlineshop.domain.dto.CompletePaymentDto;
import tudor.onlineshop.onlineshop.domain.dto.PaymentDto;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Component
public class MicroservicePayment extends MicroserviceRequest{

    @Autowired
    Request requestSender;

    @Value("${execute.payment}")
    private String EXECUTE_PAYMENT_LINK;
    @Value("${get.all.payments}")
    private String GET_ALL_PAYMENTS_LINK;
    @Value("${add.balance}")
    private String ADD_BALANCE_LINK;

    public PaymentDto createPayment(CompletePaymentDto completePaymentDto) throws IOException {

        Object returnedObject = requestSender.sendBodyPassedRequest_recievingObject(RequestType.POST,
                EXECUTE_PAYMENT_LINK,
                completePaymentDto,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return convertToRequestedObject(returnedObject, PaymentDto.class);
    }

    public List<PaymentDto> showAllPayments() {

        List<PaymentDto> returnedList = requestSender.sendListReturningRequest(RequestType.GET,
                GET_ALL_PAYMENTS_LINK,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return returnedList;
    }

    public String addBalance(String userId, String amount) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("user", userId);
        queryParams.put("amount", amount);

        String returnedString = requestSender.sendMessageReturningRequest(RequestType.POST,
                ADD_BALANCE_LINK,
                queryParams,
                NO_HEADERS
        );
        return returnedString;
    }
}
