package tudor.onlineshop.onlineshop.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tudor.onlineshop.onlineshop.configuration.requestMaker.NoHeaders;
import tudor.onlineshop.onlineshop.configuration.requestMaker.NoQueryParams;
import tudor.onlineshop.onlineshop.configuration.requestMaker.Request;
import tudor.onlineshop.onlineshop.configuration.requestMaker.RequestType;
import tudor.onlineshop.onlineshop.domain.dto.CreateOrderRequest;
import tudor.onlineshop.onlineshop.domain.dto.OrderDto;

import java.util.List;

@Component
public class MicroserviceOrderRelations extends MicroserviceRequest {

    @Autowired
    Request requestSender;

    @Value("${create.order}")
    private String CREATE_ORDER_LINK;
    @Value("${get.all.orders}")
    private String GET_ALL_ORDERS_LINK;


    public String createOrder(CreateOrderRequest orderRequest) {

        Object returnedObject = requestSender.sendBodyPassedRequest_recievingString(RequestType.POST,
                CREATE_ORDER_LINK,
                orderRequest,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return returnedObject.toString();
    }

    public List<OrderDto> getAllOrders() {
        List<OrderDto> returnedList = requestSender.sendListReturningRequest(RequestType.GET,
                GET_ALL_ORDERS_LINK,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return returnedList;
    }
}
