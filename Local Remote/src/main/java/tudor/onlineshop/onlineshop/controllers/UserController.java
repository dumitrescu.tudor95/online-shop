package tudor.onlineshop.onlineshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tudor.onlineshop.onlineshop.configuration.MicroserviceUserRelations;
import tudor.onlineshop.onlineshop.domain.dto.UserDto;
import tudor.onlineshop.onlineshop.domain.dto.UserPersonalDto;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    MicroserviceUserRelations userRelations;

    @GetMapping("/users")
    public List<UserDto> getAllUsers() {

        return userRelations.getAllUsers();

    }

    @PostMapping("/addUser")
    public UserDto addUser(@Valid @RequestBody UserDto userDto,
                           @RequestHeader("password") String password,
                           @RequestHeader("confirmPassword") String confirmPassword) throws IOException {
        UserDto response = userRelations.createUser(userDto, password, confirmPassword);
        return response;
    }


    @PutMapping("/updateUser")
    public UserDto updateUser(@RequestBody UserDto userDto) throws IOException {
        return userRelations.updateUser(userDto);
    }


    @DeleteMapping("/delete/{userId}")
    public String deleteUser(@PathVariable("userId") String userId) {
        return userRelations.deleteUser(userId);
    }


    @GetMapping("/users/myPage/{userId}")
    public UserPersonalDto showMyProfile(@PathVariable("userId") String userId) throws IOException {
        return userRelations.showMyProfile(userId);
    }


    @GetMapping("/users/{userId}")
    public UserDto getSpecificUser(@PathVariable("userId") String userId) throws IOException {
        return userRelations.getSpecificUser(userId);
    }


    @PutMapping("/changePassword/{userId}")
    public String changePassword(@PathVariable("userId") String userId,
                                 @RequestHeader("newPassword") String newPassword) {
        return userRelations.changePassword(userId, newPassword);
    }


    @GetMapping("/forgotMyPassword")
    public String sendPasswordViaEmail(@RequestParam("email") String email) {
        return userRelations.forgotMyPassword(email);
    }

}
