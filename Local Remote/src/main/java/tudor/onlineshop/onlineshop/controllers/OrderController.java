package tudor.onlineshop.onlineshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tudor.onlineshop.onlineshop.configuration.MicroserviceOrderRelations;
import tudor.onlineshop.onlineshop.domain.dto.CreateOrderRequest;
import tudor.onlineshop.onlineshop.domain.dto.OrderDto;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    MicroserviceOrderRelations orderRelations;

    @PostMapping("/createOrder")
    public String createOrder(@RequestBody CreateOrderRequest orderRequest){
        return orderRelations.createOrder(orderRequest);
    }

    @GetMapping("/getAllOrders")
    public List<OrderDto> getAllOrders(){
        return orderRelations.getAllOrders();
    }

}
