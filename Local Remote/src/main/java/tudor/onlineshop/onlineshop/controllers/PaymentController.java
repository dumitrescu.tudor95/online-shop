package tudor.onlineshop.onlineshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tudor.onlineshop.onlineshop.configuration.MicroservicePayment;
import tudor.onlineshop.onlineshop.domain.dto.CompletePaymentDto;
import tudor.onlineshop.onlineshop.domain.dto.PaymentDto;

import java.io.IOException;
import java.util.List;

@RestController
public class PaymentController {

    @Autowired
    MicroservicePayment paymentRelations;

    @PostMapping("/executePayment")
    public PaymentDto executePayment(@RequestBody CompletePaymentDto completePaymentDto) throws IOException {
        return paymentRelations.createPayment(completePaymentDto);
    }

    @GetMapping("/payments")
    public List<PaymentDto> showAllPayments(){
        return paymentRelations.showAllPayments();
    }
}
