package tudor.onlineshop.onlineshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import tudor.onlineshop.onlineshop.configuration.MicroserviceCardRelations;
import tudor.onlineshop.onlineshop.configuration.MicroservicePayment;
import tudor.onlineshop.onlineshop.domain.dto.CreditCardDto;

import java.io.IOException;

@RestController
public class CreditCardController {

    @Autowired
    MicroserviceCardRelations cardRelations;
    @Autowired
    MicroservicePayment paymentRelatioins;

    @PostMapping("/addCreditCardTo/{userId}")
    public String addCreditCardTo(@PathVariable("userId") String userId,
                                  @RequestHeader String creditCardNumber,
                                  @RequestHeader String numberBack,
                                  @RequestHeader String ownerName,
                                  @RequestHeader String expirationMonth,
                                  @RequestHeader String expirationYear) {
        return cardRelations.addCreditCard(userId, creditCardNumber, numberBack, ownerName, expirationMonth, expirationYear);

    }


    @GetMapping("/getCreditCard/{userId}")
    public CreditCardDto getCreditCard(@PathVariable("userId") String userId) throws InterruptedException, IOException {
        return cardRelations.getCreditCardFrom(userId);
    }


    @PutMapping("/addBalance/{userId}")
    public String addBalance(@PathVariable("userId") String userId,
                             @RequestHeader("amount") String amount) {
        return paymentRelatioins.addBalance(userId, amount);
    }
}
