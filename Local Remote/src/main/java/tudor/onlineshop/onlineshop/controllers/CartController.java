package tudor.onlineshop.onlineshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tudor.onlineshop.onlineshop.configuration.MicroserviceCartRelations;
import tudor.onlineshop.onlineshop.domain.dto.CartDto;

import java.io.IOException;
import java.util.List;

@RestController
public class CartController {

    @Autowired
    MicroserviceCartRelations cartRelations;

    @GetMapping("/carts")
    public List<CartDto> getAllCarts() {
        return cartRelations.getAllCarts();
    }


    @GetMapping("/getCartFrom/{userId}")
    public CartDto getCartFrom(@PathVariable("userId") String userId) throws IOException {
        return cartRelations.getCartFrom(userId);
    }

    @PutMapping("/carts/resetCart/")
    public CartDto resetCart(@RequestParam("cartId") String cartId) throws IOException {
        return cartRelations.resetCart(cartId);
    }

    @PutMapping("/carts/removeCart")
    public String deleteCart(@RequestParam("cartId") String cartId) throws IOException {
        return cartRelations.deleteCart(cartId);
    }


}
