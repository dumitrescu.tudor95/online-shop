package tudor.onlineshop.onlineshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tudor.onlineshop.onlineshop.configuration.MicroserviceProductRelations;
import tudor.onlineshop.onlineshop.configuration.ShopStock;
import tudor.onlineshop.onlineshop.domain.dto.ProductDto;

import java.io.IOException;
import java.util.List;

@RestController
public class ProductController {

    @Autowired
    MicroserviceProductRelations productRelations;
    @Autowired
    ShopStock stock;

    @GetMapping("/products")
    public List<ProductDto> getAllProducts() {
        return stock.getAllProducts();
    }

    @GetMapping("/getProduct")
    public ProductDto getProduct(@RequestParam("productId") String productId) throws IOException {
        return stock.getProduct(productId);
    }

    @GetMapping("/getItemCountInStock")
    public String getItemCountInStock(@RequestParam("productName") String productName){
        return stock.getProductCountInStock(productName);
    }

    @GetMapping("/products/category/{category}")
    public List<ProductDto> getProductsByCategory(@PathVariable("category") String category) {
        return stock.getProductsByCategory(category);
    }

    @PostMapping("/addProduct")
    public ProductDto addProduct(@RequestBody ProductDto productDto) throws IOException {
        return stock.addProductInShop(productDto);
    }


    @PutMapping("/addProductInCart")
    public String addProductInCart(@RequestParam("cartId") String cartId,
                                   @RequestParam("productId") String productId) {
        return productRelations.addProductInCart(cartId, productId);
    }

    @PutMapping("/removeProductFromCart")
    public String removeProductFromCart(@RequestParam("cartId") String cartId,
                                        @RequestParam("productId") String productId) {
        return productRelations.removeProductFromCart(cartId, productId);
    }



}
