package Shop.payment.exceptions;

public class GeneralException extends RuntimeException {
    public GeneralException(String message){
        super(message);
    }
}
