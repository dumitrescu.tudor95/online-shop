package Shop.payment.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class CreditCardNotFoundException extends RuntimeException{

    public CreditCardNotFoundException(String message){
        super(message);
    }

    public CreditCardNotFoundException(String message, Throwable t){
        super(message,t);
    }
}
