package Shop.payment.configuration;

import Shop.payment.configuration.requestMaker.RequestType;
import Shop.payment.dto.UserPersonalDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class MicroserviceUserRelations extends MicroserviceRequest {

    @Value("${get.personal.user}")
    private String GET_PERSONAL_USER_LINK;

    public UserPersonalDto getPersonalUser(String user) throws IOException {
        Object returnedObject = requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_PERSONAL_USER_LINK+user,
                NO_QUERY_PARAMS,
                NO_HEADERS);
        return convertToRequestedObject(returnedObject, UserPersonalDto.class);
    }
}
