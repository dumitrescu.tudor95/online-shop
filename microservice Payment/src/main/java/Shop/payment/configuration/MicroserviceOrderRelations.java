package Shop.payment.configuration;

import Shop.payment.configuration.requestMaker.RequestType;
import Shop.payment.dto.OrderDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

@Component
public class MicroserviceOrderRelations extends MicroserviceRequest {

    @Value("${get.order}")
    private String GET_ORDER_LINK;
    @Value("${remove.order}")
    private String REMOVE_ORDER_LINK;

    public OrderDto getOrder(String orderId) throws IOException {
        Object returnedObject = requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_ORDER_LINK + orderId,
                NO_QUERY_PARAMS,
                NO_HEADERS);
        return convertToRequestedObject(returnedObject, OrderDto.class);
    }

    public void removeOrder(String orderId) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("orderId", orderId);

        requestSender.sendMessageReturningRequest(RequestType.DELETE,
                REMOVE_ORDER_LINK,
                queryParams,
                NO_HEADERS);
    }
}
