package Shop.payment.configuration.requestMaker;

public enum RequestType {
    GET, PUT, POST, DELETE;
}
