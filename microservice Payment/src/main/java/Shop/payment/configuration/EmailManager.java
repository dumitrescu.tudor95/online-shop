package Shop.payment.configuration;

import Shop.payment.dto.EmailFormDTO;
import Shop.payment.errorhandler.RestTemplateResponseErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
public class EmailManager {

    @Autowired
    RestTemplate restTemplate;

    @Value("${send.email}")
    private String SEND_EMAIL_LINK;

    public String sendEmail(String specificUserEmail, String subject, String message) {
        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

        EmailFormDTO emailDTO = new EmailFormDTO();
        emailDTO.setFrom("tudor_project_shop@mailtrap.com");
        emailDTO.setTo(specificUserEmail);
        emailDTO.setSubject(subject);
        emailDTO.setMessage(message);


        restTemplate.postForEntity(SEND_EMAIL_LINK, emailDTO, String.class);


        return "Mail will be sent shortly";
    }

}
