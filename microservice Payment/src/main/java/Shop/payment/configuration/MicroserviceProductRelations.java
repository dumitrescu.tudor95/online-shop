package Shop.payment.configuration;

import Shop.payment.configuration.requestMaker.RequestType;
import Shop.payment.errorhandler.RestTemplateResponseErrorHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class MicroserviceProductRelations extends MicroserviceRequest {

    @Value("${put.product.back.in.shop}")
    private String PUT_PRODUCT_BACK_IN_SHOP_LINK;
    @Value("${remove.product}")
    private String REMOVE_PRODUCT_LINK;

    public void cancelReservationsFor(String user, String productId) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("user", user);
        queryParams.put("productId", productId);

        requestSender.sendMessageReturningRequest(RequestType.DELETE,
                PUT_PRODUCT_BACK_IN_SHOP_LINK + productId,
                queryParams,
                NO_HEADERS);
    }


    public void removeProductFromShop(String productId) {
        requestSender.sendMessageReturningRequest(RequestType.DELETE,
                REMOVE_PRODUCT_LINK + productId,
                NO_QUERY_PARAMS,
                NO_HEADERS);
    }
}
