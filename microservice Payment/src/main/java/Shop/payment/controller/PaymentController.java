package Shop.payment.controller;

import Shop.payment.dto.DepositDto;
import Shop.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @PostMapping("/createDeposit")
    public String createDeposit(@RequestParam("user") String user) {
        return paymentService.createDeposit(user);
    }

    @DeleteMapping("/deleteDeposit")
    public String deleteDeposit(@RequestParam("user") String user) {
        return paymentService.deleteDeposit(user);
    }

    @PostMapping("/addBalance")
    public String addBalance(@RequestParam("user") String user,
                             @RequestParam("amount") String amount) {
        return paymentService.addBalance(user, amount);
    }
    @PutMapping("/removeBalance")
    public void removeBalance(@RequestParam("user") String user,
                              @RequestParam("balance") String balance){
        paymentService.removeBalance(user,balance);
    }

    @GetMapping("/deposits")
    public List<DepositDto> showDeposits() {
        return paymentService.showDepositsList();
    }

    @GetMapping("/getDeposit")
    public String getDeposit(@RequestParam("userId") String userId){
        return paymentService.getDeposit(userId);
    }



}
