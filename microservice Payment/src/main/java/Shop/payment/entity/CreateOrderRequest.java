package Shop.payment.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CreateOrderRequest {

    private PaymentMethod paymentType;

    private String cartId;

    private String phoneNumber;

    private String country;
    private String city;
    private String street;
    private String number;
}
