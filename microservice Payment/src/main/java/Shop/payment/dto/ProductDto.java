package Shop.payment.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@EqualsAndHashCode
public class ProductDto {

    private String product_id;
    private String name;
    private String categoryId;
    private String details;
    private Double price;

}
