package Shop.payment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UserPersonalDto {


    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String creditCardId;
    private String cartId;

}