package Shop.payment.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class OrderDto {

    private String orderId;
    private String user;
    private Double orderPrice;
    private List<String> productIds;
    private Availability availability;
    private LocalDateTime orderExpirationDate;

}
