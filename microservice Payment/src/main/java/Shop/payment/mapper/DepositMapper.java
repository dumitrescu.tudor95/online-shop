package Shop.payment.mapper;

import Shop.payment.dto.DepositDto;
import Shop.payment.entity.Deposit;

public class DepositMapper {
    public static DepositDto convertToDto(Deposit payment) {
        return DepositDto.builder()
                .depositId(payment.getDepositId())
                .user(payment.getUserId())
                .balance(payment.getBalance())
                .build();
    }
}
