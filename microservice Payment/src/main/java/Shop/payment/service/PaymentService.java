package Shop.payment.service;

import Shop.payment.configuration.EmailManager;
import Shop.payment.configuration.MicroserviceProductRelations;
import Shop.payment.configuration.MicroserviceUserRelations;
import Shop.payment.dto.DepositDto;
import Shop.payment.dto.UserPersonalDto;
import Shop.payment.entity.Deposit;
import Shop.payment.entity.PaymentMethod;
import Shop.payment.event.CreateOrderEvent;
import Shop.payment.event.EventSender;
import Shop.payment.event.GoodsFetchedEvent;
import Shop.payment.event.PaymentProcessedEvent;
import Shop.payment.event.Status;
import Shop.payment.exceptions.PaymentNotSpecifiedException;
import Shop.payment.mapper.DepositMapper;
import Shop.payment.repository.DepositRepositoy;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PaymentService {

    @Autowired
    DepositRepositoy depositRepository;

    @Autowired
    MicroserviceUserRelations userRelations;

    @Autowired
    MicroserviceProductRelations productRelations;

    @Autowired
    EmailManager emailManager;

    @Autowired
    private EventSender eventSender;

    private ObjectMapper mapper;

    private static final String TOPIC_EXCHANGE_NAME_PAYMENT = "payment_sender";
    private static final String ROUTING_KEY_PAYMENT = "PAYMENT_EVENTS";


    private static final String TOPIC_EXCHANGE_NAME_GOODS_FETCHED = "goods_fetched_sender";
    private static final String ROUTING_KEY_GOODS_FETCHED = "GOODS_FETCHED_EVENTS";

    @RabbitListener(queues = "ORDER_EVENTS")
    public void recieveOrderEvent(String event) throws Exception {

        mapper = new ObjectMapper();
        CreateOrderEvent orderEvent = mapper.readValue(event, CreateOrderEvent.class);
        GoodsFetchedEvent goodsFetchedEvent = new GoodsFetchedEvent();
        goodsFetchedEvent.setProducts(orderEvent.getProductList());
        goodsFetchedEvent.setFutureOwner(orderEvent.getUser());
        eventSender.sendMessageInQueue(TOPIC_EXCHANGE_NAME_GOODS_FETCHED,
                ROUTING_KEY_GOODS_FETCHED,
                mapper.writeValueAsString(goodsFetchedEvent));

        if (orderEvent.getPaymentType() == PaymentMethod.CREDIT_CARD) {
            creditCardPay(orderEvent);
        } else if (orderEvent.getPaymentType() == PaymentMethod.CASH_ON_DELIVERY) {
            cashPay(orderEvent);
        } else {
            throw new PaymentNotSpecifiedException("You must specify the payment: CREDIT_CARD / CASH_ON_DELIVERY");
        }

    }

    public void creditCardPay(CreateOrderEvent orderEvent) throws Exception {

        UserPersonalDto orderOwner = userRelations.getPersonalUser(orderEvent.getUser());
        Deposit deposit = depositRepository.findByUserId(orderOwner.getUsername());  // !

        System.out.println(deposit);

        if (deposit.getBalance() >= orderEvent.getOrderPrice()) {
            deposit.setBalance(deposit.getBalance() - orderEvent.getOrderPrice());
            depositRepository.save(deposit);
            sendPaymentEvent(orderEvent);
        }
    }

    public void cashPay(CreateOrderEvent event) throws Exception {
        sendPaymentEvent(event);
    }

    public void sendPaymentEvent(CreateOrderEvent orderEvent) throws Exception {
        mapper = new ObjectMapper();

        PaymentProcessedEvent event = new PaymentProcessedEvent();
        event.setStatus(Status.APROOVED);
        event.setOrderId(orderEvent.getOrderId());
        eventSender.sendMessageInQueue(TOPIC_EXCHANGE_NAME_PAYMENT,
                ROUTING_KEY_PAYMENT,
                mapper.writeValueAsString(event));

        //todo emailManager.sendEmail(userToBeChecked.getEmail(),"Payment completed","Your payment for order "+orderToBePaid.getOrderId()+" has been made");
    }

    public List<DepositDto> showDepositsList() {
        return depositRepository.findAll()
                .stream()
                .map(DepositMapper::convertToDto)
                .collect(Collectors.toList());
    }

    public String createDeposit(String user) {
        Deposit deposit = new Deposit();
        deposit.setUserId(user);
        deposit.setBalance(0.0);
        depositRepository.save(deposit);
        return "Deposit created";
    }

    public String addBalance(String user, String amount) {
        Deposit specificDeposit = depositRepository.findByUserId(user);
        specificDeposit.setBalance(specificDeposit.getBalance() + Double.parseDouble(amount));
        depositRepository.save(specificDeposit);
        return "Balance added";
    }

    public String getDeposit(String userId) {
        return String.valueOf(depositRepository.findByUserId(userId).getBalance());
    }

    public void removeBalance(String user, String orderPrice) {
        Deposit specificDeposit = depositRepository.findByUserId(user);
        specificDeposit.setBalance(specificDeposit.getBalance() - Double.parseDouble(orderPrice));
    }

    public String deleteDeposit(String user) {
        depositRepository.deleteByUserId(user);
        return "Deposit deleted";
    }
}
