package Shop.payment.errorhandler;

import Shop.payment.exceptions.CreditCardNotFoundException;
import Shop.payment.exceptions.GeneralException;
import Shop.payment.exceptions.PaymentNotSpecifiedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler(value = {GeneralException.class, CreditCardNotFoundException.class, PaymentNotSpecifiedException.class})
    protected ResponseEntity<Object> handleMethodArgumentNotValid(HttpServletRequest request, Exception ex) {

        System.out.println("======================== Exception thrower ============================");

        ErrorDTO error = new ErrorDTO();
        error.setMessage(ex.getMessage());
        error.setStatusCode("404");


        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

}