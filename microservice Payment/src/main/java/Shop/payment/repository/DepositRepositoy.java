package Shop.payment.repository;

import Shop.payment.entity.Deposit;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepositRepositoy extends MongoRepository<Deposit,String> {

    Deposit findByUserId(String user);

    void deleteByUserId(String user);
}
