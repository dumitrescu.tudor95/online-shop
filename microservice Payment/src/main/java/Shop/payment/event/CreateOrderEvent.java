package Shop.payment.event;

import Shop.payment.entity.PaymentMethod;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CreateOrderEvent {

    private PaymentMethod paymentType;

    private String orderId;

    private String user;

    private Double orderPrice;

    private List<String> productList;
}
