package Shop.payment.event;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class EventSender {

    private final RabbitTemplate rabbitTemplate;

    public EventSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessageInQueue(String topicExchangeName,String routingKey, String paymentCreatedEvent) throws Exception {

        rabbitTemplate.convertAndSend(topicExchangeName, routingKey, paymentCreatedEvent);
        System.out.println("Message has been sent "+paymentCreatedEvent);
    }
}
