package Shop.payment.service;

import Shop.payment.configuration.MicroserviceOrderRelations;
import Shop.payment.configuration.MicroserviceProductRelations;
import Shop.payment.configuration.MicroserviceUserRelations;
import Shop.payment.dto.DepositDto;
import Shop.payment.dto.OrderDto;
import Shop.payment.dto.UserPersonalDto;
import Shop.payment.entity.Deposit;
import Shop.payment.entity.PaymentMethod;
import Shop.payment.event.CreateOrderEvent;
import Shop.payment.event.EventSender;
import Shop.payment.mapper.DepositMapper;
import Shop.payment.repository.DepositRepositoy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

//
//@RunWith(SpringRunner.class)
//public class PaymentServiceTest {
//
//    Deposit deposit;
//    String depositId = "testId";
//    OrderDto order;
//    String orderDtoId = "testIdOrder";
//    UserPersonalDto user;
//    String userId = "testIdUser";
//
//    @MockBean
//    DepositRepositoy depositRepository;
//    @MockBean
//    MicroserviceOrderRelations orderRelations;
//    @MockBean
//    MicroserviceUserRelations userRelations;
//    @MockBean
//    MicroserviceProductRelations productRelations;
//    @MockBean
//    EventSender eventSender;
//
//
//    @InjectMocks
//    PaymentService paymentService;
//
//    @Before
//    public void setUp() throws Exception {
//        MockitoAnnotations.initMocks(this);
//        order = new OrderDto();
//        deposit = new Deposit();
//        user = new UserPersonalDto();
//
//        deposit.setDepositId(depositId);
//        order.setOrderId(orderDtoId);
//        order.setUser(userId);
//        user.setUsername(userId);
//    }
//
//    @Test
//    public void creditCardPay() throws Exception {
//        CreateOrderEvent event = new CreateOrderEvent();
//        event.setUser("testUser");
//        event.setOrderId("testOrderId");
//        event.setPaymentType(PaymentMethod.CREDIT_CARD);
//        event.setOrderPrice(50.0);
//
//        UserPersonalDto userTest = new UserPersonalDto();
//        userTest.setUsername("testUser");
//
//        Deposit returnedDeposit = new Deposit();
//        returnedDeposit.setBalance(100.0);
//        returnedDeposit.setUserId("testUser");
//        returnedDeposit.setDepositId("testDeposit");
//
//        System.out.println(returnedDeposit);
//        when(userRelations.getPersonalUser(anyString())).thenReturn(userTest);
//        when(depositRepository.findByUserId(anyString())).thenReturn(returnedDeposit);
//        when(userRelations.getPersonalUser(anyString())).thenReturn(userTest);
//
//        paymentService.creditCardPay(event);
//        verify(depositRepository, times(1)).save(returnedDeposit);
//    }
//
//
//    @Test
//    public void showPaymentList() {
//        List<Deposit> list = new ArrayList<>();
//        list.add(deposit);
//
//        List<DepositDto> returnedList = list.stream()
//                .map(DepositMapper::convertToDto)
//                .collect(Collectors.toList());
//
//        when(depositRepository.findAll()).thenReturn(list);
//
//        assertEquals(paymentService.showDepositsList(), returnedList);
//
//        verify(depositRepository, times(1)).findAll();
//    }
//}
@RunWith(MockitoJUnitRunner.class)
public class PaymentServiceTest {

    Deposit deposit;
    String depositId = "testId";
    OrderDto order;
    String orderDtoId = "testIdOrder";
    UserPersonalDto user;
    String userId = "testIdUser";

    @Mock
    DepositRepositoy depositRepository;
    @Mock
    MicroserviceOrderRelations orderRelations;
    @Mock
    MicroserviceUserRelations userRelations;
    @Mock
    MicroserviceProductRelations productRelations;
    @Mock
    EventSender eventSender;

    @InjectMocks
    PaymentService paymentService = new PaymentService();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
//        order = new OrderDto();
        deposit = new Deposit();
        deposit.setBalance(0.0);
    }

    @Test
    public void creditCardPay() throws Exception {
        CreateOrderEvent event = new CreateOrderEvent();
        event.setUser("testUser");
        event.setOrderId("testOrderId");
        event.setPaymentType(PaymentMethod.CREDIT_CARD);
        event.setOrderPrice(50.0);

        UserPersonalDto userTest = new UserPersonalDto();
        userTest.setUsername("testUser");

        Deposit returnedDeposit = new Deposit();
        returnedDeposit.setBalance(100.0);
        returnedDeposit.setUserId("testUser");
        returnedDeposit.setDepositId("testDeposit");

        System.out.println(returnedDeposit);
        when(userRelations.getPersonalUser(anyString())).thenReturn(userTest);
        when(depositRepository.findByUserId(anyString())).thenReturn(returnedDeposit);
        when(userRelations.getPersonalUser(anyString())).thenReturn(userTest);

        paymentService.creditCardPay(event);
        verify(depositRepository, times(1)).save(returnedDeposit);
    }

    @Test
    public void sendPaymentEvent() throws Exception {
        CreateOrderEvent event = new CreateOrderEvent();

        paymentService.sendPaymentEvent(event);
        verify(eventSender, times(1)).sendMessageInQueue(anyString(), anyString(), anyString());
    }


    @Test
    public void showDepositList() {
        List<Deposit> list = new ArrayList<>();
        list.add(deposit);

        List<DepositDto> returnedList = list.stream()
                .map(DepositMapper::convertToDto)
                .collect(Collectors.toList());

        when(depositRepository.findAll()).thenReturn(list);

        assertEquals(paymentService.showDepositsList(), returnedList);

        verify(depositRepository, times(1)).findAll();
    }

    @Test
    public void createDeposit() {
        Deposit newDeposit = new Deposit();
        newDeposit.setUserId("abc");
        newDeposit.setBalance(0.0);
        paymentService.createDeposit("abc");
        verify(depositRepository, times(1)).save(newDeposit);
    }

    @Test
    public void addBalance() {
        deposit.setUserId("testId");
        when(depositRepository.findByUserId(anyString())).thenReturn(deposit);

        paymentService.addBalance(deposit.getUserId(), "100.0");

        verify(depositRepository, times(1)).save(deposit);
    }
}