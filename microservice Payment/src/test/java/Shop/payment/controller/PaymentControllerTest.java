package Shop.payment.controller;

import Shop.payment.dto.DepositDto;
import Shop.payment.service.PaymentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PaymentController.class)
public class PaymentControllerTest {

    @MockBean
    PaymentService paymentService;

    @Autowired
    MockMvc mockMvc;

    private ObjectMapper mapper;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void createDeposit() throws Exception {
        when(paymentService.createDeposit(anyString())).thenReturn("Deposit created");

        mockMvc.perform(post("/createDeposit")
                .param("user", "test"))
                .andExpect(status().isOk())
                .andExpect(content().string("Deposit created"));
        verify(paymentService, times(1)).createDeposit(anyString());
    }

    @Test
    public void addBalance() throws Exception {
        when(paymentService.addBalance(anyString(), anyString())).thenReturn("Balance added");

        mockMvc.perform(post("/addBalance")
                .param("user", "testUser")
                .param("amount", "100.0"))
                .andExpect(status().isOk())
                .andExpect(content().string("Balance added"));
    }

    @Test
    public void removeBalance() throws Exception {

        Double doubleNo=100.0;
        mockMvc.perform(put("/removeBalance")
                .param("user", "testUser")
                .param("balance", String.valueOf(doubleNo)))
                .andExpect(status().isOk());
    }

    @Test
    public void showDeposits() throws Exception {
        mapper=new ObjectMapper();
        DepositDto deposit1= DepositDto.builder().user("user1").build();
        DepositDto deposit2= DepositDto.builder().user("user2").build();
        List<DepositDto> returnedList= new ArrayList<>();
        returnedList.add(deposit1);
        returnedList.add(deposit2);

        when(paymentService.showDepositsList()).thenReturn(returnedList);

        String jsonString=mapper.writeValueAsString(returnedList);

        mockMvc.perform(get("/deposits"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void getDeposit() throws Exception {
        when(paymentService.getDeposit(anyString())).thenReturn("100.0");
        mockMvc.perform(get("/getDeposit")
        .param("userId","testId"))
                .andExpect(status().isOk());
    }
}