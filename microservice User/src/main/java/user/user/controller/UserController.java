package user.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import user.user.dto.UserDto;
import user.user.dto.UserEmailAndPasswordDto;
import user.user.dto.UserPersonalDto;
import user.user.dto.UserWithCardDto;
import user.user.exceptions.UserDoesNotExistException;
import user.user.service.UserService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * Returning a list of  all the users in the database
     *
     * @return all users from db
     */
    @GetMapping("/users")
    public List<UserDto> showAllUsers() {
        return userService.getAllUsers();
    }


    /**
     * Showing someone's full data based on a given existing username
     *
     * @param username
     * @return a more detailed object , containging someone's personal information
     * @throws UserDoesNotExistException in case the client enters a wrong username,this exception will be thrown
     */
    @GetMapping("/users/myPage/{userId}")
    public UserPersonalDto getMyPage(@PathVariable("userId") String username) throws UserDoesNotExistException {
        return userService.getMyPersonalPage(username);
    }


    /**
     * Creating and returning a new user ,using an input UserDtoBody, and two headers for the password
     *
     * @param userDto         given User details
     * @param password        given password
     * @param confirmPassword -repeat : validate password
     * @return Returns the created user
     */
    @PostMapping("/addUser")
    public UserDto addNewUser(@Valid @RequestBody UserDto userDto,
                              @RequestHeader("password") String password,
                              @RequestHeader("confirmPassword") String confirmPassword) throws IOException {
        return userService.addUser(userDto, password, confirmPassword);
    }


    /**
     * Updating an existing user based on a UserDto taken as JSON input. Only the edited fields will be changed
     * You can not change your user
     *
     * @param userDto
     * @return the modified user
     */
    @PutMapping("/updateUser")
    public UserDto updateExistingUser(@RequestBody UserDto userDto) {
        return userService.updateUser(userDto);
    }


    /**
     * Modifying a user's password,given his username and the new password
     *
     * @param userId
     * @param password
     * @return confirmation- It was/It wasn't changed
     */
    @PutMapping("/changePassword/{userId}")
    public String changePassword(@PathVariable("userId") String userId,
                                 @RequestHeader("password") String password) {
        return userService.changePassword(userId, password);
    }


    /**
     * Requesting someones password via email,in case the client forgot it
     *
     * @param email Specify the email and
     * @return you will recieve an email (if existing) containing your username and password
     */
    @GetMapping("/forgotMyPassword")
    public String getMyPasswordViaEmail(@RequestParam("email") String email) {

        return userService.requestPasswordViaEmail(email);
    }


    /**
     * Showing an user's public data,given the username
     *
     * @param username
     * @return User's public data
     */
    @GetMapping("/users/{username}")
    public UserDto getUserByUsername(@PathVariable("username") String username) {
        return userService.getUserByUsername(username);
    }

    /**
     * EndPoint used when called via RestTemplate from other microservices.Used for validation
     *
     * @param username
     * @return Data transfer object
     */
    @GetMapping("/users/myPage/getUsernameAndPassword/{username}")
    public UserEmailAndPasswordDto getUserMailAndPassword(@PathVariable("username") String username) {
        return userService.getUsernameEmailAndPasswordOf(username);
    }

    /**
     * EndPoint used when called via RestTemplate from the Microservice CreditCard- Used to see if a specific
     * user has /doesn't have a CreditCard;
     *
     * @param username
     * @return Data transfer object
     */
    @GetMapping("/users/myPage/getUsernameAndCard/{username}")
    public UserWithCardDto getUsernameAndCard(@PathVariable("username") String username) {
        return userService.getUsernameAndCardOf(username);
    }

    /**
     * Delete a specific user using it's given username
     *
     * @param username
     * @return Confirmation
     */
    @DeleteMapping("/delete/{userId}")
    public String deleteUser(@PathVariable("userId") String username) {
        return userService.deleteUser(username);
    }


    /**
     * EndPoint used in MicroserviceCreditCard.
     * Each time a CreditCard will be saved, it's specific User will be modified,the CreditCard's id being specified
     * in it's structure
     * @param userId
     * @param cardNumber
     * @return Confirmation
     */
    @PutMapping("/assignCreditCardTo/{userId}")
    public String assignCreditCard(@PathVariable("userId") String userId,
                                   @RequestHeader("cardNumber") String cardNumber) {
        return userService.assignCreditCard(userId, cardNumber);
    }


    /**
     * EndPoint used in other microservices to check if a given username exists or not.
     * @param userId
     * @return
     */
    @GetMapping("/checkUser/{userId}")
    public String checkIfUserExists(@PathVariable("userId") String userId) {
        return userService.checkIfUserExistsForReview(userId);
    }


}
