package user.user.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@Document(collection = "Users")
@ToString
public class User {

    @Id
    private String username;

    @Indexed(unique = true)
    private String email;

    private String firstName;

    private String lastName;

    private String cartId;

    private String creditCardId;

    private String password;


    public String getFullName() {
        return firstName + " " + lastName;
    }

}
