package user.user.exceptions;

public class GeneralException extends RuntimeException {

    public String statusCode;

    public GeneralException(String message,String statusCode){
        super(message);
        this.statusCode=statusCode;
    }
}
