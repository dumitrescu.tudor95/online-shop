package user.user.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@Getter
@Setter
public class UserDoesNotExistException extends RuntimeException {
    private String statusCode;
    public UserDoesNotExistException(String message){
        super(message);
        statusCode="404";
    }
}
