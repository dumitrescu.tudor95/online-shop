package user.user.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import user.user.configuration.requestMaker.NoHeaders;
import user.user.configuration.requestMaker.NoQueryParams;
import user.user.configuration.requestMaker.Request;

import java.io.IOException;

public class MicroserviceRequest {

    @Autowired
    Request requestSender;

    @Autowired
    ObjectMapper mapper;

    protected final NoQueryParams NO_QUERY_PARAMS = new NoQueryParams();
    protected final NoHeaders NO_HEADERS = new NoHeaders();

    <T> T convertToRequestedObject(Object givenObject, Class<T> requiredClass) throws IOException {
        return mapper.readValue(mapper.writeValueAsString(givenObject), requiredClass);
    }
}
