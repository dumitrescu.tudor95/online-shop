package user.user.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import user.user.dto.EmailFormDTO;


@Component
public class EmailManager extends MicroserviceRequest implements EmailSender {

    @Autowired
    RestTemplate restTemplate;

    @Value("${send.email}")
    private String SEND_EMAIL_LINK;

    @Override
    public String sendEmail(String specificUserEmail, String subject, String message) {
        restTemplate = new RestTemplate();

        EmailFormDTO emailDTO = new EmailFormDTO();
        emailDTO.setFrom("tudor_project_shop@mailtrap.com");
        emailDTO.setTo(specificUserEmail);
        emailDTO.setSubject(subject);
        emailDTO.setMessage(message);

        restTemplate.postForEntity(SEND_EMAIL_LINK, emailDTO, String.class);


        return "Mail will be sent shortly";
    }

}
