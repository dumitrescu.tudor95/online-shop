package user.user.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import user.user.configuration.requestMaker.NoHeaders;
import user.user.configuration.requestMaker.NoQueryParams;
import user.user.configuration.requestMaker.RequestType;
import user.user.dto.CartDto;

import java.io.IOException;
import java.util.HashMap;

@Component
public class CartManager extends MicroserviceRequest{


    @Value("${get.cart.from}")
    private String GET_CART_FROM_LINK;
    @Value("${delete.cart.from}")
    private String DELETE_CART_FROM_LINK;
    @Value("${add.cart}")
    private String ADD_CART_LINK;


    public CartDto getCartFrom(String userId) throws IOException {

        Object returnedObject = requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_CART_FROM_LINK + userId,
                NO_QUERY_PARAMS,
                NO_HEADERS
        );
        return convertToRequestedObject(returnedObject, CartDto.class);
    }

    public String deleteCartFrom(String userId) {

        String returnedMessage = requestSender.sendMessageReturningRequest(RequestType.DELETE,
                DELETE_CART_FROM_LINK + userId,
                NO_QUERY_PARAMS,
                NO_HEADERS);
        return returnedMessage;
    }

    public CartDto addCartToUser(String user) throws IOException {
        HashMap<String,String> queryParams=new HashMap<>();
        queryParams.put("userId",user);

        Object returnedObject=requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.POST,
                ADD_CART_LINK,
                queryParams,
                NO_HEADERS);
        return convertToRequestedObject(returnedObject,CartDto.class);
    }
}
