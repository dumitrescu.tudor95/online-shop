package user.user.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import user.user.configuration.requestMaker.RequestType;

import java.util.HashMap;

@Component
public class PaymentManager extends MicroserviceRequest{

    @Value("${create.deposit}")
    private String CREATE_DEPOSIT_LINK;
    @Value("${delete.deposit}")
    private String DELETE_DEPOSIT_LINK;


    public void createDeposit(String user) {
        HashMap<String,String> queryParams=new HashMap<>();
        queryParams.put("user",user);
        requestSender.sendMessageReturningRequest(RequestType.POST,
                CREATE_DEPOSIT_LINK,
                queryParams,
                NO_HEADERS);
    }

    public void deleteDeposit(String user) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("user", user);

        requestSender.sendMessageReturningRequest(RequestType.DELETE,
                DELETE_DEPOSIT_LINK,
                queryParams, NO_HEADERS);
    }
}
