package user.user.configuration.requestMaker;

public enum RequestType {
    GET, PUT, POST, DELETE;
}
