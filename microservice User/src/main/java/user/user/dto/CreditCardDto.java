package user.user.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreditCardDto {

    private String cardNumber;
    private String ownerName;
    private Integer cardNumberBack;
    private LocalDate expirationDate;
    private String ownerUsername;



}
