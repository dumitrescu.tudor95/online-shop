package user.user.mapper;

import user.user.dto.UserEmailAndPasswordDto;
import user.user.dto.UserDto;
import user.user.dto.UserPersonalDto;
import user.user.dto.UserWithCardDto;
import user.user.entity.User;

public class UserMapper {

    public static UserDto convertToDto(User user){
           return UserDto.builder()
                   .username(user.getUsername())
                   .firstName(user.getFirstName())
                   .lastName(user.getLastName())
                   .email(user.getEmail())
                   .cartId(user.getCartId())
                   .build();
    }

    public static UserPersonalDto convertToPersonalDto(User user){
        if (user.getCreditCardId() == null){
            return UserPersonalDto.builder()
                    .username(user.getUsername())
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .email(user.getEmail())
                    .creditCardId(null)
                    .cartId(user.getCartId())
                    .build();
        }
        else {
            return UserPersonalDto.builder()
                    .username(user.getUsername())
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .email(user.getEmail())
                    .cartId(user.getCartId())
                    .creditCardId(user.getCreditCardId())
                    .build();
        }
    }

    public static User convertToEntity(UserDto userDto){
        User user=new User();
        user.setUsername(userDto.getUsername());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setCartId(userDto.getCartId());
        user.setEmail(userDto.getEmail());
        return user;
    }

    public static UserEmailAndPasswordDto convertToUserAndPassword(User user){
        UserEmailAndPasswordDto userEmailAndPasswordDto =new UserEmailAndPasswordDto();
        userEmailAndPasswordDto.setUsername(user.getUsername());
        userEmailAndPasswordDto.setEmail(user.getEmail());
        userEmailAndPasswordDto.setPassword(user.getPassword());
        return userEmailAndPasswordDto;
    }

    public static UserWithCardDto convertToUserAndCard(User user){
        UserWithCardDto userWithCardDto=new UserWithCardDto();
        userWithCardDto.setUsername(user.getUsername());
        userWithCardDto.setCard(user.getCreditCardId());
        return userWithCardDto;
    }
}
