package user.user.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import user.user.configuration.CartManager;
import user.user.configuration.EmailSender;
import user.user.configuration.PaymentManager;
import user.user.dto.UserDto;
import user.user.dto.UserEmailAndPasswordDto;
import user.user.dto.UserPersonalDto;
import user.user.dto.UserWithCardDto;
import user.user.entity.User;
import user.user.exceptions.AccountAlreadyExistsException;
import user.user.exceptions.PasswordsDoNotMatchException;
import user.user.exceptions.PermissionException;
import user.user.exceptions.UserDoesNotExistException;
import user.user.mapper.UserMapper;
import user.user.repository.UserRepository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    EmailSender emailSender;
    @Autowired
    CartManager cartManager;
    @Autowired
    PaymentManager paymentRelations;

    public List<UserDto> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(UserMapper::convertToDto)
                .collect(Collectors.toList());

    }

    public UserDto getUserByUsername(String username) throws UserDoesNotExistException {
        Optional<User> specificUser = userRepository.findById(username);

        checkIfUserExists(specificUser);

        return UserMapper.convertToDto(specificUser.get());
    }

    public UserPersonalDto getMyPersonalPage(String username) throws UserDoesNotExistException {

        Optional<User> currentUser = userRepository.findById(username);

        checkIfUserExists(currentUser);

        return UserMapper.convertToPersonalDto(currentUser.get());

    }

    public UserEmailAndPasswordDto getUsernameEmailAndPasswordOf(String username) throws UserDoesNotExistException {
        Optional<User> currentUser = userRepository.findById(username);

        checkIfUserExists(currentUser);

        return UserMapper.convertToUserAndPassword(currentUser.get());
    }

    public UserWithCardDto getUsernameAndCardOf(String username) throws UserDoesNotExistException {
        Optional<User> currentUser = userRepository.findById(username);
        checkIfUserExists(currentUser);
        return UserMapper.convertToUserAndCard(currentUser.get());
    }

    public String requestPasswordViaEmail(String email) throws UserDoesNotExistException {
        Optional<User> specificUser = userRepository.findByEmail(email);

        checkIfUserExists(specificUser);

        //todo     emailSender.sendEmail(email, "Your password", "Hello," + specificUser.get().getUsername() + ".Your password is " + specificUser.get().getPassword());

        return "An email has been sent.Check out your inbox";
    }


    public String deleteUser(String username) throws UserDoesNotExistException {
        Optional<User> specificUser = userRepository.findById(username);

        checkIfUserExists(specificUser);

        userRepository.deleteById(specificUser.get().getUsername());

        cartManager.deleteCartFrom(username);
        paymentRelations.deleteDeposit(username);

        //todo  emailSender.sendEmail(specificUser.get().getEmail(), "Account deleted", "Your account has been deleted ");

        return "User successfuly deleted";
    }


    public UserDto addUser(UserDto userDto, String password, String repeatPassword) throws AccountAlreadyExistsException, PasswordsDoNotMatchException, IOException {

        if (userRepository.findById(userDto.getUsername()).isPresent() ||
                userRepository.findByEmail(userDto.getEmail()).isPresent()) {
            throw new AccountAlreadyExistsException("There is already an account registered with this username and/or email");
        }

        checkIfPasswordsMatch(password, repeatPassword);

        User user = UserMapper.convertToEntity(userDto);
        user.setPassword(password);

        user = userRepository.save(user);

        user.setCartId(
                cartManager.addCartToUser(user.getUsername())
                        .getCartId()
        );

        userRepository.save(user);

        paymentRelations.createDeposit(user.getUsername());

        StringBuffer mailContent = new StringBuffer();
        mailContent.append("Your new account on Tudor's Shop has been successfully created.\nAccount details:\n");
        mailContent.append("Username: " + user.getUsername() + "\n");
        mailContent.append("Name: " + user.getFullName() + "\n");
        mailContent.append("Email: " + user.getEmail() + "\n\n");
        mailContent.append("Please note:In order to purchase products,you must enter your credit card at http://localhost:9999/users/myPage/addCreditCard");
        //todo            emailSender.sendEmail(user.getEmail(), "Registered successfully", mailContent.toString());

        System.out.println(UserMapper.convertToDto(user));
        return UserMapper.convertToDto(user);
    }


    public UserDto updateUser(UserDto userDto) throws UserDoesNotExistException {

        Optional<User> specificUser = userRepository.findById(userDto.getUsername());

        checkIfUserExists(specificUser);

        int emailCountInDatabase = (int) userRepository.findAll()
                .stream()
                .filter(u -> u.getEmail().equals(userDto.getEmail()))
                .count();
        if (emailCountInDatabase > 1) {
            throw new PermissionException("That email is already taken by somebody else");
        }
        User modifiedUser = UserMapper.convertToEntity(userDto);

        modifiedUser.setCartId(specificUser.get().getCartId());

        modifiedUser.setCreditCardId(specificUser.get().getCreditCardId());

        userRepository.save(modifiedUser);

        return UserMapper.convertToDto(modifiedUser);
    }

    public String changePassword(String userId, String password) throws UserDoesNotExistException {
        Optional<User> specificUser = userRepository.findById(userId);

        checkIfUserExists(specificUser);

        specificUser.get().setPassword(password);

        userRepository.save(specificUser.get());

        return "Password changed";
    }





    public String assignCreditCard(String userId, String cardNumber) throws UserDoesNotExistException {

        Optional<User> specificUser = userRepository.findById(userId);

        specificUser.get().setCreditCardId(cardNumber);

        userRepository.save(specificUser.get());

        return "Credit card assigned";
    }

    public void addCartTo(String userId, String cartId) {
        Optional<User> specificUser = userRepository.findById(userId);
        checkIfUserExists(specificUser);

        specificUser.get().setCartId(cartId);

        userRepository.save(specificUser.get());
    }

    public String checkIfUserExistsForReview(String userId) {
        Optional<User> specificUser=userRepository.findById(userId);
        if (!specificUser.isPresent()){
            throw new UserDoesNotExistException("The user you specified does not exist");
        }
        return "It exists";
    }


    public void checkIfUserExists(Optional<User> specificUser) throws UserDoesNotExistException {
        if (!specificUser.isPresent()) {
            throw new UserDoesNotExistException("The user you specified does not exist");
        }
    }


    public void checkIfPasswordsMatch(String password, String repeatPassword)  {
        if (!password.equals(repeatPassword)) {
            throw new PasswordsDoNotMatchException("The passwords do not match");
        }
    }


}
