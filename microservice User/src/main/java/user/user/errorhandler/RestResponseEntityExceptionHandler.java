package user.user.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import user.user.exceptions.AccountAlreadyExistsException;
import user.user.exceptions.PasswordsDoNotMatchException;
import user.user.exceptions.UserDoesNotExistException;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler(value = AccountAlreadyExistsException.class)
    protected ResponseEntity<Object> handleAccountAlreadyExistException( AccountAlreadyExistsException ex) {

        System.out.println("======================== ACCOUNT EXISTS EXCEPTION HANDLER ============================");

        ErrorDTO error = new ErrorDTO();
        error.setMessage(ex.getMessage());
        error.setStatusCode(ex.getStatusCode());

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UserDoesNotExistException.class)
    protected ResponseEntity<Object> handleUserDoesNotExistException( UserDoesNotExistException ex) {

        System.out.println("======================== USER DOES NOT EXIST EXCEPTION HANDLER ============================");

        ErrorDTO error = new ErrorDTO();
        error.setMessage(ex.getMessage());
        error.setStatusCode(ex.getStatusCode());

        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

}
