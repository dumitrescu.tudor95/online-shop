package user.user.repository;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import user.user.entity.User;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataMongoTest
public class UserRepositoryTest {


    @Autowired
    private UserRepository userRepository;

    User user ;
    User user2 ;

    @Before
    public void setUp(){
        user = new User();
        user.setUsername("testUsername");
        user.setEmail("testEmail");

        user2=new User();
        user2.setUsername("testUsername2");
    }

    @Test
    public void findById(){
        userRepository.save(user);

        User found=userRepository.findById("testUsername").get();

        assertEquals(found.getUsername(),user.getUsername());
    }


    @Test
    public void whenFindByEmail() {

        // given
       userRepository.save(user);

        // when
        User found = userRepository.findByEmail(user.getEmail()).get();

        // then
        assertEquals(found.getEmail(),user.getEmail());
    }

    @After
    public void clean(){
        userRepository.delete(user);
        userRepository.delete(user2);
    }
}