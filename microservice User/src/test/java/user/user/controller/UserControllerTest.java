package user.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import user.user.dto.UserDto;
import user.user.dto.UserEmailAndPasswordDto;
import user.user.dto.UserPersonalDto;
import user.user.dto.UserWithCardDto;
import user.user.exceptions.GeneralException;
import user.user.exceptions.UserDoesNotExistException;
import user.user.service.UserService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @MockBean
    UserService userService;

    @Autowired
    MockMvc mockMvc;

    UserDto userDto;
    String userId = "testUsername";

    ObjectMapper mapper;

    @Before
    public void setUp() {
        userDto = new UserDto();
        userDto.setUsername(userId);
        mapper = new ObjectMapper();
    }


    @Test
    public void showAllUsers() throws Exception {
        List<UserDto> list = new ArrayList<>();
        list.add(userDto);

        String jsonString = mapper.writeValueAsString(list);

        when(userService.getAllUsers()).thenReturn(list);

        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }


    @Test
    public void getMyPageHappyCase() throws Exception {
        UserPersonalDto userPersonalDto = new UserPersonalDto();
        userPersonalDto.setUsername(userId);

        String jsonString = mapper.writeValueAsString(userPersonalDto);

        when(userService.getMyPersonalPage(anyString()))
                .thenReturn(userPersonalDto);

        mockMvc.perform(get("/users/myPage/{userId}", userId))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void getMyPageErrorThrown() throws Exception {
        UserPersonalDto userPersonalDto = new UserPersonalDto();
        userPersonalDto.setUsername(userId);
        String jsonString = mapper.writeValueAsString(userPersonalDto);

        when(userService.getMyPersonalPage(anyString()))
                .thenThrow(UserDoesNotExistException.class);

        mockMvc.perform(get("/users/myPage/{userId}", "invalidId"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void addNewUserHappyCase() throws Exception {
        String jsonString = mapper.writeValueAsString(userDto);

        when(userService.addUser(any(), anyString(), anyString()))
                .thenReturn(userDto);

        mockMvc.perform(post("/addUser")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonString)
                .header("password", "password")
                .header("confirmPassword", "passowrd"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void addNewUserExceptionThrown() throws Exception {
        String jsonString = mapper.writeValueAsString(userDto);

        when(userService.addUser(any(), anyString(), anyString()))
                .thenThrow(GeneralException.class);

        mockMvc.perform(post("/addUser")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonString)
                .header("password", "password")
                .header("confirmPassword", "passowrdx2"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void updateExistingUserHappyCase() throws Exception {
        String jsonString = mapper.writeValueAsString(userDto);

        when(userService.updateUser(any())).thenReturn(userDto);

        mockMvc.perform(put("/updateUser")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonString))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void updateExistingUserExceptionThrown() throws Exception {
        String jsonString = mapper.writeValueAsString(userDto);

        when(userService.updateUser(any())).thenThrow(GeneralException.class);

        mockMvc.perform(put("/updateUser")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonString))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void changePasswordHappyCase() throws Exception {

        when(userService.changePassword(anyString(), anyString()))
                .thenReturn("Password changed");

        mockMvc.perform(put("/changePassword/{userId}", userId)
                .header("password", "password"))
                .andExpect(status().isOk())
                .andExpect(content().string("Password changed"));
    }

    @Test
    public void changePasswordExceptionThrown() throws Exception {

        when(userService.changePassword(anyString(), anyString()))
                .thenThrow(GeneralException.class);

        mockMvc.perform(put("/changePassword/{userId}", "invalidId")
                .header("password", "password"))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void getMyPasswordViaEmailHappyCase() throws Exception {
        when(userService.requestPasswordViaEmail(anyString()))
                .thenReturn("An email has been sent.Check out your inbox");

        mockMvc.perform(get("/forgotMyPassword")
                .param("email", "validEmail"))
                .andExpect(status().isOk())
                .andExpect(content().string("An email has been sent.Check out your inbox"));
    }

    @Test
    public void getMyPasswordViaEmailExceptionThrown() throws Exception {
        when(userService.requestPasswordViaEmail(anyString()))
                .thenThrow(UserDoesNotExistException.class);

        mockMvc.perform(get("/forgotMyPassword")
                .param("email", "invalidEmail"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void getUserByUsernameHappyCase() throws Exception {

        String jsonString = mapper.writeValueAsString(userDto);

        when(userService.getUserByUsername(anyString())).thenReturn(userDto);

        mockMvc.perform(get("/users/{}id", userId))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void getUserByUsername() throws Exception {

        when(userService.getUserByUsername(anyString()))
                .thenThrow(UserDoesNotExistException.class);

        mockMvc.perform(get("/users/{}id", userId))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void getUserMailAndPasswordHappyCase() throws Exception {
        UserEmailAndPasswordDto userEmailAndPasswordDto = new UserEmailAndPasswordDto();
        userEmailAndPasswordDto.setUsername(userId);

        String jsonString = mapper.writeValueAsString(userEmailAndPasswordDto);

        when(userService.getUsernameEmailAndPasswordOf(anyString()))
                .thenReturn(userEmailAndPasswordDto);

        mockMvc.perform(get("/users/myPage/getUsernameAndPassword/{username}", userId))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void getUserMailAndPassword() throws Exception {

        when(userService.getUsernameEmailAndPasswordOf(anyString()))
                .thenThrow(UserDoesNotExistException.class);

        mockMvc.perform(get("/users/myPage/getUsernameAndPassword/{username}", userId))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void getUsernameAndCardHappyCase() throws Exception {
        UserWithCardDto userWithCardDto = new UserWithCardDto();
        userWithCardDto.setUsername(userId);

        String jsonString = mapper.writeValueAsString(userWithCardDto);

        when(userService.getUsernameAndCardOf(anyString()))
                .thenReturn(userWithCardDto);

        mockMvc.perform(get("/users/myPage/getUsernameAndCard/{username}", userId))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }

    @Test
    public void getUsernameAndCardExceptionThrown() throws Exception {

        when(userService.getUsernameAndCardOf(anyString()))
                .thenThrow(UserDoesNotExistException.class);

        mockMvc.perform(get("/users/myPage/getUsernameAndCard/{username}", userId))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void deletesUserHappyCase() throws Exception {
        when(userService.deleteUser(anyString())).thenReturn("User successfuly deleted");
        mockMvc.perform(delete("/delete/{userId}", userId))
                .andExpect(status().isOk())
                .andExpect(content().string("User successfuly deleted"));
    }

    @Test
    public void deletesUserExceptionThrown() throws Exception {
        when(userService.deleteUser(anyString())).thenThrow(UserDoesNotExistException.class);
        mockMvc.perform(delete("/delete/{userId}", userId))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void assignCreditCardHappyCase() throws Exception {

        when(userService.assignCreditCard(anyString(), anyString()))
                .thenReturn("Credit card assigned");

        mockMvc.perform(put("/assignCreditCardTo/{userId}", userId)
                .header("cardNumber", "validCardNumber"))
                .andExpect(status().isOk())
                .andExpect(content().string("Credit card assigned"));
    }

    @Test
    public void assignCreditCardExceptionThrown() throws Exception {

        when(userService.assignCreditCard(anyString(), anyString()))
                .thenThrow(UserDoesNotExistException.class);

        mockMvc.perform(put("/assignCreditCardTo/{userId}", userId)
                .header("cardNumber", "validCardNumber"))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void addCartToHappyCase() throws Exception {
        mockMvc.perform(put("/addCartTo/{userId}", userId)
                .header("cartId", "someValidCartId"))
                .andExpect(status().isOk());
    }


//    @Test
//    public void removeCartHappyCase() throws Exception {
//        when(userService.removeCart(anyString(), anyString())).thenReturn("Cart removed");
//
//        mockMvc.perform(delete("/removeCartFrom/{userId}", userId)
//                .header("cartId", "validCartId"))
//                .andExpect(status().isOk())
//                .andExpect(content().string("Cart removed"));
//    }
//
//    @Test
//    public void removeCartExceptionThrown() throws Exception {
//        when(userService.removeCart(anyString(), anyString()))
//                .thenThrow(UserDoesNotExistException.class);
//
//        mockMvc.perform(delete("/removeCartFrom/{userId}", userId)
//                .header("cartId", "validCartId"))
//                .andExpect(status().is4xxClientError());
//    }
}