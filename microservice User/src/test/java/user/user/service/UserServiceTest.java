package user.user.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import user.user.configuration.CartManager;
import user.user.configuration.EmailManager;
import user.user.configuration.PaymentManager;
import user.user.dto.CartDto;
import user.user.dto.UserDto;
import user.user.dto.UserEmailAndPasswordDto;
import user.user.dto.UserPersonalDto;
import user.user.dto.UserWithCardDto;
import user.user.entity.User;
import user.user.exceptions.AccountAlreadyExistsException;
import user.user.exceptions.PasswordsDoNotMatchException;
import user.user.exceptions.PermissionException;
import user.user.exceptions.UserDoesNotExistException;
import user.user.mapper.UserMapper;
import user.user.repository.UserRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserServiceTest {

    @MockBean
    UserRepository userRepository;
    @MockBean
    CartManager cartManager;
    @MockBean
    PaymentManager depositRelations;
    @MockBean
    EmailManager emailManager;

    @InjectMocks
    UserService userService;

    User user;
    String userId = "userId";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        user = new User();
        user.setUsername(userId);
    }

    @Test
    public void getAllUsers() {
        List<User> list = new ArrayList<>();
        list.add(user);

        UserDto userDto = UserMapper.convertToDto(user);

        List<UserDto> returnedList = new ArrayList<>();
        returnedList.add(userDto);

        when(userRepository.findAll()).thenReturn(list);

        assertEquals(userService.getAllUsers(), returnedList);
        verify(userRepository, times(1)).findAll();
    }

    @Test
    public void getUserByUsername() {
        UserDto returnedUser = UserMapper.convertToDto(user);

        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));

        assertEquals(userService.getUserByUsername(userId), returnedUser);
        verify(userRepository, times(1)).findById(anyString());

    }

    @Test(expected = UserDoesNotExistException.class)
    public void getUserByUsernameWhenItDoesntExist() {
        when(userRepository.findById(anyString())).thenReturn(Optional.empty());

        userService.getUserByUsername(userId);
        verify(userRepository, times(1)).findById(anyString());
    }

    @Test
    public void getMyPersonalPage() {
        UserPersonalDto returnedUser = UserMapper.convertToPersonalDto(user);

        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));

        assertEquals(userService.getMyPersonalPage(userId), returnedUser);
        verify(userRepository, times(1)).findById(anyString());
    }

    @Test(expected = UserDoesNotExistException.class)
    public void getMyPersonalPageWhenUserDoesNotExist() {

        when(userRepository.findById(anyString())).thenReturn(Optional.empty());

        userService.getMyPersonalPage(userId);
        verify(userRepository, times(1)).findById(anyString());
    }

    @Test
    public void getUsernameEmailAndPasswordOf() {
        UserEmailAndPasswordDto returnedUser = UserMapper.convertToUserAndPassword(user);

        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));

        assertEquals(userService.getUsernameEmailAndPasswordOf(userId), returnedUser);
        verify(userRepository, times(1)).findById(anyString());
    }

    @Test(expected = UserDoesNotExistException.class)
    public void getUsernameEmailAndPasswordWhenUserDoesNotExist() {

        when(userRepository.findById(anyString())).thenReturn(Optional.empty());

        userService.getUsernameEmailAndPasswordOf(userId);
        verify(userRepository, times(1)).findById(anyString());
    }

    @Test
    public void getUsernameAndCardOf() {
        UserWithCardDto returnedUser = UserMapper.convertToUserAndCard(user);
        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));

        assertEquals(userService.getUsernameAndCardOf(userId), returnedUser);
        verify(userRepository, times(1)).findById(anyString());
    }

    @Test(expected = UserDoesNotExistException.class)
    public void getUsernameAndCardWhenUserDoesNotExist() {
        when(userRepository.findById(anyString())).thenReturn(Optional.empty());
        userService.getUsernameAndCardOf(userId);
        verify(userRepository, times(1)).findById(anyString());
    }

    @Test
    public void requestPasswordViaEmail() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

        userService.requestPasswordViaEmail("someValidEmail");

        //todo verify(emailSender,times(1)).sendEmail(anyString(),anyString(),anyString());
    }

    @Test(expected = UserDoesNotExistException.class)
    public void requestPasswordViaEmailWhenEmailDoesNotExist() {

        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        userService.requestPasswordViaEmail("someInvalidEmail");
    }

    @Test
    public void deleteUser() {
        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));

        userService.deleteUser(userId);

        verify(userRepository, times(1)).findById(anyString());
        verify(userRepository, times(1)).deleteById(anyString());
        verify(cartManager, times(1)).deleteCartFrom(anyString());
        //todo verify(emailManager,times(1)).sendEmail(anyString(),anyString(),anyString());
    }

    @Test(expected = UserDoesNotExistException.class)
    public void deleteUserWhenItDoesNotExist() {
        when(userRepository.findById(anyString())).thenReturn(Optional.empty());

        userService.deleteUser(userId);
        verify(userRepository, times(1)).findById(anyString());
    }

    @Test(expected = AccountAlreadyExistsException.class)
    public void addUserWhenTheUsernameIsTaken() throws IOException {
        when(userRepository.findById(anyString())).thenReturn(Optional.of(new User()));
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        userService.addUser(UserMapper.convertToDto(user), "password", "password");
        verify(userRepository, times(1)).findById(anyString());
        verify(userRepository, times(1)).findByEmail(anyString());
    }

    @Test(expected = AccountAlreadyExistsException.class)
    public void addUserWhenTheEmailIsTaken() throws IOException {
        user.setEmail("emailThatIsAlreadyInTheDataBase");

        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(new User()));
        when(userRepository.findById(anyString())).thenReturn(Optional.empty());

        userService.addUser(UserMapper.convertToDto(user), "password", "password");
        verify(userRepository, times(1)).findById(anyString());
        verify(userRepository, times(1)).findByEmail(anyString());
    }

    @Test(expected = PasswordsDoNotMatchException.class)
    public void addUserWhenPasswordsDoNotMatch() throws IOException {
        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

        userService.addUser(new UserDto(), "password", "notTheSamePassword");

        verify(userRepository, times(1)).findById(anyString());
        verify(userRepository, times(1)).findByEmail(anyString());
    }

    @Test
    public void addUser() throws IOException {
        user.setEmail("testEmail");

        CartDto newCartDto = new CartDto();
        newCartDto.setCartId("testId");

        UserDto returnedUser = UserMapper.convertToDto(user);
        returnedUser.setCartId("testId");

        when(userRepository.findById(anyString())).thenReturn(Optional.empty());
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());
        when(cartManager.addCartToUser(anyString())).thenReturn(newCartDto);
        when(userRepository.save(any())).thenReturn(user);

        assertEquals(userService.addUser(UserMapper.convertToDto(user), "password", "password"), returnedUser);

        verify(userRepository, times(1)).findById(anyString());
        verify(userRepository, times(1)).findByEmail(anyString());
        verify(userRepository, times(2)).save(any());
        verify(cartManager, times(1)).addCartToUser(anyString());
        verify(depositRelations, times(1)).createDeposit(anyString());
    }


    @Test(expected = UserDoesNotExistException.class)
    public void updateUserWhenItDoesNotExist() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());
        userService.updateUser(new UserDto());
        verify(userRepository, times(1)).findById(anyString());
    }

    @Test(expected = PermissionException.class)
    public void updateUserWhenEmailIsAlreadyTakenBySomeoneElse() {
        user.setEmail("sameEmail");
        User user2 = new User();
        user2.setUsername("username");
        user2.setEmail("sameEmail");
        List<User> returnedList = new ArrayList<>();
        returnedList.add(user);
        returnedList.add(user2);

        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));
        when(userRepository.findAll()).thenReturn(returnedList);

        userService.updateUser(UserMapper.convertToDto(user2));
        verify(userRepository, times(1)).findById(anyString());
        verify(userRepository, times(1)).findAll();
    }

    @Test
    public void updateUser() {
        user.setCartId("cartId");
        user.setEmail("email");
        user.setCreditCardId("creditCardId");
        List<User> returnedList = new ArrayList<>();
        returnedList.add(user);

        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));
        when(userRepository.findAll()).thenReturn(returnedList);

        assertEquals(userService.updateUser(UserMapper.convertToDto(user)), UserMapper.convertToDto(user));
        verify(userRepository, times(1)).findById(anyString());
        verify(userRepository, times(1)).findAll();
        verify(userRepository, times(1)).save(any());
    }

    @Test
    public void changePassword() {
        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));

        userService.changePassword(userId, "newPassword");
        verify(userRepository, times(1)).findById(anyString());
        verify(userRepository, times(1)).save(any());
    }

    @Test(expected = UserDoesNotExistException.class)
    public void changePasswordWhenUserDoesNotExist() {
        when(userRepository.findById(anyString())).thenReturn(Optional.empty());
        userService.changePassword("someInvalidUserId", "newPassword");
        verify(userRepository, times(1)).findById(anyString());
    }


    @Test
    public void assignCreditCard() {
        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));

        userService.assignCreditCard(userId,"validCardId");
        verify(userRepository,times(1)).findById(anyString());
        verify(userRepository,times(1)).save(any());
    }

    @Test
    public void addCartTo() {
        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));

        userService.addCartTo(userId,"validCartId");
        verify(userRepository,times(1)).findById(anyString());
        verify(userRepository,times(1)).save(any());
    }
    @Test(expected = UserDoesNotExistException.class)
    public void addCartWhenUserDoesNotExist() {
        userService.addCartTo("invalidUserId","validCardId");
        verify(userRepository,times(1)).findById(anyString());
    }



    @Test(expected = UserDoesNotExistException.class)
    public void checkIfUserExists() {
        userService.checkIfUserExists(Optional.empty());
    }

    @Test(expected = PasswordsDoNotMatchException.class)
    public void checkIfPasswordsMatch() {
        userService.checkIfPasswordsMatch("aaa","bbb");
    }

}