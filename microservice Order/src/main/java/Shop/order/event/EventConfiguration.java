package Shop.order.event;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EventConfiguration {

    private static final String TOPIC_EXCHANGE_NAME = "order_sender";
    private static final String QUEUE_NAME_ORDER_CREATED = "ORDER_EVENTS";


    private static final String TOPIC_EXCHANGE_NAME_2 = "goods_fetched_sender";
    private static final String QUEUE_NAME_GOODS_FETCHED = "GOODS_FETCHED_EVENTS";


    @Bean(name = "OrderQueue")
    Queue queueGateway() {
        return new Queue(QUEUE_NAME_ORDER_CREATED, true);
    }

    @Bean(name = "GoodsFetchedQueue")
    Queue goodsFetchedQueue() {
        return new Queue(QUEUE_NAME_GOODS_FETCHED, true);
    }

    @Bean(name = "OrderSenderTopic")
    TopicExchange topic() {
        return new TopicExchange(TOPIC_EXCHANGE_NAME);
    }

    @Bean(name = "GoodsFetchedTopic")
    TopicExchange topicGoodsFetched() {
        return new TopicExchange(TOPIC_EXCHANGE_NAME_2);
    }

    @Bean(name = "OrderEventBinding")
    Binding bindingGateway(@Qualifier("OrderQueue") Queue queue,
                           @Qualifier("OrderSenderTopic") TopicExchange topicExchange) {
        return BindingBuilder.bind(queue).to(topicExchange).with(QUEUE_NAME_ORDER_CREATED);
    }

    @Bean(name = "GoodsFetchedBinding")
    Binding bindingGoods(@Qualifier("GoodsFetchedQueue") Queue queue,
                         @Qualifier("GoodsFetchedTopic") TopicExchange topicExchange) {
        return BindingBuilder.bind(queue).to(topicExchange).with(QUEUE_NAME_GOODS_FETCHED);
    }

}
