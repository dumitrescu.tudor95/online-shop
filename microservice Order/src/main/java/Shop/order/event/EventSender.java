package Shop.order.event;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class EventSender {

    private final RabbitTemplate rabbitTemplate;

    public EventSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessageInQueue(String topicExchangeName, String routingKey, String eventContent) throws Exception {
        rabbitTemplate.convertAndSend(topicExchangeName, routingKey, eventContent);
        System.out.println("Message has been sent "+eventContent);
    }
}
