package Shop.order.service;

import Shop.order.configuration.MicroserviceCart;
import Shop.order.configuration.MicroserviceProduct;
import Shop.order.dto.CartDto;
import Shop.order.dto.CreateOrderRequest;
import Shop.order.dto.OrderDto;
import Shop.order.entity.Order;
import Shop.order.entity.Status;
import Shop.order.event.CreateOrderEvent;
import Shop.order.event.EventSender;
import Shop.order.event.PaymentMethod;
import Shop.order.event.PaymentProcessedEvent;
import Shop.order.exceptions.IncompleteAddressException;
import Shop.order.exceptions.OrderDoesNotExistException;
import Shop.order.exceptions.PaymentMethodNotSpecifiedException;
import Shop.order.exceptions.PhoneNumberNotSpecifiedException;
import Shop.order.mapper.OrderMapper;
import Shop.order.repository.OrderRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    MicroserviceCart cartManager;
    @Autowired
    MicroserviceProduct microserviceProduct;
    @Autowired
    EventSender eventSender;

    ObjectMapper mapper;

    private static final String TOPIC_EXCHANGE_NAME = "order_sender";
    private static final String ROUTING_KEY = "ORDER_EVENTS";


    @RabbitListener(queues = "PAYMENT_EVENTS")
    public void recievePaymentEvent(String paymentProcessedEvent) throws Exception {
        mapper = new ObjectMapper();

        PaymentProcessedEvent event = mapper.readValue(paymentProcessedEvent, PaymentProcessedEvent.class);

        Optional<Order> modifiedOrder = orderRepository.findById(event.getOrderId());

        if (event.getStatus() == Status.APROOVED) {
            modifiedOrder.get().setStatus(Status.APROOVED);
            modifiedOrder.get().setOrderExpirationDate(LocalDateTime.now());
            orderRepository.save(modifiedOrder.get());
            for (String productId : modifiedOrder.get().getProductIds()) {
                microserviceProduct.removeReservation(modifiedOrder.get().getUser(), productId);
            }
        }
    }

    public String createOrder(CreateOrderRequest createOrderRequest) throws Exception {

        mapper = new ObjectMapper();

        checkIfOrderRequestIsValid(createOrderRequest);

        CartDto specificCart = cartManager.getCart(createOrderRequest.getCartId());
        cartManager.resetCart(specificCart.getCartId());

        Order newOrder = new Order();
        newOrder.setProductIds(specificCart.getProductsId());
        newOrder.setOrderPrice(specificCart.getPrice());
        newOrder.setUser(specificCart.getUser());
        newOrder.setOrderExpirationDate(LocalDateTime.now().plusMinutes(1));
        newOrder.setStatus(Status.PENDING);

        Order savedOrder = orderRepository.save(newOrder);

        CreateOrderEvent event = new CreateOrderEvent();

        event.setOrderId(savedOrder.getOrderId());
        event.setOrderPrice(newOrder.getOrderPrice());
        event.setProductList(newOrder.getProductIds());
        event.setUser(newOrder.getUser());

        if (createOrderRequest.getPaymentType().equals("CASH_ON_DELIVERY")) {
            event.setPaymentType(PaymentMethod.CASH_ON_DELIVERY);
        } else if (createOrderRequest.getPaymentType().equals("CREDIT_CARD")) {
            event.setPaymentType(PaymentMethod.CREDIT_CARD);
        }

        String sendEvent = mapper.writeValueAsString(event);

        eventSender.sendMessageInQueue(TOPIC_EXCHANGE_NAME, ROUTING_KEY, sendEvent);

        return "Order request sent.Check your orders to see your order status";
    }

    public void checkIfOrderRequestIsValid(CreateOrderRequest createOrderRequest) {
        String givenPaymentMethod = createOrderRequest.getPaymentType();
        if (givenPaymentMethod == null ||
                (!givenPaymentMethod.equals("CASH_ON_DELIVERY") &&
                        !givenPaymentMethod.equals("CREDIT_CARD"))) {
            throw new PaymentMethodNotSpecifiedException("You must specify the exception correctly : CREDIT_CARD or CASH_ON_DELIVERY");
        }
        if (createOrderRequest.getPhoneNumber() == null) {
            throw new PhoneNumberNotSpecifiedException("You must enter your phone number");
        }
        if (createOrderRequest.getCountry() == null ||
                createOrderRequest.getCity() == null ||
                createOrderRequest.getStreet() ==null ||
                createOrderRequest.getNumber() == null) {
            throw new IncompleteAddressException("Please make sure you specified the country,city,street and number");
        }
    }

    public List<OrderDto> getAllOrders() {
        return orderRepository.findAll()
                .stream()
                .map(OrderMapper::convertToDto)
                .collect(Collectors.toList());

    }

    public String deleteOrder(String orderId) {

        Optional<Order> specificOrder = orderRepository.findById(orderId);
        checkIfOrderExists(specificOrder);

        orderRepository.deleteById(orderId);

        return "Order deleted";
    }

    public OrderDto getOrder(String orderId) {
        Optional<Order> specificOrder = orderRepository.findById(orderId);
        checkIfOrderExists(specificOrder);
        return OrderMapper.convertToDto(specificOrder.get());
    }

    public void checkIfOrderExists(Optional<Order> specificOrder) {
        if (!specificOrder.isPresent()) {
            throw new OrderDoesNotExistException("The order you specified does not exist");
        }
    }
}
