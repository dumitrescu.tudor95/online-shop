package Shop.order.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document(collection = "OrderDatabase")
@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
@ToString
@Getter
@Setter
public class Order {

    @Id
    private String orderId;
    private String user;
    private Double orderPrice;
    private List<String> productIds;
    private Status status;
    private LocalDateTime orderExpirationDate;

}
