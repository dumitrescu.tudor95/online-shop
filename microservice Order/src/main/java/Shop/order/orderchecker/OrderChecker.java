package Shop.order.orderchecker;

import Shop.order.configuration.MicroservicePayment;
import Shop.order.configuration.MicroserviceProduct;
import Shop.order.event.EventSender;
import Shop.order.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Timer;

@Component
public class OrderChecker {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    MicroservicePayment microservicePayment;
    @Autowired
    MicroserviceProduct microserviceProduct;
    @Autowired
    EventSender eventSender;

    private Timer timer;


    @PostConstruct
    public void checkOrders() {
        this.startTimer();
    }

    public void startTimer() {
        timer = new Timer();

        OrderCheckerTimerTask task = new OrderCheckerTimerTask(orderRepository,microservicePayment,microserviceProduct,eventSender);

        timer.schedule(task, 1000 * 2, 1000 * 15);
    }
}
