package Shop.order.orderchecker;

import Shop.order.configuration.MicroservicePayment;
import Shop.order.configuration.MicroserviceProduct;
import Shop.order.entity.Status;
import Shop.order.event.EventSender;
import Shop.order.event.GoodsFetchedEvent;
import Shop.order.repository.OrderRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.TimerTask;

@Component
public class OrderCheckerTimerTask extends TimerTask {


    OrderRepository orderRepository;
    MicroservicePayment microservicePayment;
    MicroserviceProduct microserviceProduct;
    EventSender eventSender;

    ObjectMapper mapper;

    private static final String TOPIC_EXCHANGE_NAME_GOODS_FETCHED = "goods_fetched_sender";
    private static final String ROUTING_KEY_GOODS_FETCHED = "GOODS_FETCHED_EVENTS";

    public OrderCheckerTimerTask(OrderRepository orderRepository,
                                 MicroservicePayment microservicePayment,
                                 MicroserviceProduct microserviceProduct,
                                 EventSender eventSender) {
        this.orderRepository = orderRepository;
        this.microservicePayment = microservicePayment;
        this.microserviceProduct = microserviceProduct;
        this.eventSender = eventSender;
    }

    @Override
    public void run() {

        mapper = new ObjectMapper();

        orderRepository.findAll()
                .stream()

                .forEach(order -> {
                    if (order.getStatus() == Status.PENDING) {
                        if (order.getOrderExpirationDate().isBefore(LocalDateTime.now())) {
                            order.setStatus(Status.EXPIRED);
                            orderRepository.save(order);
                            for (String product : order.getProductIds()) {
                                microserviceProduct.putProductBackInShop(order.getUser(), product);
                            }
                        } else if (microservicePayment.getBalance(order.getUser()) >= order.getOrderPrice()) {

                            order.setStatus(Status.APROOVED);
                            order.setOrderExpirationDate(LocalDateTime.now());
                            orderRepository.save(order);

                            microservicePayment.removeBalance(order.getUser(), order.getOrderPrice());

                            for (String productId : order.getProductIds()) {
                                microserviceProduct.removeReservation(order.getUser(), productId);
                            }
                        }
                    }


                });
        System.out.println("Orders checked");
    }
}
