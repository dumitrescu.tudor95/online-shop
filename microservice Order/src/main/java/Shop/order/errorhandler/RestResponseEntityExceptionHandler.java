package Shop.order.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleMethodArgumentNotValid(HttpServletRequest request, Exception ex) {

        ErrorDTO error = new ErrorDTO();
        error.setStatusCode("404");
        error.setMessage(ex.getMessage());
        System.out.println("an error has been thrown with the message "+ex.getMessage());
        ResponseEntity<Object> response = new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
        return response;
    }
}