package Shop.order.dto;

import Shop.order.entity.Status;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
@ToString
@Getter
@Setter
public class OrderDto {

    private String orderId;
    private Double orderPrice;
    private String user;
    private List<String> productIds;
    private Status availability;
    private LocalDateTime orderExpirationDate;

}
