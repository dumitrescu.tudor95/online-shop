package Shop.order.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class ProductDto {

    private String product_id;
    private String name;
    private String categoryId;
    private String details;
    private Double price;
    private Integer reviews;
    private Integer countInStock;
}
