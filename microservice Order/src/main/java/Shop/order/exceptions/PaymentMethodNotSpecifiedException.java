package Shop.order.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PaymentMethodNotSpecifiedException extends RuntimeException {
    public PaymentMethodNotSpecifiedException(String message){
        super(message);
    }
}
