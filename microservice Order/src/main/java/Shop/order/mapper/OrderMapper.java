package Shop.order.mapper;

import Shop.order.dto.OrderDto;
import Shop.order.entity.Order;

public class OrderMapper {

    public static OrderDto convertToDto(Order order){
        OrderDto orderDto=new OrderDto();
        orderDto.setOrderId(order.getOrderId());
        orderDto.setOrderPrice(order.getOrderPrice());
        orderDto.setUser(order.getUser());
        orderDto.setOrderExpirationDate(order.getOrderExpirationDate());
        orderDto.setAvailability(order.getStatus());
        orderDto.setProductIds(order.getProductIds());
        return orderDto;
    }
}
