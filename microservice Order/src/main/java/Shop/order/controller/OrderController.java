package Shop.order.controller;

import Shop.order.dto.OrderDto;
import Shop.order.dto.CreateOrderRequest;
import Shop.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping("/createOrder")
    public String createOrder(@RequestBody CreateOrderRequest event) throws Exception {
        return orderService.createOrder(event);
    }

    @DeleteMapping("/deleteOrder")
    public String deleteOrder(@RequestParam("orderId") String orderId){
        return orderService.deleteOrder(orderId);
    }

    @GetMapping("/getOrder/{orderId}")
    public OrderDto getOrder(@PathVariable("orderId") String orderId){
        return orderService.getOrder(orderId);
        }

    @GetMapping("/getAllOrders")
    public List<OrderDto> getAllOrders(){
        return orderService.getAllOrders();
    }
}
