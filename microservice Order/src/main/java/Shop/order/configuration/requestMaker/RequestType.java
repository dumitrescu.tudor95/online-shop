package Shop.order.configuration.requestMaker;

public enum RequestType {
    GET, PUT, POST, DELETE;
}
