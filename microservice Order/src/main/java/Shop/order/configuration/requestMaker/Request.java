package Shop.order.configuration.requestMaker;

import Shop.order.errorhandler.RestTemplateResponseErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.List;

@Component
public class Request {

    @Autowired
    RestTemplate restTemplate;

    HttpHeaders headers;

    UriComponentsBuilder urlBuilder;

    public Object sendBodyPassedRequest_recievingObject(RequestType requestType,
                                                        String link,
                                                        Object passedBody,
                                                        HashMap<String, String> paramsSet,
                                                        HashMap<String, String> headersSet) {

        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

        initializeHeaders();

        populateHeaders(headersSet);

        HttpEntity<Object> entity = new HttpEntity<>(passedBody, headers);

        setUriBuilderLink(link);

        populateQueryParams(paramsSet);

        UriComponents uri = urlBuilder.build();

        HttpMethod httpRequestType = getRequestType(requestType);

        ResponseEntity<Object> responseEntity = restTemplate.exchange(
                uri.toString(),
                httpRequestType,
                entity,
                Object.class
        );

        return responseEntity.getBody();
    }

    public String sendBodyPassedRequest_recievingString(RequestType requestType,
                                                        String link,
                                                        Object passedBody,
                                                        HashMap<String, String> paramsSet,
                                                        HashMap<String, String> headersSet) {

        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

        initializeHeaders();

        populateHeaders(headersSet);

        HttpEntity<Object> entity = new HttpEntity<>(passedBody, headers);

        setUriBuilderLink(link);

        populateQueryParams(paramsSet);

        UriComponents uri = urlBuilder.build();

        HttpMethod httpRequestType = getRequestType(requestType);

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                uri.toString(),
                httpRequestType,
                entity,
                String.class
        );

        return responseEntity.getBody();
    }

    public Object sendNoBodyPassedRequest_recievingObject(RequestType requestType,
                                                          String link,
                                                          HashMap<String, String> paramsSet,
                                                          HashMap<String, String> headersSet) {

        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

        initializeHeaders();

        populateHeaders(headersSet);

        HttpEntity entity = new HttpEntity<>(headers);

        setUriBuilderLink(link);

        populateQueryParams(paramsSet);

        UriComponents uri = urlBuilder.build();

        HttpMethod httpRequestType = getRequestType(requestType);

        ResponseEntity<Object> responseEntity = restTemplate.exchange(
                uri.toString(),
                httpRequestType,
                entity,
                Object.class
        );


        return responseEntity.getBody();
    }


    public String sendMessageReturningRequest(RequestType requestType,
                                              String link,
                                              HashMap<String, String> paramsSet,
                                              HashMap<String, String> headersSet) {

        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

        initializeHeaders();

        populateHeaders(headersSet);

        HttpEntity entity = new HttpEntity<>(headers);

        setUriBuilderLink(link);

        populateQueryParams(paramsSet);

        UriComponents uri = urlBuilder.build();

        HttpMethod httpRequestType = getRequestType(requestType);

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                uri.toString(),
                httpRequestType,
                entity,
                String.class
        );

        return responseEntity.getBody();
    }


    public <T> List<T> sendListReturningRequest(RequestType requestType,
                                                String link,
                                                HashMap<String, String> paramsSet,
                                                HashMap<String, String> headersSet) {
        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

        initializeHeaders();

        populateHeaders(headersSet);

        HttpEntity entity = new HttpEntity<>(headers);

        setUriBuilderLink(link);

        populateQueryParams(paramsSet);

        UriComponents uri = urlBuilder.build();

        HttpMethod httpRequestType = getRequestType(requestType);

        ResponseEntity<List<T>> responseEntity = restTemplate.exchange(
                uri.toString(),
                httpRequestType,
                entity,
                new ParameterizedTypeReference<List<T>>() {
                }
        );
        return responseEntity.getBody();
    }


    private void initializeHeaders() {
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
    }

    private void populateHeaders(HashMap<String, String> givenHeaders) {
        for (String s : givenHeaders.keySet()) {
            headers.set(s, givenHeaders.get(s));
        }
    }

    private void setUriBuilderLink(String link) {
        urlBuilder = UriComponentsBuilder.fromHttpUrl(link);
    }

    private void populateQueryParams(HashMap<String, String> givenParams) {
        for (String s : givenParams.keySet()) {
            urlBuilder.queryParam(s, givenParams.get(s));
        }
    }

    private HttpMethod getRequestType(RequestType requestType) {
        switch (requestType) {
            case GET:
                return HttpMethod.GET;
            case PUT:
                return HttpMethod.PUT;
            case POST:
                return HttpMethod.POST;
            case DELETE:
                return HttpMethod.DELETE;
            default:
                return null;
        }
    }
}
