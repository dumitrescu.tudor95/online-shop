package Shop.order.configuration;

import Shop.order.configuration.requestMaker.RequestType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.util.HashMap;

@Component
public class MicroserviceProduct extends MicroserviceRequest {

    @Value("${put.product.back.in.shop}")
    private String PUT_PRODUCT_BACK_IN_SHOP_LINK;

    @Value("${remove.reservation}")
    private String REMOVE_RESERVATION_LINK;

    public void putProductBackInShop(String user, String product) {
        HashMap<String,String> queryParams=new HashMap<>();
        queryParams.put("user",user);
        queryParams.put("productId",product);

        requestSender.sendMessageReturningRequest(RequestType.PUT,
                PUT_PRODUCT_BACK_IN_SHOP_LINK,
                queryParams,
                NO_HEADERS);
    }

    public void removeReservation(String user, String product) {
        HashMap<String,String> queryParams=new HashMap<>();
        queryParams.put("user",user);
        queryParams.put("productId",product);

        requestSender.sendMessageReturningRequest(RequestType.DELETE,
                REMOVE_RESERVATION_LINK,
                queryParams,
                NO_HEADERS);
    }
}
