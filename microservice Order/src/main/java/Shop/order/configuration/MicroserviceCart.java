package Shop.order.configuration;

import Shop.order.configuration.requestMaker.RequestType;
import Shop.order.dto.CartDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

@Component
public class MicroserviceCart extends MicroserviceRequest {

    @Value("${get.cart}")
    private String GET_CART_LINK;
    @Value("${reset.cart}")
    private String RESET_CART_LINK;

    public CartDto getCart(String cartId) throws IOException {
        HashMap<String,String> queryParams=new HashMap<>();
        queryParams.put("cartId",cartId);

        Object returnedObject=requestSender.sendNoBodyPassedRequest_recievingObject(RequestType.GET,
                GET_CART_LINK,
                queryParams,
                NO_HEADERS);
        return convertToRequestedObject(returnedObject,CartDto.class);
    }

    public void resetCart(String cartId) {
        HashMap<String,String> queryParams=new HashMap<>();
        queryParams.put("cartId",cartId);

        requestSender.sendMessageReturningRequest(RequestType.PUT,
                RESET_CART_LINK,
                queryParams,
                NO_HEADERS);
    }
}
