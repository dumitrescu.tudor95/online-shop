package Shop.order.configuration;

import Shop.order.configuration.requestMaker.RequestType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;

@Component
public class MicroservicePayment extends MicroserviceRequest {

    @Value("${get.balance}")
    private String GET_DEPOSIT_LINK;
    @Value("${remove.balance}")
    private String REMOVE_BALANCE_LINK;

    public Double getBalance(String user) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("userId", user);
        String returnedString = requestSender.sendMessageReturningRequest(RequestType.GET,
                GET_DEPOSIT_LINK,
                queryParams,
                NO_HEADERS
        );
        return Double.parseDouble(returnedString);
    }


    public void removeBalance(String user, Double orderPrice) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("user", user);
        queryParams.put("balance", String.valueOf(orderPrice));

        requestSender.sendMessageReturningRequest(RequestType.PUT,
                REMOVE_BALANCE_LINK,
                queryParams,
                NO_HEADERS);
    }


}
