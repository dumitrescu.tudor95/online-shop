package Shop.order.controller;

import Shop.order.dto.OrderDto;
import Shop.order.exceptions.GeneralException;
import Shop.order.service.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    @MockBean
    OrderService orderService;

    @Autowired
    MockMvc mockMvc;

    OrderDto orderDto;
    String orderId = "testId";

    ObjectMapper mapper;


    @Before
    public void setUp() {
        orderDto = new OrderDto();
        orderDto.setOrderId(orderId);
        mapper = new ObjectMapper();
    }



    @Test
    public void deleteOrderHappyCase() throws Exception {

        when(orderService.deleteOrder(anyString())).thenReturn("Order deleted");

        mockMvc.perform(delete("/deleteOrder").param("orderId", "testOrderId"))
                .andExpect(status().isOk())
                .andExpect(content().string("Order deleted"));
    }

    @Test
    public void deleteOrderExceptionThrown() throws Exception {

        when(orderService.deleteOrder(anyString())).thenThrow(GeneralException.class);

        mockMvc.perform(delete("/deleteOrder").param("orderId", "testOrderId"))
                .andExpect(status().is4xxClientError());
    }




    @Test
    public void getOrderHappyCase() throws Exception {

        String jsonString = mapper.writeValueAsString(orderDto);

        when(orderService.getOrder(anyString())).thenReturn(orderDto);

        mockMvc.perform(get("/getOrder/{orderId}", orderId))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));

    }

    @Test
    public void getOrderErrorThrown() throws Exception {

        when(orderService.getOrder(anyString())).thenThrow(GeneralException.class);

        mockMvc.perform(get("/getOrder/{orderId}", orderId))
                .andExpect(status().is4xxClientError());

    }




    @Test
    public void getAllOrders() throws Exception {
        List<OrderDto> orderList = new ArrayList<>();
        orderList.add(orderDto);
        orderList.add(new OrderDto());

        String jsonString= mapper.writeValueAsString(orderList);

        when(orderService.getAllOrders()).thenReturn(orderList);

        mockMvc.perform(get("/getAllOrders"))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonString));
    }
}