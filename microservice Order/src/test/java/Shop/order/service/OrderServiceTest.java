package Shop.order.service;

import Shop.order.configuration.MicroserviceCart;
import Shop.order.dto.CartDto;
import Shop.order.dto.CreateOrderRequest;
import Shop.order.dto.OrderDto;
import Shop.order.entity.Order;
import Shop.order.event.EventSender;
import Shop.order.exceptions.IncompleteAddressException;
import Shop.order.exceptions.OrderDoesNotExistException;
import Shop.order.exceptions.PaymentMethodNotSpecifiedException;
import Shop.order.exceptions.PhoneNumberNotSpecifiedException;
import Shop.order.mapper.OrderMapper;
import Shop.order.repository.OrderRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class OrderServiceTest {

    @MockBean
    OrderRepository orderRepository;
    @MockBean
    MicroserviceCart cartManager;
    @MockBean
    EventSender eventSender;

    @InjectMocks
    OrderService orderService;

    Order order;
    String orderId = "orderId";
    CartDto cartDto;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        order = new Order();
        order.setOrderId(orderId);

        cartDto = new CartDto();
        cartDto.setCartId("testId");
        cartDto.setPrice(100.0);
        cartDto.setUser("username");
        cartDto.setProductsId(new ArrayList<>());
    }

    @Test
    public void createOrder() throws Exception {

        CreateOrderRequest orderRequest = new CreateOrderRequest();
        orderRequest.setCartId("testId");
        orderRequest.setPaymentType("CASH_ON_DELIVERY");
        orderRequest.setPhoneNumber("0721276374");
        orderRequest.setCountry("Romania");
        orderRequest.setCity("Bucharest");
        orderRequest.setStreet("Uruguay");
        orderRequest.setNumber("12");

        System.out.println(cartDto);

        when(cartManager.getCart(anyString())).thenReturn(cartDto);
        when(orderRepository.save(any())).thenReturn(order);

        assertEquals(orderService.createOrder(orderRequest), "Order request sent.Check your orders to see your order status");
        verify(cartManager, times(1)).getCart(anyString());
        verify(cartManager, times(1)).resetCart(anyString());
        verify(orderRepository, times(1)).save(any());
    }


    @Test(expected = PaymentMethodNotSpecifiedException.class)
    public void orderRequestCheck_whenPaymentTypeNotSpecified() throws Exception {
        CreateOrderRequest orderRequest = new CreateOrderRequest();
        orderRequest.setCartId("testId");

        orderService.createOrder(orderRequest);
    }
    @Test(expected= PhoneNumberNotSpecifiedException.class)
    public void orderRequestCheck_whenPhoneNumberIsNotSpecified() throws Exception {
        CreateOrderRequest orderRequest = new CreateOrderRequest();
        orderRequest.setCartId("testId");
        orderRequest.setPaymentType("CASH_ON_DELIVERY");

        orderService.createOrder(orderRequest);
    }
    @Test(expected= IncompleteAddressException.class)
    public void orderRequestCheck_whenCountryIsNotSpecified() throws Exception {
        CreateOrderRequest orderRequest = new CreateOrderRequest();
        orderRequest.setCartId("testId");
        orderRequest.setPaymentType("CASH_ON_DELIVERY");
        orderRequest.setPhoneNumber("0721637483");

        orderService.createOrder(orderRequest);
    }
    @Test(expected= IncompleteAddressException.class)
    public void orderRequestCheck_whenCityIsNotSpecified() throws Exception {
        CreateOrderRequest orderRequest = new CreateOrderRequest();
        orderRequest.setCartId("testId");
        orderRequest.setPaymentType("CASH_ON_DELIVERY");
        orderRequest.setPhoneNumber("0721637483");
        orderRequest.setCountry("Romania");

        orderService.createOrder(orderRequest);
    }
    @Test(expected= IncompleteAddressException.class)
    public void orderRequestCheck_whenStreetIsNotSpecified() throws Exception {
        CreateOrderRequest orderRequest = new CreateOrderRequest();
        orderRequest.setCartId("testId");
        orderRequest.setPaymentType("CASH_ON_DELIVERY");
        orderRequest.setPhoneNumber("0721637483");
        orderRequest.setCountry("Romania");
        orderRequest.setCity("Bucharest");

        orderService.createOrder(orderRequest);
    }
    @Test(expected= IncompleteAddressException.class)
    public void orderRequestCheck_whenStreetNumberIsNotSpecified() throws Exception {
        CreateOrderRequest orderRequest = new CreateOrderRequest();
        orderRequest.setCartId("testId");
        orderRequest.setPaymentType("CASH_ON_DELIVERY");
        orderRequest.setPhoneNumber("0721637483");
        orderRequest.setCountry("Romania");
        orderRequest.setCity("Bucharest");
        orderRequest.setStreet("Uruguay");

        orderService.createOrder(orderRequest);
    }

    @Test
    public void getAllOrders() {
        List<Order> list = new ArrayList<>();
        list.add(order);

        List<OrderDto> returnedList = new ArrayList<>();
        returnedList.add(OrderMapper.convertToDto(order));

        when(orderRepository.findAll()).thenReturn(list);

        assertEquals(orderService.getAllOrders(), returnedList);
        verify(orderRepository, times(1)).findAll();
    }

    @Test
    public void deleteOrder() {

        when(orderRepository.findById(anyString())).thenReturn(Optional.of(order));

        orderService.deleteOrder(orderId);

        verify(orderRepository.findById(anyString()));
        verify(orderRepository, times(1)).deleteById(anyString());
    }

    @Test(expected = OrderDoesNotExistException.class)
    public void deleteOrderWhenOrderDoesNotExist() {
        when(orderRepository.findById(anyString())).thenReturn(Optional.ofNullable(null));

        orderService.deleteOrder(orderId);

        verify(orderRepository, times(1)).findById(anyString());
    }

    @Test
    public void getOrder() {
        OrderDto returnedOrder = OrderMapper.convertToDto(order);

        when(orderRepository.findById(anyString())).thenReturn(Optional.of(order));

        assertEquals(orderService.getOrder(orderId), returnedOrder);
        verify(orderRepository, times(1)).findById(anyString());
    }

    @Test(expected = OrderDoesNotExistException.class)
    public void getOrderWhenOrderDoesNotExist() {
        when(orderRepository.findById(anyString())).thenReturn(Optional.ofNullable(null));

        orderService.getOrder(orderId);

        verify(orderRepository, times(1)).findById(anyString());
    }

    @Test(expected = OrderDoesNotExistException.class)
    public void checkIfOrderExist_whenItDoesnt(){
        Optional<Order> optional=Optional.empty();
        orderService.checkIfOrderExists(optional);
    }
}