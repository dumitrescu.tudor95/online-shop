package tudor.shop.email.microservice.email.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tudor.shop.email.microservice.email.services.EmailServiceImpl;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(EmailController.class)
public class EmailControllerTest {

    @MockBean
    EmailServiceImpl emailService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void sendEmail() throws Exception {
        when(emailService.sendEmail(any()))
                .thenReturn("Mail has been sent");

        mockMvc.perform(post("/sendEmail"))
                .andExpect(status().isOk())
                .andExpect(content().string("Mail has been sent"));
    }
}