package tudor.shop.email.microservice.email.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;
import tudor.shop.email.microservice.email.config.EmailCfg;
import tudor.shop.email.microservice.email.dto.EmailFormDTO;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    EmailCfg emailCfg;

    @Override
    public String sendEmail(EmailFormDTO emailFormDTO) {
        //create mail sender
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(emailCfg.getHost());
        mailSender.setPort(emailCfg.getPort());
        mailSender.setUsername(emailCfg.getUsername());
        mailSender.setPassword(emailCfg.getPassword());

        //create an email instance
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(emailFormDTO.getFrom());
        mailMessage.setTo(emailFormDTO.getTo());
        mailMessage.setSubject(emailFormDTO.getSubject());
        mailMessage.setText(emailFormDTO.getMessage());

        //send mail
        mailSender.send(mailMessage);

        return "Mail has been sent";
    }
}
