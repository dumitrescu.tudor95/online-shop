package tudor.shop.email.microservice.email.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tudor.shop.email.microservice.email.dto.EmailFormDTO;
import tudor.shop.email.microservice.email.services.EmailService;

@RestController
public class EmailController {

    @Autowired
    private EmailService emailService;

    @PostMapping("/sendEmail")
    public String sendEmail(@RequestBody EmailFormDTO emailFormDTO) {
        return emailService.sendEmail(emailFormDTO);
    }


}
