package tudor.shop.email.microservice.email.services;

import org.springframework.stereotype.Service;
import tudor.shop.email.microservice.email.dto.EmailFormDTO;

public interface EmailService {

    String sendEmail(EmailFormDTO emailFormDTO);
}
