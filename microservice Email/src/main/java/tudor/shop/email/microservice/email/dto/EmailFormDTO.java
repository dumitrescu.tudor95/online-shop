package tudor.shop.email.microservice.email.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailFormDTO {

    String from;
    String to;
    String subject;
    String message;
}
