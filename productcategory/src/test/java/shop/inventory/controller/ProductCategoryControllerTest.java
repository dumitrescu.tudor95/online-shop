package shop.inventory.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import shop.inventory.service.ProductCategoryService;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(ProductCategoryController.class)
public class ProductCategoryControllerTest {

    @MockBean
    ProductCategoryService service;


    @Autowired
    MockMvc mockMvc;



    @Test
    public void addProductCategory() throws Exception {

        when(service.addCategory(anyString())).thenReturn("Category is in stock");

        mockMvc.perform(post("/addProductCategory")
                .param("categoryName", "newCategory"))
                .andExpect(status().isOk())
                .andExpect(content().string("Category is in stock"));
    }
}