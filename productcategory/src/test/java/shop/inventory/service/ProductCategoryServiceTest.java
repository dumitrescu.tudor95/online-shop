package shop.inventory.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import shop.inventory.configuration.ProductManager;
import shop.inventory.entity.Category;
import shop.inventory.repository.ProductCategoryRepository;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ProductCategoryServiceTest {

    @MockBean
    ProductCategoryRepository categoryRepository;
    @MockBean
    ProductManager productManager;

    @InjectMocks
    ProductCategoryService categoryService;

    Category category;
    String categoryId = "testId";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        category = new Category();
        category.setName(categoryId);
    }



    @Test
    public void addCategory() {

        when(categoryRepository.findById(anyString())).thenReturn(Optional.ofNullable(null));

        categoryService.addCategory(categoryId);

        verify(categoryRepository,times(1)).findById(anyString());
        verify(categoryRepository,times(1)).save(any());
    }
}