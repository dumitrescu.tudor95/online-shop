package shop.inventory.mapper;

import shop.inventory.entity.Category;
import shop.inventory.dto.CategoryDto;

public class ProductCategoryMapper {

    public static CategoryDto convertToDto(Category productCategory){
        CategoryDto dto=new CategoryDto();
        dto.setCategoryName(productCategory.getName());
        return dto;
    }
}
