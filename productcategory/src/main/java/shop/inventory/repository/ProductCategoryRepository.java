package shop.inventory.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import shop.inventory.entity.Category;

public interface ProductCategoryRepository extends MongoRepository<Category, String> {
}
