package shop.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shop.inventory.entity.Category;
import shop.inventory.repository.ProductCategoryRepository;

import java.util.Optional;

@Service
public class ProductCategoryService {

    @Autowired
    ProductCategoryRepository productCategoryRepository;


    public String addCategory(String categoryName) {

        Optional<Category> specificCategory = productCategoryRepository.findById(categoryName);

        if (specificCategory.isEmpty()) {
            Category newCategory = new Category();
            newCategory.setName(categoryName);
            productCategoryRepository.save(newCategory);
        }

        return "Category is in stock";
    }
}
