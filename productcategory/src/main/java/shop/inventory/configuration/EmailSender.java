package shop.inventory.configuration;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import shop.inventory.dto.EmailFormDTO;


@Component
public class EmailSender {

    RestTemplate restTemplate;

    public String sendEmail(String specificUserEmail, String subject, String message) {
        restTemplate = new RestTemplate();

        EmailFormDTO emailDTO = new EmailFormDTO();
        emailDTO.setFrom("tudor_project_shop@mailtrap.com");
        emailDTO.setTo(specificUserEmail);
        emailDTO.setSubject(subject);
        emailDTO.setMessage(message);

        new Thread(() -> {
            ResponseEntity<String> responseString = restTemplate.postForEntity("http://localhost:8081/sendEmail", emailDTO, String.class);
        }).start();

        return "Mail will be sent shortly";
    }

}
