package shop.inventory.configuration;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ProductManager {

    RestTemplate restTemplate;

    public Long getProductCountFor(String categoryName){


        restTemplate = new RestTemplate();

        //RequestHeaders
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        HttpEntity httpEntity = new HttpEntity(headers);

        UriComponents uri = UriComponentsBuilder.fromHttpUrl("http://localhost:8084/getProductCountByCategory")
                .queryParam("categoryName", categoryName)
                .build();

        ResponseEntity<Long> exchangeResponse = restTemplate.exchange(
                uri.toString(),
                HttpMethod.GET,
                httpEntity,
                Long.class);

        return exchangeResponse.getBody();
    }

}
