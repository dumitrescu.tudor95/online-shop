package shop.inventory.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailFormDTO {

    String from;
    String to;
    String subject;
    String message;
}
