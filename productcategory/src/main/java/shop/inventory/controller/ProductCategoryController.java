package shop.inventory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import shop.inventory.service.ProductCategoryService;

@RestController
public class ProductCategoryController {

    @Autowired
    ProductCategoryService productCategoryService;

    @PostMapping("/addProductCategory")
    public String addProductCategory(@RequestParam("categoryName") String categoryName){
        return productCategoryService.addCategory(categoryName);
    }
}
